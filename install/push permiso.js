//instancio una nueva conexion a mongo
var mongo = new Mongo("localhost:27017");

//selecciono la base de datos admin para crear un usuario
var db = mongo.getDB("admin");

//autentico mi usuario para poder realizar cambios
db.auth("sisgac", "c1envigado");

//selecciono la base de datos de la aplicación
db = mongo.getDB("sisgac1");

db.colTipoUsuarios.update(
        {tipoUsuario:"Super Administrador"},
        {"$push":{ctrlxMetodos: "usuarios/actualizarPregResSeguridad"}});
db.colTipoUsuarios.update(
        {tipoUsuario:"Administrador"},
        {"$push":{ctrlxMetodos: "usuarios/actualizarPregResSeguridad"}});
db.colTipoUsuarios.update(
        {tipoUsuario:"Usuario"},
        {"$push":{ctrlxMetodos: "usuarios/actualizarPregResSeguridad"}});
        