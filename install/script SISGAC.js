//instancio una nueva conexion a mongo
var mongo = new Mongo("localhost:27017");

//selecciono la base de datos admin para crear un usuario
var db = mongo.getDB("admin");

//autentico mi usuario para poder realizar cambios
db.auth("sisgac", "c1envigado");

//selecciono la base de datos de la aplicación
db = mongo.getDB("sisgac");

//configuro lista de controladores (modulos) para preparar los menus y sus respectivos enlaces
var modulosSuperAdmin = [
    {"<i class='glyphicon glyphicon-inbox'></i> Trámites": [
            {"Crear": "tramites/crear"},
            {"Listar": "tramites/listar"}
    ]},
    {"<i class='glyphicon glyphicon-user'></i> Usuarios": [
            {"Crear": "usuarios/crear"},
            {"Actualizar datos": "usuarios/actualizar"},
            {"Listar": "usuarios/listar"}
    ]},
    {"<i class='glyphicon glyphicon-th-list'></i> Logs": "logs/listar"},
    {"<i class='glyphicon glyphicon-wrench'></i> Ajustes": [
            {"Documentos de identificación personal":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarIdentificaciones')"},
            {"Tipos de trámites o actuaciones":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoTramites')"},
            {"Tipos de modalidades":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoModalidades')"},
            {"Tipos de usos":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoUsoVivienda')"},
            {"Tipos de objeto de trámite":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoObjetoTramite')"},
            {"Departamentos":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarDepartamentos')"},
            {"Municipios":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarMunicipios')"},
            {"Barrios":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarBarrios')"},
            {"Veredas":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarVeredas')"},
            {"Curadurias":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarCuradurias')"}
    ]}
];

var modulosAdmin = [
    {"<i class='glyphicon glyphicon-inbox'></i> Trámites": [
            {"Crear": "tramites/crear"},
            {"Listar": "tramites/listar"}
    ]},
    {"<i class='glyphicon glyphicon-user'></i> Usuarios": [
            {"Crear": "usuarios/crear"},
            {"Actualizar datos": "usuarios/actualizar"},
            {"Listar": "usuarios/listar"}
    ]},
    {"<i class='glyphicon glyphicon-wrench'></i> Ajustes": [
            {"Documentos de identificación personal":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarIdentificaciones')"},
            {"Tipos de trámites o actuaciones":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoTramites')"},
            {"Tipos de modalidades":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoModalidades')"},
            {"Tipos de usos":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoUsoVivienda')"},
            {"Tipos de objeto de trámite":"javascript: sisgac1.cargarModalAjustes('ajustes/cargarTipoObjetoTramite')"}
    ]}
];

var modulosUsuario = [
    {"<i class='glyphicon glyphicon-inbox'></i> Trámites": [
            {"Crear": "tramites/crear"},
            {"Listar": "tramites/listar"}
    ]},
    {"<i class='glyphicon glyphicon-user'></i> Actualizar datos": "usuarios/actualizar"}
];

//ajusto los permisos que tendrá cada usuario (controlador/metodo)
var ctrlxMetodosSuperAdmin = [
    "login/index",
    "login/entrar",
    "login/cerrar",
    "archivos/subir",
    "archivos/consultar",
    "archivos/eliminar",
    "tramites/listar",
    "tramites/crear",
    "tramites/crearNuevo",
    "tramites/consultar",
    "tramites/editar",
    "tramites/eliminar",
    "usuarios/index",
    "usuarios/crear",
    "usuarios/crearNuevo",
    "usuarios/actualizar",
    "usuarios/actualizarUsuario",
    "usuarios/listar",
    "usuarios/validarNombreUsuarioLibre",
    "usuarios/validarEmailLibre",
    "usuarios/cambiarPass",
    "usuarios/cambiarEstadoUsuario",
    "usuarios/cambiarCuraduriaUsuario",
    "usuarios/consultarDatosUsuario",
    "usuarios/actualizarDatosUsuario",
    "ajustes/index",
    "ajustes/crearDepartamento",
    "ajustes/crearMunicipio",
    "ajustes/crearBarrio",
    "ajustes/crearVereda",
    "ajustes/crearCuraduria",
    "ajustes/actualizarDepartamento",
    "ajustes/actualizarMunicipio",
    "ajustes/actualizarBarrio",
    "ajustes/actualizarVereda",
    "ajustes/actualizarCuraduria",
    "ajustes/cargarDepartamentos",
    "ajustes/cargarMunicipios",
    "ajustes/cargarBarrios",
    "ajustes/cargarVeredas",
    "ajustes/cargarCuradurias",
    "ajustes/crearRegistro",
    "ajustes/actualizarRegistro",
    "ajustes/cargarAjusteEspecifico",
    "ajustes/cargarIdentificaciones",
    "ajustes/cargarTipoTramites",
    "ajustes/cargarTipoModalidades",
    "ajustes/cargarTipoUsoVivienda",
    "ajustes/cargarTipoObjetoTramite",
    "logs/listar",
    "error/index"
];
var ctrlxMetodosAdmin = [
    "login/index",
    "login/entrar",
    "login/cerrar",
    "archivos/subir",
    "archivos/consultar",
    "archivos/eliminar",
    "tramites/listar",
    "tramites/crear",
    "tramites/crearNuevo",
    "tramites/consultar",
    "tramites/editar",
    "tramites/eliminar",
    "usuarios/index",
    "usuarios/crear",
    "usuarios/crearNuevo",
    "usuarios/actualizar",
    "usuarios/actualizarUsuario",
    "usuarios/listar",
    "usuarios/validarNombreUsuarioLibre",
    "usuarios/validarEmailLibre",
    "usuarios/cambiarPass",
    "usuarios/cambiarEstadoUsuario",
    "usuarios/consultarDatosUsuario",
    "usuarios/actualizarDatosUsuario",
    "ajustes/index",
    "ajustes/crearRegistro",
    "ajustes/actualizarRegistro",
    "ajustes/cargarAjusteEspecifico",
    "ajustes/cargarIdentificaciones",
    "ajustes/cargarTipoTramites",
    "ajustes/cargarTipoModalidades",
    "ajustes/cargarTipoUsoVivienda",
    "ajustes/cargarTipoObjetoTramite",
    "error/index"
];
var ctrlxMetodosUsuario = [
    "login/index",
    "login/entrar",
    "login/cerrar",
    "archivos/subir",
    "archivos/consultar",
    "archivos/editar",
    "archivos/eliminar",
    "tramites/listar",
    "tramites/crear",
    "tramites/crearNuevo",
    "tramites/consultar",
    "usuarios/actualizar",
    "usuarios/actualizarUsuario",
    "usuarios/validarNombreUsuarioLibre",
    "usuarios/validarEmailLibre",
    "usuarios/cambiarPass",
    "error/index"
];
//inserto tipos de usuarios con permisos a los metodos que se usarán
db.colTipoUsuarios.insert({
    tipoUsuario: "Super Administrador",
    ctrlxMetodos: ctrlxMetodosSuperAdmin,
    modulos: modulosSuperAdmin});

db.colTipoUsuarios.insert({
    tipoUsuario: "Administrador",
    ctrlxMetodos: ctrlxMetodosAdmin,
    modulos: modulosAdmin});

db.colTipoUsuarios.insert({
    tipoUsuario: "Usuario",
    ctrlxMetodos: ctrlxMetodosUsuario,
    modulos: modulosUsuario});


db.colDepartamentos.insert({
    _id: new ObjectId(),
    nombreDepartamento: "Antioquia",
    codigoDepartamento: "05",
    municipios: [{
            _id: new ObjectId(),
            nombreMunicipio: "Envigado",
            codigoMunicipio: "266",
            curadurias: [
                { _id: new ObjectId(), nombreCuraduria: "Curaduría Urbana Primera de Envigado", codigoCuraduria: "1"}
            ],
            barrios: [
                { _id: new ObjectId(), nombreBarrio: "El Chingui", codigoBarrio: 1 },
                { _id: new ObjectId(), nombreBarrio: "El Salado", codigoBarrio: 2 },
                { _id: new ObjectId(), nombreBarrio: "La Mina", codigoBarrio: 3 },
                { _id: new ObjectId(), nombreBarrio: "San Rafael", codigoBarrio: 4 },
                { _id: new ObjectId(), nombreBarrio: "Las Antillas", codigoBarrio: 5 },
                { _id: new ObjectId(), nombreBarrio: "El Trianón", codigoBarrio: 6  },
                { _id: new ObjectId(), nombreBarrio: "Loma del Barro", codigoBarrio: 7 },
                { _id: new ObjectId(), nombreBarrio: "Las Casitas", codigoBarrio: 8 },
                { _id: new ObjectId(), nombreBarrio: "Las Vegas", codigoBarrio: 9 },
                { _id: new ObjectId(), nombreBarrio: "Primavera", codigoBarrio: 10 },
                { _id: new ObjectId(), nombreBarrio: "La Paz", codigoBarrio: 11 },
                { _id: new ObjectId(), nombreBarrio: "Milán Vallejuelos", codigoBarrio: 12 },
                { _id: new ObjectId(), nombreBarrio: "El Dorado", codigoBarrio: 13 },
                { _id: new ObjectId(), nombreBarrio: "San José", codigoBarrio: 14 },
                { _id: new ObjectId(), nombreBarrio: "Las Brujas", codigoBarrio: 15 },
                { _id: new ObjectId(), nombreBarrio: "La Pradera", codigoBarrio: 16 },
                { _id: new ObjectId(), nombreBarrio: "El Chocho", codigoBarrio: 17 },
                { _id: new ObjectId(), nombreBarrio: "La Inmaculada", codigoBarrio: 18 },
                { _id: new ObjectId(), nombreBarrio: "La Sebastiana", codigoBarrio: 19 },
                { _id: new ObjectId(), nombreBarrio: "Los Naranjos", codigoBarrio: 20 },
                { _id: new ObjectId(), nombreBarrio: "Mesa", codigoBarrio: 21 },
                { _id: new ObjectId(), nombreBarrio: "Zona Centro", codigoBarrio: 22 },
                { _id: new ObjectId(), nombreBarrio: "Alcalá", codigoBarrio: 23 },
                { _id: new ObjectId(), nombreBarrio: "El Portal", codigoBarrio: 24 },
                { _id: new ObjectId(), nombreBarrio: "San Marcos", codigoBarrio: 25 },
                { _id: new ObjectId(), nombreBarrio: "Obrero", codigoBarrio: 26 },
                { _id: new ObjectId(), nombreBarrio: "Bucarest", codigoBarrio: 27 },
                { _id: new ObjectId(), nombreBarrio: "La Magnolia", codigoBarrio: 28 },
                { _id: new ObjectId(), nombreBarrio: "Las Florez", codigoBarrio: 29 },
                { _id: new ObjectId(), nombreBarrio: "Uribe Ángel", codigoBarrio: 30 },
                { _id: new ObjectId(), nombreBarrio: "El Esmeraldal", codigoBarrio: 31 },
                { _id: new ObjectId(), nombreBarrio: "Loma del Atravesado", codigoBarrio: 32 },
                { _id: new ObjectId(), nombreBarrio: "Zúñiga", codigoBarrio: 33 },
                { _id: new ObjectId(), nombreBarrio: "Alto de Misael", codigoBarrio: 34 },
                { _id: new ObjectId(), nombreBarrio: "Las Orquídeas", codigoBarrio: 35 },
                { _id: new ObjectId(), nombreBarrio: "Pontevedra", codigoBarrio: 36 },
                { _id: new ObjectId(), nombreBarrio: "Jardines", codigoBarrio: 37 },
                { _id: new ObjectId(), nombreBarrio: "Villagrande", codigoBarrio: 38 },
                { _id: new ObjectId(), nombreBarrio: "Bosques de Zúñiga", codigoBarrio: 39 }
            ],
            veredas: [
                { _id: new ObjectId(), nombreVereda: "El Vallano", codigoVereda: 1 },
                { _id: new ObjectId(), nombreVereda: "El Escobero", codigoVereda: 2 },
                { _id: new ObjectId(), nombreVereda: "Santa Catalina", codigoVereda: 3 },
                { _id: new ObjectId(), nombreVereda: "Las Palmas", codigoVereda: 4 },
                { _id: new ObjectId(), nombreVereda: "Pantanillo", codigoVereda: 5 },
                { _id: new ObjectId(), nombreVereda: "Perico", codigoVereda: 6 }
            ]
    }]
});

var idCuraduria1Env = db.colDepartamentos.findOne( {"municipios.curadurias.codigoCuraduria": "1"} , {"municipios.curadurias._id" : true} );

idCuraduria1Env = idCuraduria1Env['municipios'][0]['curadurias'][0]['_id'].str;

//consulto el id de del rol super administrador, esto retorna un array
var tipoUsuario = db.colTipoUsuarios.findOne({tipoUsuario: "Super Administrador"}, {"_id": true});
//obtengo el string que contiene ObjectID("9dfj2932h3dj29fh2fh29") retornando solo "9dfj2932h3dj29fh2fh29"
tipoUsuario = tipoUsuario['_id'].str;

//configuro los datos del superadministrador
var datosCurador = {
    nombreUsuario: "curador",
    pass: "eb581eb3587871a046cf3bf5c0788bbdd3282cb800e0ec5408b55d5a2c3ba686",
    nombre: "Osbaldo Alonso",
    apellido: "Carmona Correa",
    email: "curador@curaduria1env.com.co",
    preguntaSeguridad: "Curaduría en la que labora",
    respuestaSeguridad: "1",
    tipoUsuario: tipoUsuario,
    estado: true,
    curaduria: idCuraduria1Env
};

//inserto al super administrador la contraseña encriptada por default es c1envigado (todo en minúsculas)
db.colUsuarios.insert(datosCurador);

//printjson(datosCurador);

//guardo primer log de actividades con el registro del primer usuario y super administrador
db.colLog.insert({
    fecha: new Date(),
    accion: "Primer usuario registrado en la base de datos.",
    cambios: "nombreUsuario: " + datosCurador.nombreUsuario + ",<br>nombre: " + datosCurador.nombre + ",<br>apellido: " + datosCurador.apellido + ",<br>email: " + datosCurador.email + ",<br>tipoUsuario: Super Administrador, <br>idTipoUsuario: " + tipoUsuario,
    autor: "SISGAC"
});

db = mongo.getDB("admin");

db.auth("sa","sa");

db = mongo.getDB("sisgac");