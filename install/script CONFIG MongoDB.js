//instancio una nueva conexion a mongo
var mongo = new Mongo("localhost:27017");

//selecciono la base de datos admin para crear un usuario
var db = mongo.getDB("admin");

//creo el usuario super administrador (SA) con todos los derechos
db.createUser({user: "sa", pwd: "sa", roles: [ "userAdminAnyDatabase", "readWriteAnyDatabase", "dbAdminAnyDatabase", "clusterAdmin" ]});

//creo el usuario para acceder a la base de datos de sisgac1
db.createUser({user: "sisgac", pwd: "c1envigado", roles: [ {role: "readWrite", db: "sisgac"} ] });