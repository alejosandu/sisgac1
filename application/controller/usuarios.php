<?php

/**
 * Description of usuarios
 *
 * @author asanchez
 */
class Usuarios extends Controller {

    private $usuariosModel = null;
    private $ajustesModel = null;
    private $logsModel = null;

    public function __construct() {
        parent::__construct();
        $this->usuariosModel = $this->loadModel("usuariosModel");
        $this->ajustesModel = $this->loadModel("ajustesModel");
        $this->logsModel = $this->loadModel("logsModel");
    }

    public function index() {
        header("location: " . URL . "usuarios/listar");
    }

    public function crear() {
        $tipoUsuarios = $this->usuariosModel->consultarTipoUsuarios();
        if($this->consultarPermisosSuperAdmin()){
            $curadurias = $this->ajustesModel->listarCuraduriasCrearUsuario();
        }
        
        $seccion = "Crear usuario";
        require APP . 'view/_templates/header.php';
        require APP . 'view/usuarios/crear.php';
        require APP . 'view/_templates/footer.php';
    }

    public function crearNuevo() {
        if ($_POST['btnCrearNuevoUsuario'] == true) {
            $this->usuariosModel->__SET("nombreUsuario", strtolower($_POST['nombreUsuario']));
            $this->usuariosModel->__SET("email", $_POST['email']);
            $this->usuariosModel->__SET("nombre", $_POST['nombre']);
            $this->usuariosModel->__SET("apellido", $_POST['apellido']);
            $this->usuariosModel->__SET("tipoUsuario", $_POST['tipoUsuario']);
            if(isset($_POST['curaduria'])){
                if($_POST['curaduria'] == ""){
                    $this->usuariosModel->__SET("curaduria", $_SESSION['curaduria']);
                }else{
                    $this->usuariosModel->__SET("curaduria", $_POST['curaduria']);
                }
            }
            $newPass = new MongoId();
            $newPass = hash("sha256",$newPass);
            $newPass = substr($newPass, -16);
            $this->usuariosModel->__SET("pass", $newPass);
            
            $res = $this->usuariosModel->crearNuevo();
            
            try{
                $this->enviarEmail(strtolower($_POST['nombreUsuario']), $newPass , $_POST['nombre'], $_POST['apellido'], $_POST['email']);
            }catch(Exception $e){
                
            }

            if ($res['n'] == 1) {
                //registro nuevo usuario en el log
                $cambios = "nombreUsuario: " . strtolower($_POST['nombreUsuario']) . "<br>";
                $cambios .= "nombre: " . $_POST['nombre'] . "<br>";
                $cambios .= "apellido: " . $_POST['apellido'] . "<br>";
                $cambios .= "email: " . $_POST['email'] . "<br>";
                $cambios .= "tipoUsuario: " . $_POST['tipoUsuarioTexto'] . "<br>";
                $cambios .= "idTipo: " . $_POST['tipoUsuario'];
                $cambios .= "idCuraduria: " . $_POST['curaduria'];
                $this->logsModel->registrarLog("Creación de nuevo usuario.", $cambios);
            }
            
            header('Content-Type: application/json');
            echo json_encode($res);
        }
    }
    
    public function enviarEmail($nombreUsuario,$pass,$nombre,$apellido,$email){
        try{
            require_once APP . 'libs/PHPMailer/PHPMailerAutoload.php';
            require_once APP . 'libs/PHPMailer/class.phpmailer.php';
            require_once APP . 'libs/PHPMailer/class.smtp.php';

            $mailer = new PHPMailer();
            $mailer->IsSMTP();
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = "ssl";
            $mailer->Host = "smtp.gmail.com";
            $mailer->Port = 465;
//            $mailer->SMTPDebug = 0;
//            $mailer->Debugoutput = "html";
            $mailer->Username = "alejosandu@gmail.com";
            $mailer->Password = "103513195asd";

            $mailer->From = "mobilemarket751130@gmail.com";
            $mailer->FromName = "no-reply";
            $mailer->Subject = "Bienvenido a SISGAC";
            $mailer->addAddress($email, $nombre . " " . $apellido);
            $mailer->CharSet = "UTF-8";
            $mensaje = "Hola " . $nombre . " " . $apellido . "<br>";
            $mensaje .= "Usted ha sido registrado en SISGAC y esta es la información para acceder<br>";
            $mensaje .= "Su usuario para ingresar es: <strong>" . $nombreUsuario . "</strong><br>";
            $mensaje .= "Su contraseña para ingresar es: <strong>" . $pass . "</strong><br>";
            $mensaje .= "Nota: se recomienda que una vez ingrese al sistema cambie la contraseña<br>";
            $mensaje .= "<strong>NO RESPONDA A ESTE MENSAJE</strong><br>";
            $mensaje .= "Si ha recibido este mensaje por error por favor hacer caso omiso a este.";
            $mailer->msgHTML($mensaje);
            $mailer->send();
            return true;
        }catch(Exception $e){
            return false;
        }
    }

    public function actualizar() {
        $this->usuariosModel->__SET("idUsuario", $_SESSION['_idUsuario']);
        $datosUsuario = $this->usuariosModel->consultarDatosUsuario(array("pass" => false));
        $seccion = "Actualizar usuario";
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/usuarios/actualizar.php';
        require APP . 'view/_templates/footer.php';
    }

    public function actualizarUsuario() {
        if ($_POST['btnActualizarUsuario'] == true) {
            $this->usuariosModel->__SET("idUsuario", $_SESSION['_idUsuario']);
            $this->usuariosModel->__SET("nombreUsuario", strtolower($_POST['nombreUsuario']));
            $this->usuariosModel->__SET("email", $_POST['email']);
            $this->usuariosModel->__SET("nombre", $_POST['nombre']);
            $this->usuariosModel->__SET("apellido", $_POST['apellido']);

            $res = $this->usuariosModel->actualizarUsuario();

            if ($res['n'] == 1) {
                $cambios = "nombreUsuario: " . strtolower($_POST['nombreUsuario']) . "<br>";
                $cambios .= "nombre: " . $_POST['nombre'] . "<br>";
                $cambios .= "apellido: " . $_POST['apellido'] . "<br>";
                $cambios .= "email: " . $_POST['email'];
                $this->logsModel->registrarLog("Actualización de datos de usuario.", $cambios);
            }

            header('Content-Type: application/json');
            echo json_encode($res);
        }
    }
    
    public function actualizarPregResSeguridad(){
        $this->usuariosModel->__SET("preguntaSeguridad", $_POST['preguntaSeguridad'] );
        $this->usuariosModel->__SET("respuestaSeguridad", $_POST['respuestaSeguridad'] );
        $this->usuariosModel->__SET("pass", hash("sha256", $_POST['pass']) );
        $res = $this->usuariosModel->cambiarPregResSeguridad();
        header("content-type: application/json");
        echo json_encode($res);
    }

    public function actualizarDatosUsuario() {
        $this->usuariosModel->__SET("idUsuario", $_POST['idUsuario']);
        $this->usuariosModel->__SET("tipoUsuario", $_POST['tipoUsuario']);
        $res = $this->usuariosModel->actualizarDatosUsuario();
        if ($res['n'] == 1) {
            $cambios = "Nuevo rol: " . $_POST['tipoUsuarioTexto'];
            $this->logsModel->registrarLog("Cambio de rol al usuario: " . $_POST['nombreUsuario'], $cambios);
        }
        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function listar() {
        $tipoUsuarios = $this->usuariosModel->consultarTipoUsuarios();
        
        $permisoSuperAdmin = $this->consultarPermisosSuperAdmin();
        $usuarios = $this->usuariosModel->listarUsuarios( $permisoSuperAdmin );
        $curadurias = $permisoSuperAdmin ? $this->ajustesModel->listarCuraduriasCrearUsuario() : false;
        
        $seccion = "Listar usuarios";
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/usuarios/listar.php';
        require APP . 'view/_templates/footer.php';
    }

    public function validarNombreUsuarioLibre() {
        $this->usuariosModel->__SET("nombreUsuario", strtolower($_POST['nombreUsuario']));
        $res = $this->usuariosModel->validarNombreUsuarioLibre($_POST['param']);
        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function validarEmailLibre() {
        $this->usuariosModel->__SET("email", $_POST['email']);
        $res = $this->usuariosModel->validarEmailLibre($_POST['param']);
        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function cambiarPass() {
        if ($_POST['btnActualizarPass'] == true) {

            $this->usuariosModel->__SET("idUsuario", $_SESSION['_idUsuario']);
            $res = $this->usuariosModel->consultarDatosUsuario(array("pass" => true));

            if ($res['pass'] == hash("sha256", $_POST['oldPass'])) {

                if (hash("sha256", $_POST['newPass']) === hash("sha256", $_POST['newPassReapeat'])) {
                    $this->usuariosModel->__SET("pass", hash("sha256", $_POST['newPass']));
                    $res = $this->usuariosModel->cambiarPass();
                    
                    if ($res['n'] == 1) {
                        $cambios = "Nueva contraseña: PROTEGIDA.";
                        $this->logsModel->registrarLog("Cambio de contraseña", $cambios);
                    }

                    header('Content-Type: application/json');
                    echo json_encode($res);
                } else {
                    header('Content-Type: application/json');
                    echo json_encode(array("res" => "noCoinciden"));
                }
            } else {
                header('Content-Type: application/json');
                echo json_encode(array("res" => "malPassActual"));
            }
        } else {
            echo false;
        }
    }

    public function cambiarEstadoUsuario() {
        $this->usuariosModel->__SET("idUsuario", $_POST['idUsuario']);
        $this->usuariosModel->__SET("estado", $_POST['estado']);
        $res = $this->usuariosModel->cambiarEstadoUsuario();
        
        if ($res['n'] == 1) {
            $_POST['estado'] === "true" ? $cambios = "Nuevo estado: Habilitado" : $cambios = "Nuevo estado: Deshabilitado";
            $this->logsModel->registrarLog("Cambio de estado al usuario: " . $_POST['nombreUsuario'], $cambios);
        }
        
        header('Content-Type: application/json');
        echo json_encode($res);
    }
    
    public function cambiarCuraduriaUsuario(){
        $this->usuariosModel->__SET("curaduria", $_POST['curaduria']);
        $this->usuariosModel->__SET("idUsuario", $_POST['idUsuario']);
        $res = $this->usuariosModel->cambiarCuraduriaUsuario();
        
        if (isset($res['n']) == 1) {
            $cambios = "idCuraduria a la que se traslada: " . $_POST['curaduria'];
            $this->logsModel->registrarLog("Cambio de curaduria al usuario: " . $_POST['nombreUsuario'], $cambios);
        }
        
        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function consultarDatosUsuario() {
        $this->usuariosModel->__SET("idUsuario", $_POST['idUsuario']);
        $res = $this->usuariosModel->consultarDatosUsuario(array("pass" => false, "estado" => false));
        header('content-type: application/json');
        echo json_encode($res);
    }

}