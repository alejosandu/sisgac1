<?php

/**
 * Description of tramites
 *
 * @author asanchez
 */
class tramites extends Controller{
    
    private $tramitesModel = null;
    private $archivosModel = null;
    private $ajustesModel = null;
    private $logsModel = null;
    
    function __construct() {
        parent::__construct();
        $this->tramitesModel = $this->loadModel("tramitesModel");
        $this->archivosModel = $this->loadModel("archivosModel");
        $this->ajustesModel = $this->loadModel("ajustesModel");
        $this->logsModel = $this->loadModel("logsModel");
    }
    
    public function listar($registrosPorPagina, $numeroPagina){
        
        if(!isset($registrosPorPagina) || !isset($numeroPagina)){
            header("location: " . URL . "tramites/listar/25/1");
        }
        
        if(isset($_POST['busqueda'])){
            $_SESSION['parametroBusqueda'] = array($_POST['parametro'] => $_POST['valor'] ) ;
        }else if( isset($_POST['parametro']) && $_POST['parametro'] == "reset"){
            unset($_SESSION['parametroBusqueda']);
        }else if( isset($_POST['busquedaRegex'])  ){
            if($_POST['parametro'] == "titular.numDocTitular"){
                $_SESSION['parametroBusqueda'] = array($_POST['parametro'] => $_POST['valor'] ) ;
            }else{
                $_SESSION['parametroBusqueda'] = array($_POST['parametro'] => array('$regex' => new MongoRegex("/.*" . $_POST['valor'] . ".*/i") ) ) ;
            }
        }else if(isset($_POST['busquedaFecha']) ){
            $_SESSION['parametroBusqueda'] = array($_POST['parametro'] => array('$gte' => new MongoDate(strtotime($_POST['valor'])) , '$lte' => new MongoDate( strtotime($_POST['valor']) + (60 * 60 * 23.999) )) ) ;
        }
        
        $tipoTramites = $this->ajustesModel->consultarTipoTramites();
        $tipos = null;
        foreach ($tipoTramites as $key => $value) {
            $tipos[] = array($key => $value['tipoTramite']);
        }
        
        $listaTramites = $this->tramitesModel->listarTramites($registrosPorPagina, $numeroPagina);
        $totalPaginas = $listaTramites['totalPaginas'];
        $totalTramites = $listaTramites['totalTramites'];
        $listaTramites = $listaTramites['tramites'];
        
        $permisoSuperAdmin = $this->consultarPermisosSuperAdmin();
        $permisoAdmin = $this->consultarPermisosAdmin();
        
        
        $seccion = "Inicio - Listar trámites";
        require APP . 'view/_templates/header.php';
        require APP . 'view/tramites/listar.php';
        require APP . 'view/_templates/footer.php';
    }
    
    public function crear(){
        $tipoTramites = $this->ajustesModel->consultarTipoTramites();
        $tipoDocIdentificacion = $this->ajustesModel->consultarIdentificaciones();
        $tipoModalidades = $this->ajustesModel->cargarTipoModalidades();
        $usosVivienda = $this->ajustesModel->cargarTipoUsoVivienda();
        $objetoTramite = $this->ajustesModel->cargarTipoObjetoTramite();
        $res = $this->ajustesModel->cargarBarriosVeredas();
        $barrios = $res["barrios"];
        $veredas = $res["veredas"];
        
        $seccion = "Crear nuevo trámite";
        require APP . 'view/_templates/header.php';
        require APP . 'view/tramites/crear.php';
        require APP . 'view/_templates/footer.php';
    }
    
    public function crearNuevo(){
        $esTramite = $_POST["esTramite"] == "true" ? true : false;
        
        $this->tramitesModel->__SET("esTramite", $esTramite );
        $this->tramitesModel->__SET("observaciones", $_POST["observaciones"]);
        $this->tramitesModel->__SET("idTipoTramites", $_POST["idTipoTramites"]);
        $this->tramitesModel->__SET("idTipoModalidades", $_POST["idTipoModalidades"]);
        $this->tramitesModel->__SET("idTipoUsosVivienda", $_POST["idTipoUsosVivienda"]);
        $this->tramitesModel->__SET("idObjetoTramite", $_POST["idObjetoTramite"]);
        $this->tramitesModel->__SET("folioInicial", $_POST["folioInicial"]);
        $this->tramitesModel->__SET("folioFinal", $_POST["folioFinal"]);
        $this->tramitesModel->__SET("totalFolios", $_POST["totalFolios"]);
        $this->tramitesModel->__SET("numConsecutivo", $_POST["numConsecutivo"]);
        $this->tramitesModel->__SET("anoRadicacion", $_POST["anoRadicacion"]);
        $this->tramitesModel->__SET("fechaExpedicion", $_POST["fechaExpedicion"]);
        $this->tramitesModel->__SET("fechaEjecutoria", $_POST["fechaEjecutoria"]);
        $this->tramitesModel->__SET("fechaLegalDebidaForma", $_POST["fechaLegalDebidaForma"]);
        $this->tramitesModel->__SET("predio", $_POST["predio"]);
        $this->tramitesModel->__SET("titular", $_POST["titular"]);
        $this->tramitesModel->__SET("solicitante", $_POST["solicitante"]);
        $res = $this->tramitesModel->crearNuevo();
        
        if (isset($res[0]['ok']) == 1) {
            $cambios = $esTramite ? "Trámite: " . $res[1] : "Documento: " . $res[1];
            $this->logsModel->registrarLog($esTramite ? "Creación de nuevo trámite en colección: colTramites" : "Creación de nuevo documento en colección: colTramites", $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
        die();
    }
    
    public function eliminar(){
        $this->tramitesModel->__SET("idTramite", $_POST['idTramite']);
        $res = $this->tramitesModel->eliminarTramite();
        if (isset($res["tramiteEliminado"]['ok']) == 1) {
            $cambios = "idTrámite: ". $_POST['cod'];
            $this->logsModel->registrarLog("Eliminación de trámite/documento con todos sus archivos en colección: colTramites", $cambios);
        }
        header("content-type: application/json");
        echo json_encode($res);
        die();
    }
    
    public function consultar($idTramite){
        if(!isset($idTramite) || strlen($idTramite) != 24){
            header("location: " . URL . "tramites/listar/25/1");
        }
        
        $tipoDocIdentificacion = $this->ajustesModel->consultarIdentificaciones();
        
        $tipos = $this->ajustesModel->consultarTipoTramites();
        $tipoTramites = null;
        $consultaTipoTramites = $tipos;
        foreach ($tipos as $key => $value) {
            $tipoTramites[] = array($key => $value['tipoTramite'] . ($value['tipoTramiteSigla'] ? " - " . $value['tipoTramiteSigla'] : null) );
        }
        
        $tipos = $this->ajustesModel->cargarTipoModalidades();
        $tipoModalidades = null;
        $consultaTipoModalidades = $tipos;
        foreach ($tipos as $key => $value) {
            $tipoModalidades[] = array($key => $value['tipoModalidad']);
        }
        
        $tipos = $this->ajustesModel->cargarTipoUsoVivienda();
        $tipoUsos = null;
        $consultaTipoUsosVivienda = $tipos;
        foreach ($tipos as $key => $value) {
            $tipoUsos[] = array($key => $value['usoVivienda']);
        }
        
        $tipos = $this->ajustesModel->cargarTipoObjetoTramite();
        $tipoObjetos = null;
        $consultaTipoObjetotramite = $tipos;
        foreach ($tipos as $key => $value) {
            $tipoObjetos[] = array($key => $value['tipoObjetoTramite']);
        }
        
        $barriosVeredas = $this->ajustesModel->cargarBarriosVeredas();
        $barrios = $barriosVeredas['barrios'];
        $veredas = $barriosVeredas['veredas'];
        
        $this->tramitesModel->__SET("idTramite" , $idTramite);
        $tramite = $this->tramitesModel->consultar();
        $this->archivosModel->__SET("idTramite" , $idTramite);
        $archivos = $this->archivosModel->listarArchivos();
        
        $permisoSuperAdmin = $this->consultarPermisosSuperAdmin();
        $permisoAdmin = $this->consultarPermisosAdmin();
        
        $seccion = "Consulta de " . (isset($tramite['codigoFormUnicNac']) ? "trámite " . $tramite['codigoFormUnicNac'] : "documento " . $tramite['codigoDocumento']);
        require APP . 'view/_templates/header.php';
        require APP . 'view/tramites/consultar.php';
        require APP . 'view/_templates/footer.php';
    }
    
    public function editar(){
        $esTramite = $_POST["esTramite"] == "true" ? true : false;
        $this->tramitesModel->__SET("esTramite", $esTramite );
        $this->tramitesModel->__SET("idTramite" , $_POST['idTramite']);
        if($esTramite){
            $this->tramitesModel->__SET("idTipoTramites", $_POST["idTipoTramites"]);
            $this->tramitesModel->__SET("idTipoModalidades", $_POST["idTipoModalidades"]);
            $this->tramitesModel->__SET("idTipoUsosVivienda", $_POST["idTipoUsosVivienda"]);
            $this->tramitesModel->__SET("idObjetoTramite", $_POST["idObjetoTramite"]);
            $this->tramitesModel->__SET("folioInicial", $_POST["folioInicial"]);
            $this->tramitesModel->__SET("folioFinal", $_POST["folioFinal"]);
            $this->tramitesModel->__SET("totalFolios", $_POST["totalFolios"]);
            $this->tramitesModel->__SET("fechaExpedicion", $_POST["fechaExpedicion"]);
            $this->tramitesModel->__SET("fechaEjecutoria", $_POST["fechaEjecutoria"]);
            $this->tramitesModel->__SET("fechaLegalDebidaForma", $_POST["fechaLegalDebidaForma"]);
            $this->tramitesModel->__SET("predio", $_POST["predio"]);
            $this->tramitesModel->__SET("titular", $_POST["titular"]);
            $this->tramitesModel->__SET("solicitante", $_POST["solicitante"]);
        }else{
            $this->tramitesModel->__SET("observaciones", $_POST["observaciones"]);
            $this->tramitesModel->__SET("folioInicial", $_POST["folioInicial"]);
            $this->tramitesModel->__SET("folioFinal", $_POST["folioFinal"]);
            $this->tramitesModel->__SET("totalFolios", $_POST["totalFolios"]);
            $this->tramitesModel->__SET("fechaExpedicion", $_POST["fechaExpedicion"]);
            $this->tramitesModel->__SET("predio", $_POST["predio"]);
            $this->tramitesModel->__SET("titular", $_POST["titular"]);
        }
        $res = $this->tramitesModel->editarTramite();
        if (isset($res['ok']) == 1) {
            $cambios = $esTramite ? "Trámite: <a href='" . URL . "tramites/consultar/" . $_POST['idTramite'] . "' target='blank'>" . $_POST['idTramite'] ."</a>" : "Documento: <a href='" . URL . "tramites/consultar/" . $_POST['idTramite'] . "' target='blank'>" . $_POST['idTramite'] ."</a>";
            $this->logsModel->registrarLog($esTramite ? "Edición de trámite en colección: colTramites" : "Edición de documento en colección: colTramites", $cambios);
        }
        header("content-type: application/json");
        echo json_encode($res);
    }
    
}
