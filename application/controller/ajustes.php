<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajustes
 *
 * @author asanchez
 */
class ajustes extends Controller{
    
    private $ajustesModel = null;
    private $logsModel = null;
    
    function __construct() {
        parent::__construct();
        $this->ajustesModel = $this->loadModel("ajustesModel");
        $this->logsModel = $this->loadModel("logsModel");
    }
    
    public function index(){
        header("location: " . URL . "tramites/listar");
    }
    
    public function crearRegistro(){
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearRegistro();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nuevo ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarRegistro(){
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarRegistro();
        
        if ($res['n'] == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
            
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function crearDepartamento(){
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearDepartamento();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nuevo ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarDepartamento(){
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarDepartamento();
        
        if (isset($res['n']) == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
            
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function crearMunicipio(){
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("idDepartamento" , $_POST['idDepartamento']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearMunicipio();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nuevo ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarMunicipio(){
        $this->ajustesModel->__SET("idDepartamento" , $_POST['idDepartamento']);
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarMunicipio();
        
        if (isset($res['n']) == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
            
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function crearBarrio(){
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearBarrio();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nuevo ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarBarrio(){
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("coleccion" , $_POST['coleccion']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarBarrio();
        
        if (isset($res['n']) == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de ajuste en colección: " . $_POST['coleccion'], $cambios);
        }
            
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function crearVereda(){
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearVereda();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nueva vereda en colección: colDepartamentos", $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarVereda(){
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarVereda();
        
        if (isset($res['n']) == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de vereda en colección: colDepartamentos", $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function crearCuraduria(){
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->crearCuraduria();
        
        if (isset($res['ok']) == 1) {
            $cambios = print_r( $_POST['valor'] , true);
            $this->logsModel->registrarLog("Creación de nueva curaduría en colección: colDepartamentos", $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function actualizarCuraduria(){
        $this->ajustesModel->__SET("idMunicipio" , $_POST['idMunicipio']);
        $this->ajustesModel->__SET("idAjuste", $_POST['idAjuste']);
        $this->ajustesModel->__SET("valor" , $_POST['valor']);
        $res = $this->ajustesModel->actualizarCuraduria();
        
        if (isset($res['n']) == 1) {
            $cambios = "_id:" . $_POST['idAjuste'] . "<br>" . print_r( $_POST['valor'], true);
            $this->logsModel->registrarLog("Actualización de curaduría en colección: colDepartamentos", $cambios);
        }
        
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function cargarAjusteEspecifico(){
        $res = $this->ajustesModel->$_POST['metodo']();
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function cargarIdentificaciones(){
        $identificaciones = $this->ajustesModel->consultarIdentificaciones();
        require APP . 'view/_templates/modals/ajustesIdPersonal.php';
    }
    
    public function cargarTipoTramites(){
        $tipoTramites = $this->ajustesModel->consultarTipoTramites();
        require APP . 'view/_templates/modals/ajustesTipoTramites.php';
        
    }
    
    public function cargarTipoModalidades(){
        $modalidades = $this->ajustesModel->cargarTipoModalidades();
        require APP . 'view/_templates/modals/ajustesTipoModalidades.php';
    }
    
    public function cargarTipoUsoVivienda(){
        $usosVivienda = $this->ajustesModel->cargarTipoUsoVivienda();
        require APP . 'view/_templates/modals/ajustesTipoUsoVivienda.php';
    }
    
    public function cargarTipoObjetoTramite(){
        $objetos = $this->ajustesModel->cargarTipoObjetoTramite();
        require APP . 'view/_templates/modals/ajustesTipoObjetoTramite.php';
    }
    
    public function cargarDepartamentos(){
        $departamentos = $this->ajustesModel->cargarDepartamentos();
        require APP . 'view/_templates/modals/ajustesDepartamentos.php';
    }
    
    public function cargarMunicipios(){
        if(isset($_POST['idDepartamento'])){
            $this->ajustesModel->__SET( "idDepartamento" , $_POST['idDepartamento'] );
            $municipios = $this->ajustesModel->cargarMunicipios();
            header("content-type: application/json");
            echo json_encode($municipios);
        }else{
            $departamentos = $this->ajustesModel->cargarDepartamentos();
            require APP . 'view/_templates/modals/ajustesMunicipios.php';
        }
        
    }
    
    public function cargarBarrios(){
        if(isset($_POST['idMunicipio'])){
            $this->ajustesModel->__SET( "idMunicipio" , $_POST['idMunicipio'] );
            $municipios = $this->ajustesModel->cargarBarrios();
            header("content-type: application/json");
            echo json_encode($municipios);
        }else{
            $departamentos = $this->ajustesModel->cargarDepartamentos();
            $municipios = $this->ajustesModel->cargarMunicipios();
            require APP . 'view/_templates/modals/ajustesBarrios.php';
        }
    }
    
    public function cargarVeredas(){
        if(isset($_POST['idMunicipio'])){
            $this->ajustesModel->__SET( "idMunicipio" , $_POST['idMunicipio'] );
            $municipios = $this->ajustesModel->cargarVeredas();
            header("content-type: application/json");
            echo json_encode($municipios);
        }else{
            $departamentos = $this->ajustesModel->cargarDepartamentos();
            $municipios = $this->ajustesModel->cargarMunicipios();
            require APP . 'view/_templates/modals/ajustesVeredas.php';
        }
    }
    
    public function cargarCuradurias(){
        if(isset($_POST['idMunicipio'])){
            $this->ajustesModel->__SET( "idMunicipio" , $_POST['idMunicipio'] );
            $municipios = $this->ajustesModel->cargarCuradurias();
            header("content-type: application/json");
            echo json_encode($municipios);
        }else{
            $departamentos = $this->ajustesModel->cargarDepartamentos();
            $municipios = $this->ajustesModel->cargarMunicipios();
            require APP . 'view/_templates/modals/ajustesCuradurias.php';
        }
    }
}
