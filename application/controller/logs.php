<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of logs
 *
 * @author asanchez
 */
class logs extends Controller{
    
    private $logsModel = null;
    
    function __construct(){
        parent::__construct();
        $this->logsModel = $this->loadModel("logsModel");
    }
    
    public function listar($registrosPagina,$numPagina,$idLog,$autor,$fecha){
        
        if(!isset($registrosPagina) || !isset($numPagina) || !isset($idLog)|| !isset($autor) || !isset($fecha)){
            header('location: ' . URL . 'logs/listar/25/1/0/todos/0');
        }
        
        $fecha = $fecha ? $fecha : 0;
        
        $numPagina = $numPagina > 0 ? $numPagina : 1;
        
        $registroRecorridos = ($numPagina - 1) * $registrosPagina;
        
        $this->logsModel->__SET("autor", $autor ? $autor : "todos");
        $this->logsModel->__SET("fecha", $fecha ? $fecha : 0);
        $this->logsModel->__SET("idLog", $idLog ? $idLog : 0);
        $this->logsModel->__SET("registroRecorridos", $registroRecorridos);
        $this->logsModel->__SET("registrosPagina" , $registrosPagina);
        $res = $this->logsModel->consultarLogs();
        
        $logs = $res['registros'];
        
        $totalPaginas = $res['totalPaginas'];
        
        $seccion = "Listar logs de actividades";
        
        require APP .'view/_templates/header.php';
        require APP .'view/logs/listar.php';
        require APP .'view/_templates/footer.php';
    }
}
