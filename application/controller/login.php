<?php

class Login extends Controller {

    private $loginModel = null;
    private $logsModel = null;

    function __construct() {
        $this->loginModel = $this->loadModel("loginModel");
        $this->logsModel = $this->loadModel("logsModel");
    }

    public function index() {
        if (isset($_SESSION["nombreUsuario"])) {
            header("location: " . URL . "tramites/listar");
        } else {
            $seccion = "Login";
            require APP . 'view/login/index.php';
        }
    }
    
    public function entrar() {
        $this->loginModel->__SET("nombreUsuario", strtolower($_POST['nombreUsuario']));
        $this->loginModel->__SET("pass", hash("sha256", $_POST['pass']));
        $res = $this->loginModel->entrar();

        if ($res['res'] === true) {
            //registro nuevo usuario en el log
            $cambios = "El usuario: " . strtolower($_POST['nombreUsuario']) . " ha iniciado sesión.<br>Ip: " . ($_SERVER['REMOTE_ADDR'] == '::1' ? "Servidor Localhost" : $_SERVER['REMOTE_ADDR']) ;
            $this->logsModel->registrarLog("Inicio de sesión", $cambios);
        }

        header('Content-Type: application/json');
        echo json_encode($res);
    }

    public function cerrar() {
        //registro nuevo usuario en el log
        $cambios = "El usuario: " . $_SESSION['nombreUsuario'] . " ha cerrado sesión.<br>Ip: " . ($_SERVER['REMOTE_ADDR'] == '::1' ? "Servidor Localhost" : $_SERVER['REMOTE_ADDR']) ;
        $this->logsModel->registrarLog("Cierre de sesión", $cambios);
        session_destroy();
        header("location: " . URL . "login");
    }

}
