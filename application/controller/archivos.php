<?php

class archivos extends Controller{
    
    private $archivosModel = null;
    private $logsModel = null;
    
    function __construct() {
        
        parent::__construct();
        $this->archivosModel = $this->loadModel("archivosModel");
        $this->logsModel = $this->loadModel("logsModel");
    }
    
    public function subir(){
        
        if(isset($_FILES["archivo"])){
                $this->archivosModel->__SET("idTramite",$_POST["idTramite"]);
                $this->archivosModel->__SET("nombreArchivo",$_POST["nombreArchivo"]);
                $this->archivosModel->__SET("archivo",$_FILES["archivo"]);
                $res = $this->archivosModel->subirArchivo();
                if(is_array($res)){
                    $cambios = "Info archivos: " . print_r($_FILES["archivo"]['name'],true);
                    $this->logsModel->registrarLog("Subida de archivos", $cambios);
                    header("content-type: application/json");
                    echo true;
                }else{
                    //SI NO ES ARRAY SIGNIFICA QUE HUBO ERROR
                    header("content-type: application/json");
                    echo $res;
                }
                die();
            }else{
                header("content-type: application/json");
                echo "No se encontraron archivos para subir";
                die();
            }
    }
    
    public function eliminar(){
        $this->archivosModel->__SET("idArchivo",$_POST["idArchivo"]);
        $res = $this->archivosModel->eliminarArchivo();
        if($res == true){
            $cambios = "Archivo: " . $_POST["archivo"];
            $this->logsModel->registrarLog("Eliminación de archivo", $cambios);
        }
        header("content-type: application/json");
        echo $res;
    }
    
    public function consultar($idArchivo, $nombreArchivo){
        
        $this->archivosModel->__SET("idArchivo",$idArchivo);
        $res = $this->archivosModel->consultarArchivo();
        $stream = $res->getResource();      
        foreach ($res as $archivo) {
            if($archivo["length"] > 50 * (1024**2)){
                header('Content-Disposition: attachment; filename="'.$archivo["filename"].'"');
            }else{
                header('Content-Disposition: inline; filename="'.$archivo["filename"].'"');
            }
            header('Content-type: '.$archivo["filetype"].';');
            while (!feof($stream)) {
                echo fread($stream, $archivo["length"]);
            }
        }
        fclose($stream);
    }
        
}
