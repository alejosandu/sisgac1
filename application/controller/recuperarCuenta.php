<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of recuperarCuenta
 *
 * @author asanchez
 */
class recuperarCuenta extends Controller {
    private $usuariosModel = null;
    private $logsModel = null;
    function __construct() {
        $this->usuariosModel = $this->loadModel("usuariosModel");
        $this->logsModel = $this->loadModel("logsModel");
    }
    
    public function recuperar(){
        $this->usuariosModel->__SET("nombreUsuario", strtolower($_POST['nombreUsuario']));
        $this->usuariosModel->__SET("email",$_POST['email']);
        $newPass = new MongoId();
        $newPass = hash("sha256",$newPass);
        $newPass = substr($newPass, -16);
        $this->usuariosModel->__SET("pass", $newPass);
        $res = $this->usuariosModel->recuperarCuenta();
        if($res){
            try{
                $this->enviarEmail($_POST['nombreUsuario'], $newPass, $_POST['email']);
            }catch(Exception $e){}
            //registro log del cambio de contraseña para saber si hubo intentos
            $cambios = "nombreUsuario: " . strtolower($_POST['nombreUsuario']) . "<br>";
            $cambios .= "email: " . $_POST['email'];
            $this->logsModel->registrarLog("Recuperación de cuenta para el usuario " . strtolower($_POST['nombreUsuario']) , $cambios);
        }
        header("content-type: application/json");
        echo json_encode($res);
    }
    
    public function enviarEmail($nombreUsuario,$pass,$email){
        try{
            require_once APP . 'libs/PHPMailer/PHPMailerAutoload.php';
            require_once APP . 'libs/PHPMailer/class.phpmailer.php';
            require_once APP . 'libs/PHPMailer/class.smtp.php';

            $mailer = new PHPMailer();
            $mailer->IsSMTP();
            $mailer->SMTPAuth = true;
            $mailer->SMTPSecure = "ssl";
            $mailer->Host = "smtp.gmail.com";
            $mailer->Port = 465;
//            $mailer->SMTPDebug = 0;
//            $mailer->Debugoutput = "html";
            $mailer->Username = "alejosandu@gmail.com";
            $mailer->Password = "103513195asd";
            
            $mailer->FromName = "no-reply";
            $mailer->Subject = "Recuperación de cuenta";
            $mailer->addAddress($email,$nombreUsuario);
            $mailer->CharSet = "UTF-8";
            $mensaje = "Hola " . $nombreUsuario . "<br>";
            $mensaje .= "Se ha solicitado una recuperación de cuenta por medio de cambio de contraseña<br>";
            $mensaje .= "Su usuario para ingresar es: <strong>" . $nombreUsuario . "</strong><br>";
            $mensaje .= "Su contraseña para ingresar es: <strong>" . $pass . "</strong><br>";
            $mensaje .= "Nota: se recomienda que una vez ingrese al sistema cambie la contraseña<br>";
            $mensaje .= "<strong>NO RESPONDA A ESTE MENSAJE</strong><br>";
            $mensaje .= "Si ha recibido este mensaje por error por favor hacer caso omiso a este.";
            $mailer->msgHTML($mensaje);
            $mailer->send();
            return true;
        }catch(Exception $e){
            return false;
        }
    }
}
