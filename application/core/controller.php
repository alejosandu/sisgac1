<?php

class Controller
{
    /**
     * @var null Database Connection
     */
    public $db = null;
    
    private $user = "sisgac";
    private $pwd = "c1envigado";
    private $host = "localhost";
    private $port = "27017";
    private $dbName = "sisgac";
    /**
     * @var null Model
     */
    public $menuInController = null;
    
    public $permisos = null;
    /**
     * Whenever controller is created, open a database connection too and load "the model".
     */
    function __construct()
    {
        date_default_timezone_set("America/Bogota");
        $this->verificarSesion();
        $this->menuInController = $this->crearMenu();
        $this->permisos = $this->loadModel("permisosModel");
    }
    
    public function verificarSesion(){
        if(!isset($_SESSION["nombreUsuario"])){
            session_destroy();
            header("location: ".URL."login");
        }
    }
    
    public function consultarPermisos($controlador,$metodo){
        $permisos = $this->loadModel("permisosModel");
        $permisos->__SET("controlador", $controlador);
        $permisos->__SET("metodo", $metodo);
        $res = $permisos->consultarPermisos();
        unset($permisos);
        return $res;
    }
    
    public function consultarPermisosSuperAdmin(){
        $permisos = $this->loadModel("permisosModel");
        $res = $permisos->consultarPermisosSuperAdmin();
        unset($permisos);
        return $res;
    }
    
    public function consultarPermisosAdmin(){
        $permisos = $this->loadModel("permisosModel");
        $res = $permisos->consultarPermisosAdmin();
        unset($permisos);
        return $res;
    }
    
    public function crearMenu(){
        $menuModel = $this->loadModel("menuModel");
        $menu = $menuModel->consultarMenu();
        return $menu;
    }
    
    /**
     * Open the database connection with the credentials from application/config/config.php
     */
    private function openDatabaseConnection()
    {
        try{
            $client = new MongoClient("mongodb://" . $this->user . ":" . $this->pwd . "@" . $this->host . ":" . $this->port);
            $this->db = new MongoDB($client, $this->dbName);
        }  catch (MongoException $e){
            var_dump($e);
            exit("No se puso realizar la conexión a la base de datos.");
        }
        
    }

    /**
     * Loads the "model".
     * @return object model
     */
    public function loadModel($modelo)
    {
        $this->openDatabaseConnection();
        require_once APP . 'model/'.$modelo.'.php';
        // create new "model" (and pass the database connection)
        return new $modelo($this->db);
    }
}
