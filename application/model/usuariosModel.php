<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of usuariosModel
 *
 * @author asanchez
 * 
 * ESTA CLASE CONTIENE MÉTODOS PARA colUsuarios y colTipoUsuarios
 * 
 */
class usuariosModel {

    private $db = null;
    private $colUsuarios = null;
    private $colTipoUsuarios = null;
    private $idUsuario = null;
    private $nombreUsuario = null;
    private $pass = null;
    private $nombre = null;
    private $apellido = null;
    private $email = null;
    private $preguntaSeguridad = null;
    private $respuestaSeguridad = null;
    private $tipoUsuario = null;
    private $curaduria = null;

    public function __construct($db) {
        try {
            $this->db = $db;
            $this->colTipoUsuarios = $this->db->selectCollection("colTipoUsuarios");
            $this->colUsuarios = $this->db->selectCollection("colUsuarios");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }

    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }

    public function consultarTipoUsuarios() {
        $superAdminId = $this->colTipoUsuarios->findOne(array("tipoUsuario" => "Super Administrador") , array("_id" => true));
        $where = array('$or' => array(array("tipoUsuario" => array('$ne' => "Super Administrador")), array("_id" => $superAdminId)));
        $res = $this->colTipoUsuarios->find( $where );
        return $res;
    }

    //esta función al llamarse SIEMPRE debe tener un array en el parámetro aunque no contenga nada
    public function consultarDatosUsuario($campos) {
        $where = array("_id" => new MongoId($this->idUsuario));
        if($campos){
            $res = $this->colUsuarios->findOne($where, $campos);
        }else{
            $res = $this->colUsuarios->findOne($where);
        }
        return $res;
    }
    
    public function listarUsuarios($permisoSuperAdmin){
        $res = $this->colUsuarios->findOne();
        $where = array( "_id" => array( '$nin' => array( new MongoId($_SESSION['_idUsuario']) , $res['_id'] ) ) );
        if(isset($permisoSuperAdmin)){
            if(!$permisoSuperAdmin){
                $where = array('$and' => array( $where , array("curaduria" => $_SESSION['curaduria'])));
            }
        }
        
        $usuarios = $this->colUsuarios->find( $where ,array("pass" => false));
        return iterator_to_array($usuarios);
    }

    public function crearNuevo() {
        //primeros valores
        $valores = array(
            "nombreUsuario" => $this->nombreUsuario,
            "pass" => hash("sha256", $this->pass),
            "nombre" => $this->nombre,
            "apellido" => $this->apellido,
            "email" => $this->email,
            "tipoUsuario" => $this->tipoUsuario,
            "estado" => true,
            "curaduria" => $this->curaduria
        );
        //formateo con set para que no se borre por completo la información 
        //con los valores anteriormente configurados para actualizar
        $valores = array('$set' => $valores);

        //consulto si usuario existe
        $cont = $this->colUsuarios->count(array("nombreUsuario" => $this->nombreUsuario));
        //consulto si email existe
        $contMail = $this->colUsuarios->count(array("email" => $this->email));

        if ($cont == 0 && $contMail == 0) {
            //si no existe guardo
            $res = $this->colUsuarios->update(array("nombreUsuario" => $this->nombreUsuario), $valores, array("upsert" => true));
        } else if($cont > 0) {
            //si ya existe retorno true
            $res = array("usuarioExiste" => true);
        } else if($contMail > 0){
            $res = array("emailExiste" => true);
        }
        return $res;
    }
    
    public function eliminar(){
        $where = array("nombreUsuario" => strtolower($this->nombreUsuario));
        $res = $this->colUsuarios->remove( $where );
        if($res["ok"] == 1){
            return true;
        }else{
            return false;
        }
    }

    public function validarNombreUsuarioLibre($param){
        if($param == "true"){
            $where = array('$and' => array(array("nombreUsuario" => strtolower($this->nombreUsuario)), array("_id" => array('$ne' => new MongoId($_SESSION["_idUsuario"])))));
        }else{
            $where = array("nombreUsuario" => strtolower($this->nombreUsuario));
        }
        $res = $this->colUsuarios->count( $where );
        return $res;
    }
    
    public function validarEmailLibre($param){
        if($param == "true"){
            $where = array('$and' => array(array("email" => $this->email), array("_id" => array('$ne' => new MongoId($_SESSION["_idUsuario"])))));
        }else{
            $where = array("email" => $this->email);
        }
        $res = $this->colUsuarios->count( $where );
        return $res;
    }
    
    public function actualizarUsuario() {
        //configuro valores iniciales
        $valores = array(
            "nombreUsuario" => $this->nombreUsuario,
            "email" => $this->email,
            "nombre" => $this->nombre,
            "apellido" => $this->apellido
        );
        //formateo valores con operador $set
        $valores = array('$set' => $valores);
        //ajuste de sentencia where en base al id en la sesión
        $where = array("_id" => new MongoId($this->idUsuario));
        //verifico que el nombre de usuario no esté siendo usado por otra persona
        $cont = $this->colUsuarios->count(array('$and' => array(array("nombreUsuario" => strtolower($this->nombreUsuario)), array("_id" => array('$ne' => new MongoId($this->idUsuario))))));
        
        $contMail = $this->colUsuarios->count(array('$and' => array(array("email" => $this->email), array("_id" => array('$ne' => new MongoId($this->idUsuario))))));
        
        if($cont == 0 && $contMail == 0){
            $_SESSION['nombreUsuario'] = $this->nombreUsuario;
            $res = $this->colUsuarios->update($where,$valores);
        }else if($cont > 0){
            $res = array("usuarioExiste" => true);
        }else if($contMail > 0){
            $res = array("emailExiste" => true);
        }
        
        //actualizo

        return $res;
    }
    
    public function actualizarDatosUsuario(){
        $where = array("_id" => new MongoId($this->idUsuario));
        $valores = array(
            "tipoUsuario" => $this->tipoUsuario
        );
        $valores = array('$set' => $valores);
        $res = $this->colUsuarios->update( $where , $valores );
        return $res;
    }
    
    public function cambiarPass(){
        $valores = array(
            "pass" => $this->pass
        );
        $valores = array('$set' => $valores);
        $where = array("_id" => new MongoId($_SESSION['_idUsuario']));
        $res = $this->colUsuarios->update( $where , $valores );
        return $res;
    }
    
    public function cambiarEstadoUsuario(){
        $where = array("_id" => new MongoId($this->idUsuario));
        $valores = array('$set' => array("estado" => $this->estado === "true" ? true : false));
        $res = $this->colUsuarios->update( $where , $valores);
        return $res;
    }
    
    public function cambiarCuraduriaUsuario(){
        $where = array("_id" => new MongoId($this->idUsuario));
        $valores = array('$set' => array( "curaduria" => $this->curaduria ));
        $res = $this->colUsuarios->update( $where , $valores);
        return $res;
    }
    
    public function recuperarCuenta(){
        $where = array('$and' => array(array("nombreUsuario" => $this->nombreUsuario),array("email" => $this->email)));
        $idUsuario = $this->colUsuarios->findOne( $where , array("_id" => true) );
        if(isset($idUsuario['_id'])){
            $where = array("_id" => $idUsuario['_id'] );
            $valores = array("pass" => hash("sha256",$this->pass));
            $valores = array('$set' => $valores);
            $res = $this->colUsuarios->update( $where , $valores );
            if($res['n'] == 1){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}
