<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of menuModel
 *
 * @author asanchez
 */
class menuModel {
    
    private $db = null;
    private $colTipoUsuarios = null;
    
    function __construct($db) {
        try {
            $this->db = $db;
            $this->colTipoUsuarios = $this->db->selectCollection("colTipoUsuarios");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function consultarMenu(){
        $where = array('_id' => new MongoId($_SESSION['tipoUsuario']));
        
        $modulos = iterator_to_array($this->colTipoUsuarios->find( $where , array("modulos" => true , "_id" => false) ));
        $menu = null;        
        foreach ($modulos as $valor){
            $menu = $valor;
        }
        return $menu;
    }
}
