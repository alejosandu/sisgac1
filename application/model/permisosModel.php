<?php

class permisosModel {
    
    private $db = null;
    private $colTipoUsuarios = null;
    
    private $tipoUsuario = null;
    private $ctrlxMetodos = null;
    private $modulos = null;
    
    private $controlador = null;
    private $metodo = null;
    
    function __construct($db) {
        try {
            $this->db = $db;
            $this->colTipoUsuarios = $this->db->selectCollection("colTipoUsuarios");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function consultarPermisos(){
        
        $where = array( '$and' => array( array('_id' => new MongoId($_SESSION['tipoUsuario']) ) , array("ctrlxMetodos" => $this->controlador . "/" . $this->metodo) ) );
        
        $cont = $this->colTipoUsuarios->count( $where );
        
        if($cont > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function consultarPermisosSuperAdmin(){
        $superAdminId = $this->colTipoUsuarios->findOne(array(), array("_id" => true));
        return $_SESSION['tipoUsuario'] == $superAdminId["_id"]->__toString() ? true : false;
    }
    public function consultarPermisosAdmin(){
        $adminId = $this->colTipoUsuarios->find(array(), array("_id" => true))->skip(1)->limit(1);
        foreach ($adminId as $key => $value) {
            $adminId = $value;
        }
        return $_SESSION['tipoUsuario'] == $adminId["_id"]->__toString() ? true : false;
    }
    
}