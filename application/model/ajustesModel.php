<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajustesModel
 *
 * @author asanchez
 */
class ajustesModel {
    
    private $db = null;
    
    private $idDepartamento = null;
    private $idMunicipio = null;
    private $idBarrio = null;
    private $idAjuste = null;
    private $coleccion = null;
    private $valor = null;
    
    function __construct($db) {
        $this->db = $db;
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function crearRegistro(){
        $res = $this->db->selectCollection($this->coleccion)->insert( $this->valor );
        return $res;
    }
    
    public function actualizarRegistro() {
        $where = array( "_id" => new MongoId($this->idAjuste) );
        $valores = array('$set' => $this->valor);
        $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
        return $res;
    }
    
    public function crearDepartamento(){
        $cont = $this->db->selectCollection($this->coleccion)->count( array("codigoDepartamento" => $this->valor['codigoDepartamento']) );
        if($cont == 0){
            $res = $this->db->selectCollection($this->coleccion)->insert( $this->valor );
        }else{
            $res = array("mensaje" => "El código DANE ya está registrado para otro departamento");
        }
        return $res;
    }
    
    public function actualizarDepartamento() {
        $where = array('$and' => array( array("codigoDepartamento" => $this->valor['codigoDepartamento']) , array("_id" => array('$ne' => new MongoId($this->idAjuste) )) ));
        $cont = $this->db->selectCollection($this->coleccion)->count( $where );
        if($cont > 0){
            $res = array("mensaje" => "El código DANE ya está registrado para otro departamento.");
        }else{
            $where = array( "_id" => new MongoId($this->idAjuste) );
            $valores = array('$set' => $this->valor);
            $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
        }
        return $res;
    }
    
    public function crearMunicipio(){
        $where = array('$and' => array( array("municipios.codigoMunicipio" => $this->valor['municipios']['codigoMunicipio']) , array("_id" => new MongoId($this->idDepartamento)) ));
        
        $cont = $this->db->selectCollection($this->coleccion)->count( $where );
        
        if($cont == 0){
            $where = array('_id' => new MongoId($this->idDepartamento));
            $valores = array(
                "municipios" => array( 
                    "_id" => new MongoId() , 
                    "nombreMunicipio" => $this->valor['municipios']['nombreMunicipio'] , 
                    "codigoMunicipio" => $this->valor['municipios']['codigoMunicipio'] 
                )
            );
            $res = $this->db->selectCollection($this->coleccion)->update( $where , array('$push' => $valores) );
        }else{
            $res = array("mensaje" => "El código DANE ya está registrado para otro municipio");
        }
        return $res;
    }
    
    public function actualizarMunicipio() {
        $where = array("municipios._id" => new MongoId($this->idAjuste));
        $res = $this->db->selectCollection($this->coleccion)->findOne( $where , array("municipios._id" => true, "municipios.nombreMunicipio" => true, "municipios.codigoMunicipio" => true , "_id" => false) );
        $municipios = $res['municipios'];
        for($i = 0; $i < sizeof($municipios); $i++){
            
            if($municipios[$i]['_id']->__toString() === $this->idAjuste){
                
                if($municipios[$i]['codigoMunicipio'] === $this->valor['municipios']['codigoMunicipio']){
                    
                    $where = array( "municipios._id" => new MongoId($this->idAjuste) );
                    $valores = array(
                        'municipios.$' => array( "_id" => new MongoId($this->idAjuste) , "nombreMunicipio" => $this->valor['municipios']['nombreMunicipio'] , "codigoMunicipio" => $this->valor['municipios']['codigoMunicipio'] )
                    );
                    $valores = array('$set' => $valores);
                    $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
                    return $res;
                }else{
                    
                    $where = array( 'municipios.codigoMunicipio' => $this->valor['municipios']['codigoMunicipio'] );
                    $cont = $this->db->selectCollection($this->coleccion)->count( $where );
                    if($cont > 0){
                        $res = array("mensaje" => "El código DANE ya está registrado para otro municipio.");
                        return $res;
                    }else if($cont == 0){
                        $where = array( "municipios._id" => new MongoId($this->idAjuste) );

                        $valores = array(
                            'municipios.$' => array( "_id" => new MongoId($this->idAjuste) , "nombreMunicipio" => $this->valor['municipios']['nombreMunicipio'] , "codigoMunicipio" => $this->valor['municipios']['codigoMunicipio'] )
                        );
                        $valores = array('$set' => $valores);
                        $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
                        return $res;
                    }
                }
            }
        }
    }
    
    public function crearBarrio(){
        $where = array("municipios._id" => new MongoId($this->idMunicipio));
        
        $res = $this->db->selectCollection($this->coleccion)->findOne( $where );
        
        $barrios = null;
        for($i = 0; $i < sizeof($res['municipios']); $i++){
            
            if( $res['municipios'][$i]['_id']->__toString() == $this->idMunicipio){
                
                if(isset($res['municipios'][$i]['barrios'])){
                    
                    $barrios = $res['municipios'][$i]['barrios'];
                    break;
                    
                }else{
                    
                    $where = array('municipios._id' => new MongoId($this->idMunicipio));
                    $valores = array(
                        "municipios.$.barrios" => array( 
                            "_id" => new MongoId() , 
                            "nombreBarrio" => $this->valor['municipios']['barrios']['nombreBarrio'], 
                            "codigoBarrio" => $this->valor['municipios']['barrios']['codigoBarrio'] 
                        )
                    );
                    return $this->db->selectCollection($this->coleccion)->update( $where , array('$push' => $valores) );
                    
                }
                
            }
        }
        
        $cont = 0;
        for($i = 0; $i < sizeof($barrios);$i++){
            if( $barrios[$i]['codigoBarrio'] == $this->valor['municipios']['barrios']['codigoBarrio'] ){
                $cont = 1;
                break;
            }
        }
        
        $res = null;
        
        if($cont == 0){
            $where = array('municipios._id' => new MongoId($this->idMunicipio));
            $valores = array(
                "municipios.$.barrios" => array( 
                    "_id" => new MongoId() , 
                    "nombreBarrio" => $this->valor['municipios']['barrios']['nombreBarrio'], 
                    "codigoBarrio" => $this->valor['municipios']['barrios']['codigoBarrio'] 
                )
            );
            $res = $this->db->selectCollection($this->coleccion)->update( $where , array('$push' => $valores) );
        }else{
            $res = array("mensaje" => "El código ya está registrado para otro barrio");
        }
        return $res;
    }
    
    public function actualizarBarrio() {
        $where = array("municipios.barrios._id" => new MongoId($this->idAjuste));
        $res = $this->db->selectCollection($this->coleccion)->findOne( $where );
        for($j = 0; $j < sizeof($res['municipios']); $j++){
            if($res['municipios'][$j]['_id']->__toString() == $this->idMunicipio ){
                $municipios = $res['municipios'][$j];
                $barrios = $res['municipios'][$j]['barrios'];
                continue;
            }
        }
        
        for($i = 0; $i < sizeof($barrios); $i++){
            
            if($barrios[$i]['_id']->__toString() === $this->idAjuste){

                if($barrios[$i]['codigoBarrio'] === $this->valor['municipios']['barrios']['codigoBarrio']){

                    $where = array( "municipios.barrios._id" => new MongoId($this->idAjuste) );
                    $valores = array(
                        'municipios.$.barrios.' . $i  => array( "_id" => new MongoId($this->idAjuste) , "nombreBarrio" => $this->valor['municipios']['barrios']['nombreBarrio'] , "codigoBarrio" => $this->valor['municipios']['barrios']['codigoBarrio'] )
                    );
                    $valores = array('$set' => $valores);
                    $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
                    return $res;
                }else{
                    
                    $cont = null;
                    if(sizeof($municipios['barrios']) == 1){
                        $cont = 0;
                    }else{
                        for($k = 0; $k < sizeof($municipios['barrios']); $k++){
                            if( $this->idAjuste != $municipios['barrios'][$k]['_id'] ){
                                if( $this->valor['municipios']['barrios']['codigoBarrio'] == $municipios['barrios'][$k]['codigoBarrio'] ){
                                    $cont = 1;
                                    break 1;
                                }else{
                                    $cont = 0;
                                }
                            }
                        }
                    }
                    
                    
                    if($cont > 0){
                        $res = array("mensaje" => "El código ya está registrado para otro barrio.");
                        return $res;
                    }else if($cont === 0){
                        $where = array( "municipios.barrios._id" => new MongoId($this->idAjuste) );
                        $valores = array(
                            'municipios.$.barrios.' . $i => array( "_id" => new MongoId($this->idAjuste) , "nombreBarrio" => $this->valor['municipios']['barrios']['nombreBarrio'] , "codigoBarrio" => $this->valor['municipios']['barrios']['codigoBarrio'] )
                        );
                        $valores = array('$set' => $valores);
                        $res = $this->db->selectCollection($this->coleccion)->update( $where , $valores );
                        return $res;
                    }
                }
            }
        }
        
    }
    
    public function crearVereda(){
        $where = array("municipios._id" => new MongoId($this->idMunicipio));
        
        $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
        
        $veredas = null;
        for($i = 0; $i < sizeof($res['municipios']); $i++){
            
            if( $res['municipios'][$i]['_id']->__toString() == $this->idMunicipio){
                
                if(isset($res['municipios'][$i]['veredas'])){
                    
                    $veredas = $res['municipios'][$i]['veredas'];
                    break;
                    
                }else{
                    
                    $where = array('municipios._id' => new MongoId($this->idMunicipio));
                    $valores = array(
                        "municipios.$.veredas" => array( 
                            "_id" => new MongoId() , 
                            "nombreVereda" => $this->valor['municipios']['veredas']['nombreVereda'], 
                            "codigoVereda" => $this->valor['municipios']['veredas']['codigoVereda'] 
                        )
                    );
                    return $this->db->selectCollection("colDepartamentos")->update( $where , array('$push' => $valores) );
                    
                }
                
            }
        }
        
        $cont = 0;
        for($i = 0; $i < sizeof($veredas);$i++){
            if( $veredas[$i]['codigoVereda'] == $this->valor['municipios']['veredas']['codigoVereda'] ){
                $cont = 1;
                break;
            }
        }
        
        $res = null;
        
        if($cont == 0){
            $where = array('municipios._id' => new MongoId($this->idMunicipio));
            $valores = array(
                "municipios.$.veredas" => array( 
                    "_id" => new MongoId() , 
                    "nombreVereda" => $this->valor['municipios']['veredas']['nombreVereda'], 
                    "codigoVereda" => $this->valor['municipios']['veredas']['codigoVereda'] 
                )
            );
            $res = $this->db->selectCollection("colDepartamentos")->update( $where , array('$push' => $valores) );
        }else{
            $res = array("mensaje" => "El código ya está registrado para otra vereda");
        }
        return $res;
    }
    
    public function actualizarVereda(){
        $where = array("municipios.veredas._id" => new MongoId($this->idAjuste));
        $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
        for($j = 0; $j < sizeof($res['municipios']); $j++){
            if($res['municipios'][$j]['_id']->__toString() == $this->idMunicipio ){
                $municipios = $res['municipios'][$j];
                $barrios = $res['municipios'][$j]['veredas'];
                break 1;
            }
        }
        
        for($i = 0; $i < sizeof($barrios); $i++){
            
            if($barrios[$i]['_id']->__toString() === $this->idAjuste){
                
                if($barrios[$i]['codigoVereda'] === $this->valor['municipios']['veredas']['codigoVereda']){
                    
                    $where = array( "municipios.veredas._id" => new MongoId($this->idAjuste) );
                    $valores = array(
                        'municipios.$.veredas.' . $i  => array( "_id" => new MongoId($this->idAjuste) , "nombreVereda" => $this->valor['municipios']['veredas']['nombreVereda'] , "codigoVereda" => $this->valor['municipios']['veredas']['codigoVereda'] )
                    );
                    $valores = array('$set' => $valores);
                    $res = $this->db->selectCollection("colDepartamentos")->update( $where , $valores );
                    return $res;
                }else{
                    
                    $cont = null;
                    
                    if(sizeof($municipios['veredas']) == 1){
                        $cont = 0;
                    }else{
                        for($k = 0; $k < sizeof($municipios['veredas']); $k++){
                        if( $this->idAjuste != $municipios['veredas'][$k]['_id']->__toString() ){
                                if( $this->valor['municipios']['veredas']['codigoVereda'] == $municipios['veredas'][$k]['codigoVereda'] ){
                                    $cont = 1;
                                    break 1;
                                }else{
                                    $cont = 0;
                                }
                            }
                        }
                    }
                    
                    
                    if($cont > 0){
                        $res = array("mensaje" => "El código ya está registrado para otra vereda.");
                        return $res;
                    }else if($cont === 0){
                        
                        $where = array( "municipios.veredas._id" => new MongoId($this->idAjuste) );
                        $valores = array(
                            'municipios.$.veredas.' . $i => array( "_id" => new MongoId($this->idAjuste) , "nombreVereda" => $this->valor['municipios']['veredas']['nombreVereda'] , "codigoVereda" => $this->valor['municipios']['veredas']['codigoVereda'] )
                        );
                        $valores = array('$set' => $valores);
                        $res = $this->db->selectCollection("colDepartamentos")->update( $where , $valores );
                        return $res;
                    }
                }
            }
        }
    }
    
    public function crearCuraduria(){
        $where = array("municipios._id" => new MongoId($this->idMunicipio));
        
        $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
        
        $curadurias = null;
        
        for($i = 0; $i < sizeof($res['municipios']); $i++){
            
            if( $res['municipios'][$i]['_id']->__toString() == $this->idMunicipio){
                
                if(isset($res['municipios'][$i]['curadurias'])){
                    
                    $curadurias = $res['municipios'][$i]['curadurias'];
                    break 1;
                    
                }else{
                    
                    $where = array('municipios._id' => new MongoId($this->idMunicipio));
                    $valores = array(
                        "municipios.$.curadurias" => array( 
                            "_id" => new MongoId() , 
                            "nombreCuraduria" => $this->valor['municipios']['curadurias']['nombreCuraduria'], 
                            "codigoCuraduria" => $this->valor['municipios']['curadurias']['codigoCuraduria'] 
                        )
                    );
                    return $this->db->selectCollection("colDepartamentos")->update( $where , array('$push' => $valores) );
                    
                }
            }
        }
        
        $cont = 0;
        
        for($i = 0; $i < sizeof($curadurias);$i++){
            if( $curadurias[$i]['codigoCuraduria'] == $this->valor['municipios']['curadurias']['codigoCuraduria'] ){
                $cont = 1;
                break 1;
            }
        }
        
        
        if($cont == 0){
            $where = array('municipios._id' => new MongoId($this->idMunicipio));
            $valores = array(
                "municipios.$.curadurias" => array( 
                    "_id" => new MongoId() , 
                    "nombreCuraduria" => $this->valor['municipios']['curadurias']['nombreCuraduria'], 
                    "codigoCuraduria" => $this->valor['municipios']['curadurias']['codigoCuraduria'] 
                )
            );
            $res = $this->db->selectCollection("colDepartamentos")->update( $where , array('$push' => $valores) );
        }else{
            $res = array("mensaje" => "El código ya está registrado para otra curaduría");
        }
        return $res;
    }
    
    public function actualizarCuraduria(){
        $where = array("municipios.curadurias._id" => new MongoId($this->idAjuste));
        $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
        for($j = 0; $j < sizeof($res['municipios']); $j++){
            if($res['municipios'][$j]['_id']->__toString() == $this->idMunicipio ){
                $municipios = $res['municipios'][$j];
                $barrios = $res['municipios'][$j]['curadurias'];
                break 1;
            }
        }
        
        for($i = 0; $i < sizeof($barrios); $i++){
            
            if($barrios[$i]['_id']->__toString() === $this->idAjuste){
                
                if($barrios[$i]['codigoCuraduria'] === $this->valor['municipios']['curadurias']['codigoCuraduria']){
                    
                    $where = array( "municipios.curadurias._id" => new MongoId($this->idAjuste) );
                    $valores = array(
                        'municipios.$.curadurias.' . $i  => array( "_id" => new MongoId($this->idAjuste) , "nombreCuraduria" => $this->valor['municipios']['curadurias']['nombreCuraduria'] , "codigoCuraduria" => $this->valor['municipios']['curadurias']['codigoCuraduria'] )
                    );
                    $valores = array('$set' => $valores);
                    $res = $this->db->selectCollection("colDepartamentos")->update( $where , $valores );
                    return $res;
                }else{
                    
                    $cont = null;
                    
                    if(sizeof($municipios['curadurias']) == 1){
                        $cont = 0;
                    }else{
                        for($k = 0; $k < sizeof($municipios['curadurias']); $k++){
                        if( $this->idAjuste != $municipios['curadurias'][$k]['_id']->__toString() ){
                                if( $this->valor['municipios']['curadurias']['codigoCuraduria'] == $municipios['curadurias'][$k]['codigoCuraduria'] ){
                                    $cont = 1;
                                    break 1;
                                }else{
                                    $cont = 0;
                                }
                            }
                        }
                    }
                    
                    
                    if($cont > 0){
                        $res = array("mensaje" => "El código ya está registrado para otra curaduría.");
                        return $res;
                    }else if($cont === 0){
                        
                        $where = array( "municipios.curadurias._id" => new MongoId($this->idAjuste) );
                        $valores = array(
                            'municipios.$.curadurias.' . $i => array( "_id" => new MongoId($this->idAjuste) , "nombreCuraduria" => $this->valor['municipios']['curadurias']['nombreCuraduria'] , "codigoCuraduria" => $this->valor['municipios']['curadurias']['codigoCuraduria'] )
                        );
                        $valores = array('$set' => $valores);
                        $res = $this->db->selectCollection("colDepartamentos")->update( $where , $valores );
                        return $res;
                    }
                }
            }
        }
    }
    
    public function consultarIdentificaciones(){
        return iterator_to_array( $this->db->selectCollection("colDocIdentificacion")->find() );
    }
    
    public function consultarTipoTramites(){
        return iterator_to_array( $this->db->selectCollection("colTipoTramites")->find() );
    }
    
    public function cargarTipoModalidades(){
        return iterator_to_array( $this->db->selectCollection("colModalidades")->find() );
    }
    
    public function cargarTipoUsoVivienda(){
        return iterator_to_array( $this->db->selectCollection("colUsosVivienda")->find() );
    }
    
    public function cargarTipoObjetoTramite(){
        return iterator_to_array( $this->db->selectCollection("colObjetoTramites")->find() );
    }
      
    public function cargarDepartamentos(){
        return iterator_to_array( $this->db->selectCollection("colDepartamentos")->find()->sort( array("codigoDepartamento" => 1) ) );
    }
    
    public function cargarMunicipios(){
        if($this->idDepartamento != 'null'){
            $where = array("_id" => new MongoId($this->idDepartamento));
            $res = $this->db->selectCollection("colDepartamentos")->findOne( $where , array("municipios" => true) );
            if(is_array($res)){
                return $res['municipios'];
            }else{
                return array();
            }
            
        }
        return array();
    }
    
    public function cargarBarrios(){
        if($this->idMunicipio != 'null'){
            $where = array("municipios._id" => new MongoId($this->idMunicipio));
            $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
            if(is_array($res)){
                for($i = 0; $i < sizeof($res['municipios']); $i++){
                    if($res['municipios'][$i]['_id']->__toString() == $this->idMunicipio ){
                        return $res['municipios'][$i]['barrios'];
                    }
                }
            }else{
                return array();
            }
            
        }
        return array();
    }
    
    public function cargarBarriosVeredas(){
        $where = array("municipios.curadurias._id" => new MongoId($_SESSION['curaduria']));
        $res = $this->db->selectCollection("colDepartamentos")->findOne( $where , array( "_id" => false ) );
        if(is_array($res)){
            foreach ($res as $key => $value) {
                foreach ($res["municipios"] as $clave => $valor) {
                    foreach ($res["municipios"][$clave] as $clave2 => $curaduria) {
                        foreach ($res["municipios"][$clave]["curadurias"] as $key => $value) {
                            if($value["_id"]->__toString() == $_SESSION["curaduria"]){
                                return array(
                                    "barrios" => $res["municipios"][$clave]['barrios'],
                                    "veredas" => $res["municipios"][$clave]['veredas'],
                                );
                            }
                        }
                        
                    }
                    
                }
            }
        }else{
            return array();
        }
    }
    
    public function cargarVeredas(){
        if($this->idMunicipio != 'null'){
            $where = array("municipios._id" => new MongoId($this->idMunicipio));
            $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
            if(is_array($res)){
                for($i = 0; $i < sizeof($res['municipios']); $i++){
                    if($res['municipios'][$i]['_id']->__toString() == $this->idMunicipio ){
                        return $res['municipios'][$i]['veredas'];
                    }
                }
            }else{
                return array();
            }
            
        }
        return array();
    }
    
    public function cargarCuradurias(){
        if($this->idMunicipio != 'null'){
            $where = array("municipios._id" => new MongoId($this->idMunicipio));
            $res = $this->db->selectCollection("colDepartamentos")->findOne( $where );
            if(is_array($res)){
                for($i = 0; $i < sizeof($res['municipios']); $i++){
                    if($res['municipios'][$i]['_id']->__toString() == $this->idMunicipio ){
                        return $res['municipios'][$i]['curadurias'];
                    }
                }
            }else{
                return array();
            }
            
        }
        return array();
    }
    
    public function listarCuraduriasCrearUsuario(){
        $res = $this->db->selectCollection("colDepartamentos")->find( array() , array("municipios.curadurias" => true, "_id" => false) );
        $res = iterator_to_array($res);
        $curaduriasTotales = null;
        $curadurias = null;
        foreach ($res as $claves => $departamentos) {
            for($i = 0; $i < sizeof($departamentos); $i++){
                for($j = 0; $j < sizeof($departamentos['municipios']); $j++){
                    for($k = 0; $k < sizeof($departamentos['municipios'][$j]); $k++){
                        $curaduriasTotales[] = $departamentos['municipios'][$j]['curadurias'];
                    }
                }
                
            }
        }
        foreach ($curaduriasTotales as $clave => $valor) {
            foreach ($valor as $curaduria => $value) {
                $curadurias[] = array(
                    "_id" => $value['_id']->__toString(),
                    "nombreCuraduria" => $value['nombreCuraduria'],
                    "codigoCuraduria" => $value['codigoCuraduria']
                );
            }
        }
        return $curadurias;
    }
}
