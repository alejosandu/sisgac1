<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of tramitesModel
 *
 * @author asanchez
 */
class tramitesModel {
    
    private $db = null;
    private $colTramites = null;
    private $colArchivos = null;
    
    private $idTramite = null;
    private $esTramite = null;
    private $observaciones = null;
    private $idTipoTramites = null;
    private $idTipoModalidades = null;
    private $idTipoUsosVivienda = null;
    private $idObjetoTramite = null;
    private $folioInicial = null;
    private $folioFinal = null;
    private $totalFolios = null;
    private $numConsecutivo = null;
    private $anoRadicacion = null;
    private $fechaExpedicion = null;
    private $fechaEjecutoria = null;
    private $fechaLegalDebidaForma = null;
    private $predio = null;
    private $titular = null;
    private $solicitante = null;
    
    function __construct($db) {
        try {
            $this->db = $db;
            $this->colTramites = $this->db->selectCollection("colTramites");
            $this->colArchivos = $this->db->getGridFS("colArchivos");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function editarTramite(){
        if($this->esTramite){
            $valores = array(
                "tipoTramites" => $this->idTipoTramites,
                "modalidades" => $this->idTipoModalidades,
                "tipoUsos" => $this->idTipoUsosVivienda,
                "tipoObjetoTramite" => $this->idObjetoTramite,
                "FI_Tramite" => $this->folioInicial,
                "FF_Tramite" => $this->folioFinal,
                "FT_Tramite" => $this->totalFolios,
                "fechaEjecutoria" => new MongoDate(strtotime($this->fechaEjecutoria)),
                "fechaExpedicion" => new MongoDate(strtotime($this->fechaExpedicion)),
                "fechaLegalDebidaForma" => new MongoDate(strtotime($this->fechaLegalDebidaForma)),
                "predio" => $this->predio,
                "titular" => $this->titular,
                "solicitante" => $this->solicitante
            );
        }else{
            $valores = array(
                "observaciones" => $this->observaciones,
                "FI_Tramite" => $this->folioInicial,
                "FF_Tramite" => $this->folioFinal,
                "FT_Tramite" => $this->totalFolios,
                "fechaExpedicion" => new MongoDate(strtotime($this->fechaExpedicion)),
                "predio" => $this->predio,
                "titular" => $this->titular
            );
        }
        
        $where = array('$and' => array(array("_id" => new MongoId($this->idTramite)),array("curaduria" => $_SESSION['curaduria'])));
        $valores = array('$set' => $valores);
        $res = $this->colTramites->update( $where , $valores , array("W" => true) );
        return $res;
    }
    
    public function crearNuevo(){
        
        $where = array("municipios.curadurias._id" => new MongoId($_SESSION['curaduria']));
        $departamento = $this->db->selectCollection("colDepartamentos")->findOne( $where , array("codigoDepartamento" => true , "municipios.curadurias" => true, "municipios.codigoMunicipio" => true , "_id" => false) );
        
        $municipio = null;
        $curaduria = null;
        $anoRadicacion = null;
        for($i = 0; $i < sizeof($departamento['municipios']); $i++){
            for($j = 0; $j < sizeof($departamento['municipios'][$i]); $j++){
                for($k = 0; $k < sizeof($departamento['municipios'][$i]['curadurias']); $k++){
                    if($departamento['municipios'][$i]['curadurias'][$k]['_id']->__toString() == $_SESSION['curaduria']){
                        $municipio = $departamento['municipios'][$i];
                        $curaduria = $departamento['municipios'][$i]['curadurias'][$k];
                        break 3;
                    }
                }
            }
        }
        
        if($this->esTramite){
            
            if( strlen($this->anoRadicacion['anoRadicacionTramite']) == 2 ){
                $anoRadicacion = $this->anoRadicacion['anoRadicacionTramite'];
            }else if(strlen($this->anoRadicacion['anoRadicacionTramite']) == 4){
                $anoRadicacion = str_split($this->anoRadicacion['anoRadicacionTramite'] , 2);
                $anoRadicacion = $anoRadicacion[1];
            }else{
                return array("mensaje" => "El año de radicación debe contener 2 o 4 caracteres exactos, según sea el año. Ej: 1999 o 99");
            }
            
            if( strlen($this->anoRadicacion['anoRadicacionDocumento']) == 2 || strlen($this->anoRadicacion['anoRadicacionDocumento']) == 4 ){
            //aca está vacío, de alguna forma no funcion tanto si se pone !strlen... como si se pone strlen != 2 o != 4...
            }else{
                return array("mensaje" => "El año del trámite debe contener 2 o 4 caracteres exactos, según sea el año. Ej: 1999 o 99");
            }
            
            $valores = array(
                "codigoFormUnicNac" => $departamento['codigoDepartamento'] . $municipio['codigoMunicipio'] . "-" . $curaduria['codigoCuraduria'] . "-" . $anoRadicacion . "-" . $this->numConsecutivo['consecutivoTramite'],
                "tipoTramites" => $this->idTipoTramites,
                "modalidades" => $this->idTipoModalidades,
                "tipoUsos" => $this->idTipoUsosVivienda,
                "tipoObjetoTramite" => $this->idObjetoTramite,
                "FI_Tramite" => $this->folioInicial,
                "FF_Tramite" => $this->folioFinal,
                "FT_Tramite" => $this->totalFolios,
                "numConsecutivo" => $this->numConsecutivo,
                "anoRadicacion" => $this->anoRadicacion,
                "fechaEjecutoria" => new MongoDate(strtotime($this->fechaEjecutoria)),
                "fechaExpedicion" => new MongoDate(strtotime($this->fechaExpedicion)),
                "fechaLegalDebidaForma" => new MongoDate(strtotime($this->fechaLegalDebidaForma)),
                "predio" => $this->predio,
                "titular" => $this->titular,
                "solicitante" => $this->solicitante,
                "curaduria" => $_SESSION['curaduria']
            );
        }else{
            $valores = array(
                "codigoDocumento" => $this->numConsecutivo['consecutivoDocumento'] . "-" . $this->anoRadicacion['anoRadicacionDocumento'],
                "tipoTramites" => $this->idTipoTramites[0],
                "observaciones" => $this->observaciones,
                "FI_Tramite" => $this->folioInicial,
                "FF_Tramite" => $this->folioFinal,
                "FT_Tramite" => $this->totalFolios,
                "numConsecutivo" => $this->numConsecutivo['consecutivoDocumento'],
                "anoRadicacion" => $this->anoRadicacion['anoRadicacionDocumento'],
                "fechaExpedicion" => new MongoDate(strtotime($this->fechaExpedicion)),
                "predio" => $this->predio,
                "titular" => $this->titular,
                "curaduria" => $_SESSION['curaduria']
            );
        }
        
        $res = $this->db->selectCollection("colTramites")->insert( $valores );
        
        $res = array($res, $this->esTramite ? $valores['codigoFormUnicNac'] : $valores['codigoDocumento'] , "mensajeExito" => $this->esTramite ? "Trámite: " . $valores['codigoFormUnicNac'] . " registrado con éxito" : "Documento " . $valores['codigoDocumento'] . " registrado con éxito.");
        
        return $res;
    }
    
    public function listarTramites($registrosPorPagina, $numeroPagina){
        
        $where = array("curaduria" => $_SESSION['curaduria']);
        
        if(isset($_SESSION['parametroBusqueda'])){
            $where = array('$and' => array($where , $_SESSION['parametroBusqueda']));
        }
        
        $totalTramites = $this->colTramites->count( $where );
        
        $totalPaginas = ceil($totalTramites / $registrosPorPagina);
        $numeroPagina = $numeroPagina > 0 ? $numeroPagina : 1;
        $registrosRecoridos = ($numeroPagina - 1) * $registrosPorPagina;
        
        
        $campos = array("codigoDocumento" => true , "codigoFormUnicNac" => true , "tipoTramites" => true , "titular" => true , "predio" => true, "fechaExpedicion" => true);
        $ordenamiento = array("_id" => -1);
        $tramites = $this->colTramites->find( $where , $campos )->limit( $registrosPorPagina )->skip( $registrosRecoridos )->sort( $ordenamiento );
        $tramites  = iterator_to_array($tramites);
        
        $res = array(
            "totalTramites" => $totalTramites,
            "totalPaginas" => $totalPaginas,
            "tramites" => $tramites
        );
        
        return $res;
    }
    
    public function eliminarTramite(){
//        elimino todos los archivos que relacionados al trámite
        $where = array('$and' => array(array("tramite" => $this->idTramite), array("curaduria" => $_SESSION['curaduria'])));
        $archivo = $this->colArchivos->remove( $where );
        
//        elimino el trámite como tal para que no hayan huerfanos
        $where = array('$and' => array(array("_id" => new MongoId($this->idTramite)), array("curaduria" => $_SESSION['curaduria'])));
        $tramite = $this->colTramites->remove( $where );
        $res =  array("archivoEliminado" => $archivo,"tramiteEliminado" => $tramite);
        return $res;
    }
    
    public function consultar(){
        $where = array("_id" => new MongoId($this->idTramite));
        $res = $this->colTramites->findOne( $where );
        return $res;
    }
    
}
