<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of archivosModel
 *
 * @author asanchez
 */
class archivosModel {
    private $colArchivos = null;
    private $colArchivosConsulta = null;
    
    private $idTramite = null;
    private $idArchivo = null;
    private $archivo = null;
    private $nombreArchivo = null;
    
    function __construct($db) {
        try {
            $this->db = $db;
            $this->colArchivos = $this->db->getGridFS("colArchivos");
            $this->colArchivosConsulta = $this->db->selectCollection("colArchivos.files");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function subirArchivo(){
        for($i = 0; $i < count($this->archivo['name']); $i++) {
                $nombreArchivo = $this->nombreArchivo[$i] != null ? $this->nombreArchivo[$i] . "." . pathinfo($this->archivo["name"][$i] , PATHINFO_EXTENSION ) : $this->archivo["name"][$i];
                $tmpNombreArchivo = $this->archivo["tmp_name"][$i];
    //            ingreso la información y convierto las partes de tamaño de partes (chunks) de Megabytes a Bytes
                $infoArchivo = array(
                    "filename" => $nombreArchivo, 
                    "filetype" => $this->archivo["type"][$i], 
                    "length" => $this->archivo["size"][$i], 
                    "chunkSize" => 5 * (1024**2) , 
                    "curaduria" => $_SESSION['curaduria'], 
                    "tramite" => $this->idTramite
                );
            try{
                $res[] = $this->colArchivos->storeFile( $tmpNombreArchivo , $infoArchivo );
            }catch(Exception $e){
                $res = "Error al subir el archivo " . $infoArchivo['filename'] . ", inténtelo nuevamente";
                return $res;
            }
        }
        return $res;
    }
    
    public function listarArchivos(){
        $where = array('$and' => array(array("tramite" => $this->idTramite),array("curaduria" => $_SESSION['curaduria'])));
        
        $res = $this->colArchivosConsulta->find( $where , array("filename" => true, "length" => true, "unidadDocumental" => true) )->sort( array("filename" => 1) );
        $res = iterator_to_array($res);
        $keys = array_keys($res);
        
        $totalArchivos = $this->colArchivosConsulta->count( $where );
        
        $registrosPorPagina = 5;
        $totalPaginas = ceil($totalArchivos / $registrosPorPagina);
        $numeroPagina = 1;
        $registrosRecoridos = ($numeroPagina - 1) * $registrosPorPagina;
        $pagina = null;
        $resgistroEnPagina = null;
        
        $cont = 1;
        foreach ($res as $key => $value) {
            $resgistroEnPagina[] = $value;
            if($value == end($res)){
                $pagina[] = $resgistroEnPagina;
            }else if($cont == $registrosPorPagina){
                $pagina[] = $resgistroEnPagina;
                $resgistroEnPagina = null;
                $cont = 1;
            }else{
                $cont++;
            }
            
        }
        
        return $pagina;
    }
    
    public function eliminarArchivo(){
        try{
            $where = array('$and' => array(array("_id" => new MongoId($this->idArchivo)) , array("curaduria" => $_SESSION['curaduria'])));
            $res = $this->colArchivos->remove( $where );
            return $res ? true : false;
        }catch(Exception $e){
            $res = "No se puso eliminar el archivo, intentelo nuevamente";
            return $res;
        }
    }
    
    public function consultarArchivo(){
        $where = array("_id" => new MongoId($this->idArchivo));
        $res = $this->colArchivos->findOne( $where );
        return $res;
    }
    
}
