<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of loginController
 *
 * @author asanchez
 */
class loginModel {

    private $db = null;
    private $colUsuarios = null;
    private $nombreUsuario;
    private $pass;

    function __construct($db) {
        try {
            $this->db = $db;
            $this->colUsuarios = $this->db->selectCollection("colUsuarios");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }

    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }

    public function entrar() {
        $where = array('$and' => array(array('nombreUsuario' => $this->nombreUsuario), array('pass' => $this->pass)));
        $res = iterator_to_array($this->colUsuarios->find($where));

        $usuario = null;
        foreach ($res as $valor) {
            $usuario = $valor;
        }

        if (count($res) > 0) {
            if ($usuario['estado'] === true) {
                $_SESSION['nombreUsuario'] = $usuario["nombreUsuario"];
                $_SESSION['_idUsuario'] = $usuario["_id"]->__toString();
                $_SESSION['tipoUsuario'] = $usuario["tipoUsuario"];
                $_SESSION['curaduria'] = $usuario["curaduria"];
                return array("res" => true);
            } else {
                unset($_SESSION['usuario']);
                return array("res" => "sinAcceso");
            }
        } else {
            unset($_SESSION['usuario']);
            return array("res" => false);
        }
    }

}
