<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of logsModel
 *
 * @author asanchez
 */
class logsModel {
    
    
    private $db = null; 
    private $colLog = null;
    
    private $fecha = null;
    private $autor = null;
    
    private $registrosPagina = null;
    private $totalPaginas = null;
    private $registroRecorridos = null;
    private $idLog = null;
    
    public function __construct($db) {
        try {
            $this->db = $db;
            $this->colLog = $this->db->selectCollection("colLog");
        } catch (MongoException $e) {
            exit('No se pudo establecer la conexión a la base de datos.');
        }
    }
    
    public function __SET($atributo, $valor) {
        $this->$atributo = $valor;
    }

    public function __GET($atributo) {
        return $this->$atributo;
    }
    
    public function consultarLogs(){
        
        $where = array();
        
        if($this->idLog != 0){
            if(strlen($this->idLog) == 24){
                $where = array("_id" => new MongoId($this->idLog));
            }
        }else if($this->autor != "todos"){
            if($this->fecha != 0){
                $fecha = new MongoDate(strtotime($this->fecha));
                
                $autor = strlen($this->autor) === 24 ? array("idUsuario" => $this->autor) : array("autor" => $this->autor);
                $where = array(
                    '$and' => array( 
                        $autor , 
                        array("fecha" => array(
                            '$gt' => new MongoDate(strtotime($this->fecha) ) , 
                            '$lte' => new MongoDate( strtotime($this->fecha) + (60 * 60 * 23.999) ) 
                                )
                            )
                        )
                    );
            }else{
                $where = strlen($this->autor) === 24 ? array("idUsuario" => $this->autor) : array("autor" => $this->autor);
            }
        }else{
            if($this->fecha != 0){
                $fecha = new MongoDate(strtotime($this->fecha));
                $where = array( "fecha" => array( '$gt' => new MongoDate(strtotime($this->fecha) ) , '$lte' => new MongoDate( strtotime($this->fecha) + (60 * 60 * 23.999) ) ));
            }
        }
        
        $totalLogs = $this->colLog->count( $where );
        
        $this->__SET("totalPaginas" , ceil($totalLogs / $this->registrosPagina));
        
        $ordenamiento = array("fecha" => -1);
        
        $consulta = $this->idLog != 0 ? $this->colLog->find( $where ) : $this->colLog->find( $where )->limit( $this->registrosPagina )->skip( $this->registroRecorridos )->sort( $ordenamiento );
        $res = array(
            "totalPaginas" => $this->totalPaginas,
            "totalLogs" => $totalLogs,
            "registros" => iterator_to_array( $consulta )
                );
        return $res;
    }
    
    public function registrarLog($accion,$cambios){
        $valores = array(
            "fecha" => new MongoDate(), 
            "accion" => $accion, 
            "cambios" => $cambios, 
            "autor" => isset($_SESSION['nombreUsuario']) ? $_SESSION['nombreUsuario'] : null, 
            "idUsuario" => isset($_SESSION['_idUsuario']) ? $_SESSION['_idUsuario'] : null );
        $this->colLog->insert( $valores );
    }
}
