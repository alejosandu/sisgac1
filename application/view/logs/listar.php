<section id="content" class="container-sisgac1">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="text-right">
                    <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-8">
                        <input class="form-control" value="<?php echo $fecha == 0 ? null : $fecha ?>" max="<?php echo date("Y-m-d") ?>" placeholder="Fecha. Ej: 2016-05-20" id="fecha" type="date"  onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/0/<?php echo $autor ?>/' + $('#fecha').val() } } ">
                    </div>
                    <button class="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-4" type="button" onclick="location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/0/<?php echo $autor ?>/' + $('#fecha').val()" >Ir</button>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="text-right">
                    <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-8">
                        <input placeholder="Autor o idAutor" id="autor" type="text" class="form-control" onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/0/' + $('#autor').val() + '/' + <?php echo "'" . $fecha . "'" ?> } } ">
                    </div>
                    <button class="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-4" type="button" onclick="location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/0/' + $('#autor').val() + '/' + <?php echo "'" . $fecha . "'" ?>" >Ir</button>
                </div>
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-6 colxssm-12">
                <div class="text-right">
                    <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-8">
                        <input placeholder="_idLog" id="idLog" type="text" class="form-control" onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/' + $('#idLog').val() + '/<?php echo $autor . "/" . $fecha ?>' } } ">
                    </div>
                    <button class="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-4" type="button" onclick="location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + '1/' + $('#idLog').val() + '/<?php echo $autor . "/" . $fecha ?>' " >Ir</button>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="text-right">
                    <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-8">
                        <input placeholder="Ir a la página número..." id="irAPagina" type="text" name="irAPagina" class="form-control" onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + $('#irAPagina').val() + '/0' + '/<?php echo $autor . "/" . $fecha ?>' } }">
                    </div>
                    <button class="btn btn-primary col-lg-2 col-md-2 col-sm-2 col-xs-4" type="button" onclick="location.href = urlPHP + 'logs/listar/' + <?php echo $registrosPagina ?> + '/' + $('#irAPagina').val() + '/0' + '/<?php echo $autor . "/" . $fecha ?>'" >Ir</button>
                </div>
            </div>
            
        </div>
        
        <div class="row" style="margin-bottom: -0px">
            
            <div class="col-lg-offset-9 col-lg-3 col-xs-12">
                <div class="form-inline text-right">
                <div class="form-group col-lg-8 col-sm-10 col-xs-12">
                    <label for="registrosPagina">Mostrar: </label>
                    <select id="registrosPagina" name="registrosPagina" class="form-control">
                        <option value="25" >25</option>
                        <option value="50" >50</option>
                        <option value="100" >100</option>
                        <option value="200" >200</option>
                    </select>
                </div>
                <button class="btn btn-primary col-lg-4 col-sm-2 col-xs-12" name="btnBuscarLogPaginado" type="button" onclick="location.href = urlPHP + 'logs/listar/' + $('#registrosPagina').val() + '/1/0' + '/<?php echo $autor .'/' .$fecha ?>'  " >Listar</button>
            </div>
            </div>
        </div>
        
        <div class="row" style="margin-bottom: 0px">
            <!--PAGINACIÓN-->
            <div class="text-center">
            <ul class="pagination">
                <?php if($numPagina > 1){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/1/' : '25/1/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>"> <i class="glyphicon glyphicon-fast-backward"></i> </a></li>
                <?php } ?>

                <?php if($numPagina > 1){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo ($numPagina-1).'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>"> <i class="glyphicon glyphicon-backward"></i> </a></li>
                <?php } ?>

                <?php for($i = $numPagina - 9; $i <= $numPagina + 9; $i++){ 
                    if($i > 0 && $i <= $totalPaginas){ ?>
                    <li <?php echo $numPagina == $i ? " class='active'" : "" ?>><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo $i.'/'; echo $idLog ? $idLog : '0'; echo '/' . $autor . '/' . $fecha ?>" > <?php echo $i ?></a></li>
                <?php }
                    } ?>

                <?php if($numPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo ($numPagina+1).'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>" > <i class="glyphicon glyphicon-forward"></i> </a></li>
                <?php } ?>

                <?php if($numPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo $totalPaginas.'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>" > <i class="glyphicon glyphicon-fast-forward"></i> </a></li>
                <?php } ?>
            </ul>
        </div>
        </div>
        <!--INFORMACION DE CONSULTA-->
        <div class="row">
            <div class="text-center">
                <strong>Total páginas: <?php echo number_format($totalPaginas) ?> - Total registros: <?php echo number_format($res['totalLogs']) ?> </strong>
            </div>
        </div>
        <div class="row">
            
            <table id="tablaDatosUsuarios" class="table table-striped table-hover borde-sombreado text-left">
                <thead>
                <th class="hidden-sm hidden-xs">_idLog</th>
                <th>Fecha</th>
                <th  class="hidden-xs">Hora</th>
                <th class="hidden-xs">Acción</th>
                <th>Cambios</th>
                <th>Autor</th>
                <th class="hidden-sm hidden-xs">idAutor</th>
                </thead>
                <tbody id="bodyTablaDatosUsuarios">
                    <?php error_reporting(0); if(sizeof($logs) > 0){ 
                        foreach ($logs as $clave => $value) { ?>
                        <tr>
                            <td class="col-lg-2 hidden-sm hidden-xs"><?php echo $logs[$clave]['_id'] ?></td>
                            <td class="col-lg-2"><span class="hidden-xs"><?php echo date("l, F d \of Y", $logs[$clave]['fecha']->sec) ?></span> <br> [<?php echo date("Y-m-d", $logs[$clave]['fecha']->sec) ?>] </td>
                            <td class="col-lg-2 hidden-xs"><?php echo date("h:i:s a", $logs[$clave]['fecha']->sec) ?> <br> <?php echo date("(eP)", $logs[$clave]['fecha']->sec) ?> </td>
                            <td class="col-lg-2 hidden-xs"><?php echo $logs[$clave]['accion'] ?></td>
                            <td class="col-lg-2"><?php echo $logs[$clave]['cambios'] ?></td>
                            <td class="col-lg-1"><?php echo $logs[$clave]['autor'] ?></td>
                            <td class="col-lg-2 hidden-sm hidden-xs"><?php echo $logs[$clave]['idUsuario'] ?></td>
                        </tr>
                    <?php }
                    }else{ ?>
                <td colspan="7" class="text-center text-danger">No se encontraron logs para listar.</td>
                    <?php } error_reporting(1); ?>
                </tbody>
            </table>
            <!--PAGINACIÓN-->
            <div class="text-center">
            <ul class="pagination">
                <?php if($numPagina > 1){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/1/' : '25/1/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>"> <i class="glyphicon glyphicon-fast-backward"></i> </a></li>
                <?php } ?>

                <?php if($numPagina > 1){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo ($numPagina-1).'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>"> <i class="glyphicon glyphicon-backward"></i> </a></li>
                <?php } ?>

                <?php for($i = $numPagina - 9; $i <= $numPagina + 9; $i++){ 
                    if($i > 0 && $i <= $totalPaginas){ ?>
                    <li <?php echo $numPagina == $i ? " class='active'" : "" ?>><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo $i.'/'; echo $idLog ? $idLog : '0'; echo '/' . $autor . '/' . $fecha ?>" > <?php echo $i ?></a></li>
                <?php }
                    } ?>

                <?php if($numPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo ($numPagina+1).'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>" > <i class="glyphicon glyphicon-forward"></i> </a></li>
                <?php } ?>

                <?php if($numPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "logs/listar/"; echo $registrosPagina ? $registrosPagina.'/' : '25/'; echo $totalPaginas.'/'; echo $idLog ? $idLog : '0/'; echo $autor . '/' . $fecha ?>" > <i class="glyphicon glyphicon-fast-forward"></i> </a></li>
                <?php } ?>
            </ul>
        </div>
        </div>
    </div>

</section>