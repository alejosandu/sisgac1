<div id="content" class="container-sisgac1">
    <div class="container">
        <div class="row">
            <?php if($permisoSuperAdmin || $permisoAdmin){ ?>
            <div id="modalEditarTramite" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title">Editar <?php echo isset($tramite['codigoFormUnicNac']) ? "trámite " . $tramite['codigoFormUnicNac'] : "documento " . $tramite['codigoDocumento'] ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row" style="margin: 0px auto;">
                                <?php if(isset($tramite['codigoFormUnicNac'])){ //SI ES TRÁMITE ?>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <h4>Información del trámite</h4>
                                <hr class="hr-bordeAbajo">
                                <input type="hidden" value="true" name="esTramite">
                                <input type="hidden" value="<?php echo $tramite['_id'] ?>" name="idTramite">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="idTipoTramites">Tipos de trámites o actuaciones</label>
                                            <select id="idTipoTramites" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                                <option value="null">Seleccione tipo de trámite</option>
                                                <?php foreach ($consultaTipoTramites as $valor) { ?>
                                                    <option value="<?php echo $valor['_id'] ?>" <?php for($j = 0; $j < sizeof($tramite['tipoTramites']); $j++){ if($tramite['tipoTramites'][$j] == $valor['_id']){ echo "selected='selected'"; } } ?> ><?php echo $valor['tipoTramite'] ?><?php echo $valor['tipoTramiteSigla'] != '' ? ' - ' . $valor['tipoTramiteSigla'] : '' ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group soloDocumento col-lg-6 col-md-6 col-sm-6">
                                            <label for="idTipoModalidades">Modalidades</label>
                                            <select id="idTipoModalidades" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                                <option value="null">Seleccione modalidades del trámite</option>
                                                <?php foreach ($consultaTipoModalidades as $valor) { ?>
                                                    <option value="<?php echo $valor['_id'] ?>" <?php for($j = 0; $j < sizeof($tramite['modalidades']); $j++){ if($tramite['modalidades'][$j] == $valor['_id']){ echo "selected='selected'"; } } ?> ><?php echo $valor['tipoModalidad'] ?> </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group soloDocumento col-lg-6 col-md-6 col-sm-6">
                                            <label for="idTipoUsosVivienda">Tipos de uso</label>
                                            <select id="idTipoUsosVivienda" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                                <option value="null">Seleccione uso de vivienda del trámite</option>
                                                <?php foreach ($consultaTipoUsosVivienda as $valor) { ?>
                                                    <option value="<?php echo $valor['_id'] ?>" <?php for($j = 0; $j < sizeof($tramite['tipoUsos']); $j++){ if($tramite['tipoUsos'][$j] == $valor['_id']){ echo "selected='selected'"; } } ?> ><?php echo $valor['usoVivienda'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group soloDocumento col-lg-6 col-md-6 col-sm-6">
                                            <label for="idObjetoTramite">Objeto del trámite</label>
                                            <select id="idObjetoTramite" class="form-control">
                                                <option value="null">Seleccione objeto del trámite</option>
                                                <?php foreach ($consultaTipoObjetotramite as $valor) { ?>
                                                    <option value="<?php echo $valor['_id'] ?>" <?php echo $tramite['tipoObjetoTramite'] == $valor['_id'] ? "selected='selected'" : null ?> ><?php echo $valor['tipoObjetoTramite'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                            <label for="folioInicial">Folio inicial</label>
                                            <input id="folioInicial" value="<?php echo $tramite['FI_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                            <label for="folioFinal">Folio final</label>
                                            <input id="folioFinal" value="<?php echo $tramite['FF_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                            <label for="totalFolios">Total folios</label>
                                            <input id="totalFolios" value="<?php echo $tramite['FT_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group soloDocumento col-lg-6 col-md-6 col-sm-6">
                                            <label for="fechaEjecutoria">Fecha ejecutoria</label>
                                            <input id="fechaEjecutoria" value="<?php echo date("Y-m-d",$tramite['fechaEjecutoria']->sec) ?>" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="fechaExpedicion">Fecha de expedición</label>
                                            <input id="fechaExpedicion" value="<?php echo date("Y-m-d",$tramite['fechaExpedicion']->sec) ?>" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="fechaLegalDebidaForma">Fecha legal y debida forma</label>
                                            <input id="fechaLegalDebidaForma" value="<?php echo date("Y-m-d",$tramite['fechaLegalDebidaForma']->sec) ?>" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <h4>Información del predio</h4>
                                    <hr class="hr-bordeAbajo">
                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="matriculaInmobiliaria">Matrícula inmobiliaria</label>
                                            <input id="matriculaInmobiliaria" value="<?php echo $tramite['predio']['matriculaInmobiliaria'] ?>"  type="text" class="form-control" placeholder="Ingrese con guiones incluidos">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="codigoCatastral">Código catastral</label>
                                            <input id="codigoCatastral" value="<?php echo $tramite['predio']['codigoCatastral'] ?>" maxlength="20" onblur='if(this.value.trim().length < 20){ var calculo = 20 - parseInt(this.value.length);  var ceros = ""; for(var i = 0; i < calculo; i++){ ceros += "0"; }; this.value = this.value + ceros; $(this).trigger("keyup") }' onkeyup='if(this.value.length != 20){ this.setAttribute("style", "border-color: #d9232d"); }else { this.setAttribute("style", "border-color: green"); }' type="number" class="form-control" placeholder="Código sin guiones">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                            <label for="direccionNomenclatura">Dirección</label>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <?php $direccion = explode("/", $tramite['predio']['direccionNomenclatura']); ?>
                                                    <select name="direccionNomenclatura" class="btn btn-default dropdown-toggle" >
                                                        <option value="CL" <?php echo trim($direccion[0]) == "CL" ? "selected='seledted'" : null ?> >CL</option>
                                                        <option value="CR" <?php echo trim($direccion[0]) == "CR" ? "selected='seledted'" : null ?> >CR</option>
                                                        <option value="DG" <?php echo trim($direccion[0]) == "DG" ? "selected='seledted'" : null ?> >DG</option>
                                                        <option value="TV" <?php echo trim($direccion[0]) == "TV" ? "selected='seledted'" : null ?> >TV</option>
                                                        <option value="VI" <?php echo trim($direccion[0]) == "VI" ? "selected='seledted'" : null ?> >VI</option>
                                                    </select>
                                                </div>
                                                <input id="direccionNomenclatura" value="<?php echo trim($direccion[1]); ?>" name="direccionNomenclatura" type="text" class="form-control">
                                                <div class="input-group-addon">
                                                    Nº
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                            <div class="col-lg-6">
                                                <label for="direccionNomenclatura">&nbsp;</label>
                                                <input name="direccionNomenclatura" value="<?php echo trim($direccion[2]); ?>" type="text" class="form-control" placeholder="">
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="direccionNomenclatura">&nbsp;</label>
                                                <input name="direccionNomenclatura" value="<?php echo trim($direccion[3]); ?>" type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="estrato">Estrato</label>
                                            <select id="estrato" class="form-control">
                                                <option value="null">Seleccione estrato</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['estrato'] == "No aplica" ? "selected='selected'" : null ?> >No Aplica</option>
                                                <option value="1" <?php echo $tramite['predio']['estrato'] == "1" ? "selected='selected'" : null ?> >1</option>
                                                <option value="2" <?php echo $tramite['predio']['estrato'] == "2" ? "selected='selected'" : null ?> >2</option>
                                                <option value="3" <?php echo $tramite['predio']['estrato'] == "3" ? "selected='selected'" : null ?> >3</option>
                                                <option value="4" <?php echo $tramite['predio']['estrato'] == "4" ? "selected='selected'" : null ?> >4</option>
                                                <option value="5" <?php echo $tramite['predio']['estrato'] == "5" ? "selected='selected'" : null ?> >5</option>
                                                <option value="6" <?php echo $tramite['predio']['estrato'] == "6" ? "selected='selected'" : null ?> >6</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <?php  ?>
                                            <label for="barrio">Barrio</label>
                                            <select id="barrio" class="form-control aplicarSelect2" style="width: 100%">
                                                <option value="null" >Seleccione barrio</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['barrio'] == "No aplica" ? "selected='selected'" : null ?>>No aplica</option>
                                            <?php foreach ($barrios as $valor) { ?>
                                                <option value="<?php echo $valor['nombreBarrio'] ?>" <?php echo $tramite['predio']['barrio'] == $valor['nombreBarrio'] ? "selected='selected'" : null ?> ><?php echo $valor['codigoBarrio'] ?> - <?php echo $valor['nombreBarrio'] ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="veredaUrb">Urbanización o vereda</label>
                                            <select id="veredaUrb" class="form-control aplicarSelect2Tag" style="width: 100%">
                                                <option value="null">Seleccione vereda o digite urbanización</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['veredaUrb'] == "No aplica" ? "selected='selected'" : null ?> >No aplica</option>
                                                <?php if( !in_array( $tramite['predio']['veredaUrb'] , $veredas ) ){ ?>
                                                    <option value="<?php echo $tramite['predio']['veredaUrb']; ?>" selected="selected"><?php echo $tramite['predio']['veredaUrb']; ?></option>
                                                <?php } ?>
                                                <?php foreach ($veredas as $valor) { ?>
                                                    <option value="<?php echo $valor['nombreVereda'] ?>" <?php echo $tramite['predio']['veredaUrb'] == $valor['nombreVereda'] ? "selected='selected'" : null ?> ><?php echo $valor['codigoVereda'] ?> - <?php echo $valor['nombreVereda'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <h4>Información de personas</h4>
                                    <hr class="hr-bordeAbajo">
                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6 text-nowrap">
                                            <label for="tipoDocIdentificacionTitular">Tipo documento</label>
                                            <select id="tipoDocIdentificacionTitular" class="form-control">
                                                <option value="">Seleccione tipo de documento</option>
                                                <?php foreach ($tipoDocIdentificacion as $valor) { ?>
                                                <option value="<?php echo $valor['tipoDocumentoSigla'] ?>" <?php echo $tramite['titular']['tipoDocIdentificacionTitular'] == $valor['tipoDocumentoSigla'] ? "selected='selected'" : null ?> ><?php echo $valor['tipoDocumentoSigla'] ?> - <?php echo $valor['tipoDocumentoId'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="numDocTitular">Número de documento</label>
                                            <input id="numDocTitular" value="<?php echo $tramite['titular']['numDocTitular'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="nombreTitular">Nombre titular</label>
                                            <input id="nombreTitular" value="<?php echo $tramite['titular']['nombreTitular'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="apellidoTitular">Apellido titular</label>
                                            <input id="apellidoTitular" value="<?php echo $tramite['titular']['apellidoTitular'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6 text-nowrap">
                                            <label for="tipoDocIdentificacionSolicitante">Tipo documento</label>
                                            <select id="tipoDocIdentificacionSolicitante" class="form-control">
                                                <option value="">Seleccione tipo de documento</option>
                                                <?php foreach ($tipoDocIdentificacion as $valor) { ?>
                                                <option value="<?php echo $valor['tipoDocumentoSigla'] ?>" <?php echo $tramite['solicitante']['tipoDocIdentificacionSolicitante'] == $valor['tipoDocumentoSigla'] ? "selected='selected'" : null ?> ><?php echo $valor['tipoDocumentoSigla'] ?> - <?php echo $valor['tipoDocumentoId'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                            <label for="numDocSolicitante">Num doc solicitante</label>
                                            <input id="numDocSolicitante" value="<?php echo $tramite['solicitante']['numDocSolicitante'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                            <label for="nombreSolicitante">Nombre Solicitante</label>
                                            <input id="nombreSolicitante" value="<?php echo $tramite['solicitante']['nombreSolicitante'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                            <label for="apellidoSolicitante">Apellido Solicitante</label>
                                            <input id="apellidoSolicitante" value="<?php echo $tramite['solicitante']['apellidoSolicitante'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                            <label for="telefonoSolicitante">Teléfono solicitante</label>
                                            <input id="telefonoSolicitante" value="<?php echo $tramite['solicitante']['telefonoSolicitante'] ?>" type="tel" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6 text-nowrap">
                                            <label for="direccionSolicitante">Dirección de solicitante</label>
                                            <input id="direccionSolicitante" value="<?php echo $tramite['solicitante']['direccionSolicitante'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                            <label for="emailSolicitante">E-Mail de solicitante</label>
                                            <input id="emailSolicitante" value="<?php echo $tramite['solicitante']['emailSolicitante'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                    </div>
                                </div>
                                <?php }else{ //SI ES OFICIO ?>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <h4>Información del documento</h4>
                                    <hr class="hr-bordeAbajo">
                                    <input type="hidden" value="false" name="esTramite">
                                    <input type="hidden" value="<?php echo $tramite['_id'] ?>" name="idTramite">
                                    <div class="col-lg-12">
                                        <div class="form-group soloTramite col-lg-12 col-md-12 col-sm-12">
                                            <label for="observaciones">Asunto u observaciones</label>
                                            <textarea id="observaciones" class="form-control" style="resize:vertical" maxlength="725" placeholder="Digite aquí las observaciones"><?php echo $tramite['observaciones'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="folioInicial">Folio inicial</label>
                                            <input id="folioInicial" value="<?php echo $tramite['FI_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="folioFinal">Folio final</label>
                                            <input id="folioFinal" value="<?php echo $tramite['FF_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="totalFolios">Total folios</label>
                                            <input id="totalFolios" value="<?php echo $tramite['FT_Tramite'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="fechaExpedicion">Fecha de expedición</label>
                                            <input id="fechaExpedicion" value="<?php echo date("Y-m-d",$tramite['fechaExpedicion']->sec) ?>" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <h4>Información del predio</h4>
                                    <hr class="hr-bordeAbajo">
                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="matriculaInmobiliaria">Matrícula inmobiliaria</label>
                                            <input id="matriculaInmobiliaria" value="<?php echo $tramite['predio']['matriculaInmobiliaria'] ?>"  type="text" class="form-control" placeholder="Ingrese con guiones incluidos">
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="codigoCatastral">Código catastral</label>
                                            <input id="codigoCatastral" value="<?php echo $tramite['predio']['codigoCatastral'] ?>" maxlength="20" onblur='if(this.value.trim().length < 20){ var calculo = 20 - parseInt(this.value.length);  var ceros = ""; for(var i = 0; i < calculo; i++){ ceros += "0"; }; this.value = this.value + ceros; $(this).trigger("keyup") }' onkeyup='if(this.value.length != 20){ this.setAttribute("style", "border-color: #d9232d"); }else { this.setAttribute("style", "border-color: green"); }' type="number" class="form-control" placeholder="Código sin guiones">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                            <label for="direccionNomenclatura">Dirección</label>
                                            <div class="input-group">
                                                <div class="input-group-btn">
                                                    <?php $direccion = explode("/", $tramite['predio']['direccionNomenclatura']); ?>
                                                    <select name="direccionNomenclatura" class="btn btn-default dropdown-toggle" >
                                                        <option value="CL" <?php echo trim($direccion[0]) == "CL" ? "selected='seledted'" : null ?> >CL</option>
                                                        <option value="CR" <?php echo trim($direccion[0]) == "CR" ? "selected='seledted'" : null ?> >CR</option>
                                                        <option value="DG" <?php echo trim($direccion[0]) == "DG" ? "selected='seledted'" : null ?> >DG</option>
                                                        <option value="TV" <?php echo trim($direccion[0]) == "TV" ? "selected='seledted'" : null ?> >TV</option>
                                                        <option value="VI" <?php echo trim($direccion[0]) == "VI" ? "selected='seledted'" : null ?> >VI</option>
                                                    </select>
                                                </div>
                                                <input id="direccionNomenclatura" value="<?php echo trim($direccion[1]); ?>" name="direccionNomenclatura" type="text" class="form-control">
                                                <div class="input-group-addon">
                                                    Nº
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                            <div class="col-lg-6">
                                                <label for="direccionNomenclatura">&nbsp;</label>
                                                <input name="direccionNomenclatura" value="<?php echo trim($direccion[2]); ?>" type="text" class="form-control" placeholder="">
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="direccionNomenclatura">&nbsp;</label>
                                                <input name="direccionNomenclatura" value="<?php echo trim($direccion[3]); ?>" type="text" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="estrato">Estrato</label>
                                            <select id="estrato" class="form-control">
                                                <option value="null">Seleccione estrato</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['estrato'] == "No aplica" ? "selected='selected'" : null ?> >No Aplica</option>
                                                <option value="1" <?php echo $tramite['predio']['estrato'] == "1" ? "selected='selected'" : null ?> >1</option>
                                                <option value="2" <?php echo $tramite['predio']['estrato'] == "2" ? "selected='selected'" : null ?> >2</option>
                                                <option value="3" <?php echo $tramite['predio']['estrato'] == "3" ? "selected='selected'" : null ?> >3</option>
                                                <option value="4" <?php echo $tramite['predio']['estrato'] == "4" ? "selected='selected'" : null ?> >4</option>
                                                <option value="5" <?php echo $tramite['predio']['estrato'] == "5" ? "selected='selected'" : null ?> >5</option>
                                                <option value="6" <?php echo $tramite['predio']['estrato'] == "6" ? "selected='selected'" : null ?> >6</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <?php  ?>
                                            <label for="barrio">Barrio</label>
                                            <select id="barrio" class="form-control aplicarSelect2" style="width: 100%">
                                                <option value="null" >Seleccione barrio</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['barrio'] == "No aplica" ? "selected='selected'" : null ?>>No aplica</option>
                                            <?php foreach ($barrios as $valor) { ?>
                                                <option value="<?php echo $valor['nombreBarrio'] ?>" <?php echo $tramite['predio']['barrio'] == $valor['nombreBarrio'] ? "selected='selected'" : null ?> ><?php echo $valor['codigoBarrio'] ?> - <?php echo $valor['nombreBarrio'] ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                            <label for="veredaUrb">Urbanización o vereda</label>
                                            <select id="veredaUrb" class="form-control aplicarSelect2Tag" style="width: 100%">
                                                <option value="null">Seleccione vereda o digite urbanización</option>
                                                <option value="No aplica" <?php echo $tramite['predio']['veredaUrb'] == "No aplica" ? "selected='selected'" : null ?> >No aplica</option>
                                                <?php if( !in_array( $tramite['predio']['veredaUrb'] , $veredas ) ){ ?>
                                                    <option value="<?php echo $tramite['predio']['veredaUrb']; ?>" selected="selected"><?php echo $tramite['predio']['veredaUrb']; ?></option>
                                                <?php } ?>
                                                <?php foreach ($veredas as $valor) { ?>
                                                    <option value="<?php echo $valor['nombreVereda'] ?>" <?php echo $tramite['predio']['veredaUrb'] == $valor['nombreVereda'] ? "selected='selected'" : null ?> ><?php echo $valor['codigoVereda'] ?> - <?php echo $valor['nombreVereda'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <h4>Información de titular</h4>
                                    <hr class="hr-bordeAbajo">
                                    <div class="col-lg-12">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6 text-nowrap">
                                            <label for="tipoDocIdentificacionTitular">Tipo documento</label>
                                            <select id="tipoDocIdentificacionTitular" class="form-control">
                                                <option value="">Seleccione tipo de documento</option>
                                                <?php foreach ($tipoDocIdentificacion as $valor) { ?>
                                                <option value="<?php echo $valor['tipoDocumentoSigla'] ?>" <?php echo $tramite['titular']['tipoDocIdentificacionTitular'] == $valor['tipoDocumentoSigla'] ? "selected='selected'" : null ?> ><?php echo $valor['tipoDocumentoSigla'] ?> - <?php echo $valor['tipoDocumentoId'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="numDocTitular">Número de documento</label>
                                            <input id="numDocTitular" value="<?php echo $tramite['titular']['numDocTitular'] ?>" type="number" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="nombreTitular">Nombre</label>
                                            <input id="nombreTitular" value="<?php echo $tramite['titular']['nombreTitular'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                            <label for="apellidoTitular">Apellido</label>
                                            <input id="apellidoTitular" value="<?php echo $tramite['titular']['apellidoTitular'] ?>" class="form-control" placeholder="Ingrese">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-group-justified" onclick="sisgac1.validarEditarTramite()">Editar <?php echo isset($tramite['codigoFormUnicNac']) ? "trámite" : "documento" ?></button>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="text-right">
                <button class="btn btn-success" onclick="$('#modalEditarTramite').modal('show')">Editar trámite</button>
                <button class="btn btn-danger" onclick="sisgac1.validarEliminarTramite({},'<?php echo $tramite['_id'] ?>' , '<?php echo isset($tramite['codigoFormUnicNac']) ? $tramite['codigoFormUnicNac'] : $tramite['codigoDocumento'] ?>')">Eliminar trámite</button>
            </div>
            <br>
            <?php } ?>
            <table class="table table-bordered table-hover table-responsive table-striped">
                <?php if(is_array($tramite['tipoTramites'])){ ?>
                <thead>
                <th colspan="12"><h3 class="text-center" style="margin: 0px auto ;">Trámite <?php echo $tramite['codigoFormUnicNac'] ?></h3></th>
                </thead>
                <tr>
                    <td colspan="3" class="col-lg-3"><strong>Tipo trámites</strong></td>
                    <td colspan="3" class="col-lg-3"><strong>Modalidades</strong></td>
                    <td colspan="3" class="col-lg-3"><strong>Tipo de usos</strong></td>
                    <td colspan="3" class="col-lg-3"><strong>Objeto de trámite</strong></td>
                </tr>
                <tr>
                    <td colspan="3"><?php for($i = 0; $i < sizeof($tramite['tipoTramites']); $i++){
                        foreach ($tipoTramites as $key => $tipo) {
                            foreach ($tipo as $key => $nombreTipo) {
                                echo $key == $tramite['tipoTramites'][$i] ? " - " . $nombreTipo . "<br>" : null;
                            }
                        }
                    } ?>
                    </td>
                    <td colspan="3"><?php for($i = 0; $i < sizeof($tramite['modalidades']); $i++){
                        foreach ($tipoModalidades as $key => $tipo) {
                            foreach ($tipo as $key => $nombreTipo) {
                                echo $key == $tramite['modalidades'][$i] ? " - " . $nombreTipo . "<br>" : null;
                            }
                        }
                    } ?>
                    </td>
                    <td colspan="3"><?php for($i = 0; $i < sizeof($tramite['tipoUsos']); $i++){
                        foreach ($tipoUsos as $key => $tipo) {
                            foreach ($tipo as $key => $nombreTipo) {
                                echo $key == $tramite['tipoUsos'][$i] ? " - " . $nombreTipo . "<br>" : null;
                            }
                        }
                    } ?>
                    </td>
                    <td colspan="3"><?php for($i = 0; $i < sizeof($tramite['tipoObjetoTramite']); $i++){
                        foreach ($tipoObjetos as $key => $tipo) {
                            foreach ($tipo as $key => $nombreTipo) {
                                echo $key == $tramite['tipoObjetoTramite'] ? $nombreTipo : null;
                            }
                        }
                    } ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><strong>Trámite</strong></td>
                    <td colspan="2"><strong>Licencia</strong></td>
                    <td colspan="2"><strong>Fecha ejecutoria</strong></td>
                    <td colspan="2"><strong>Fecha expedición</strong></td>
                    <td colspan="3"><strong>Fecha de legal y debida forma</strong></td>
                </tr>
                <tr>
                    <td colspan="2">TR-<?php echo $tramite['numConsecutivo']['consecutivoTramite'] . "-" . $tramite['anoRadicacion']['anoRadicacionTramite'] ?></td>
                    <td colspan="2"><?php echo $tramite['numConsecutivo']['consecutivoDocumento'] . "-" . $tramite['anoRadicacion']['anoRadicacionDocumento'] ?></td>
                    <td colspan="2"><?php echo date("d/m/Y", $tramite['fechaEjecutoria']->sec) ?></td>
                    <td colspan="2"><?php echo date("d/m/Y", $tramite['fechaExpedicion']->sec) ?></td>
                    <td colspan="3"><?php echo date("d/m/Y",$tramite['fechaLegalDebidaForma']->sec) ?></td>
                </tr>
                <tr>
                    <td colspan="4"><strong>Folio inicial</strong></td>
                    <td colspan="4"><strong>Folio final</strong></td>
                    <td colspan="4"><strong>Folios totales</strong></td>
                </tr>
                <tr>
                    <td colspan="4"><?php echo $tramite['FI_Tramite'] ?></td>
                    <td colspan="4"><?php echo $tramite['FF_Tramite'] ?></td>
                    <td colspan="4"><?php echo $tramite['FT_Tramite'] ?></td>
                </tr>
                <tr>
                    <td colspan="12" class="text-center"><h4 style="margin: 0px auto;">Información del predio</h4></td>
                </tr>
                <tr>
                    <td colspan="4" class="col-lg-4"><strong>Matrícula inmobiliaria</strong></td>
                    <td colspan="4" class="col-lg-4"><strong>Código catastral</strong></td>
                    <td colspan="4" class="col-lg-4"><strong>Dirección</strong></td>
                    
                </tr>
                <tr>
                    <td colspan="4"><?php echo $tramite['predio']['matriculaInmobiliaria'] ?></td>
                    <td colspan="4"><?php echo $tramite['predio']['codigoCatastral'] ?></td>
                    <td colspan="4"><?php $direccion = explode("/", $tramite['predio']['direccionNomenclatura']); echo $direccion[0]." ".$direccion[1]." Nº ".$direccion[2]." - ".$direccion[3] ?></td>
                </tr>
                <tr>
                    <td colspan="4" class="col-lg-4"><strong>Estrato</strong></td>
                    <td colspan="4" class="col-lg-4"><strong>Barrio</strong></td>
                    <td colspan="4" class="col-lg-4"><strong>Urbanización / Vereda</strong></td>
                </tr>
                <tr>
                    <td colspan="4"><?php echo $tramite['predio']['estrato'] ?></td>
                    <td colspan="4"><?php echo $tramite['predio']['barrio'] ?></td>
                    <td colspan="4"><?php echo $tramite['predio']['veredaUrb'] ?></td>
                </tr>
                <tr>
                    <td colspan="12" class="text-center"><h4 style="margin: 0px auto;">Información de personas</h4></td>
                </tr>
                <tr>
                    <td colspan="6" class="text-center"><strong>Titular</strong></td>
                    <td colspan="6" class="text-center"><strong>Solicitante</strong></td>
                </tr>
                <tr>
                    <td colspan="3"><strong>Documento titular</strong></td>
                    <td colspan="3"><strong>Nombre y apellidos</strong></td>
                    <td colspan="3"><strong>Documento solicitante</strong></td>
                    <td colspan="3"><strong>Nombre y apellidos</strong></td>
                </tr>
                <tr>
                    <td colspan="3"><?php echo $tramite['titular']['tipoDocIdentificacionTitular'] . " " . $tramite['titular']['numDocTitular'] ?></td>
                    <td colspan="3"><?php echo $tramite['titular']['nombreTitular'] . " " . $tramite['titular']['apellidoTitular'] ?></td>
                    <td colspan="3"><?php echo $tramite['solicitante']['tipoDocIdentificacionSolicitante'] != "" ? ($tramite['solicitante']['tipoDocIdentificacionSolicitante'] . " " . $tramite['solicitante']['numDocSolicitante']) : "No figura" ?></td>
                    <td colspan="3"><?php echo $tramite['solicitante']['nombreSolicitante'] != "" ? ($tramite['solicitante']['nombreSolicitante'] . " " . $tramite['solicitante']['apellidoSolicitante']) : "No figura" ?></td>
                </tr>
                <tr>
                    <td colspan="6" rowspan="2"></td>
                    <td colspan="2"><strong>Telefonos</strong></td>
                    <td colspan="2"><strong>Dirección</strong></td>
                    <td colspan="2"><strong>E-Mail</strong></td>
                </tr>
                <tr>
                    <td colspan="2"><?php echo $tramite['solicitante']['telefonoSolicitante'] != "" ? $tramite['solicitante']['telefonoSolicitante'] : "No figura" ?></td>
                    <td colspan="2"><?php echo $tramite['solicitante']['direccionSolicitante'] != "" ? $tramite['solicitante']['direccionSolicitante'] : "No figura" ?></td>
                    <td colspan="2"><?php echo $tramite['solicitante']['emailSolicitante'] != "" ? $tramite['solicitante']['emailSolicitante'] : "No figura" ?></td>
                </tr>
                <?php }else{ $tipoDocumento = null; 
                    foreach ($tipoTramites as $key => $tipo) {
                        foreach ($tipo as $key => $nombreTipo) {
                            if($key == $tramite['tipoTramites']){
                                $tipoDocumento = explode(" -", $nombreTipo);
                                break 2;
                            }
                        }
                    } ?>
                    <thead>
                        <th colspan="12"><h3 class="text-center" style="margin: 0px auto ;"><?php echo $tipoDocumento[0] . (isset($tipoDocumento[1]) ? $tipoDocumento[1] . "-" : " " ) . $tramite['codigoDocumento'] ?></h3></th>
                    </thead>
                    <tr>
                        <td colspan="3" class="col-lg-3"><strong>Tipo de documento</strong></td>
                        <td colspan="7" class="col-lg-7"><strong>Observaciones</strong></td>
                    </tr>
                    <tr>
                        <td colspan="3"><?php echo $tipoDocumento[0] ?></td>
                        <td colspan="7" class="text-justify" ><?php echo $tramite['observaciones'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="1"><strong>Folio inicial</strong></td>
                        <td colspan="1"><strong>Folio final</strong></td>
                        <td colspan="1"><strong>Folios totales</strong></td>
                        <td colspan="2"><strong><?php echo $tipoDocumento[0] ?></strong></td>
                        <td colspan="2"><strong>Fecha de expedición</strong></td>
                        <td colspan="2"><strong>Documento titular</strong></td>
                        <td colspan="2"><strong>Nombre y apellidos</strong></td>
                    </tr>
                    <tr>
                        <td><?php echo $tramite['FI_Tramite'] ?></td>
                        <td><?php echo $tramite['FF_Tramite'] ?></td>
                        <td><?php echo $tramite['FT_Tramite'] ?></td>
                        <td colspan="2"><?php echo (isset($tipoDocumento[1]) ? $tipoDocumento[1] . "-" : null ). $tramite['numConsecutivo'] . "-" . $tramite['anoRadicacion'] ?></td>
                        <td colspan="2"><?php echo date("d/m/Y",$tramite['fechaExpedicion']->sec) ?></td>
                        <td colspan="2"><?php echo $tramite['titular']['tipoDocIdentificacionTitular'] . " " . $tramite['titular']['numDocTitular'] ?></td>
                        <td colspan="2"><?php echo $tramite['titular']['nombreTitular'] . " " . $tramite['titular']['apellidoTitular'] ?></td>
                    </tr>
                    <tr>
                        <td colspan="12" class="text-center"><h4 style="margin: 0px auto;">Información del predio</h4></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="col-lg-4"><strong>Matrícula inmobiliaria</strong></td>
                        <td colspan="4" class="col-lg-4"><strong>Código catastral</strong></td>
                        <td colspan="4" class="col-lg-4"><strong>Dirección</strong></td>

                    </tr>
                    <tr>
                        <td colspan="4"><?php echo $tramite['predio']['matriculaInmobiliaria'] ?></td>
                        <td colspan="4"><?php echo $tramite['predio']['codigoCatastral'] ?></td>
                        <td colspan="4"><?php $direccion = explode("/", $tramite['predio']['direccionNomenclatura']); echo $direccion[0]." ".$direccion[1]." N ".$direccion[2]." - ".$direccion[3] ?></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="col-lg-4"><strong>Estrato</strong></td>
                        <td colspan="4" class="col-lg-4"><strong>Barrio</strong></td>
                        <td colspan="4" class="col-lg-4"><strong>Urbanización / Vereda</strong></td>
                    </tr>
                    <tr>
                        <td colspan="4"><?php echo $tramite['predio']['estrato'] ?></td>
                        <td colspan="4"><?php echo $tramite['predio']['barrio'] ?></td>
                        <td colspan="4"><?php echo $tramite['predio']['veredaUrb'] ?></td>
                    </tr>
                <?php } ?>
            </table>
            <script>
//                variable para crear más subida de archivos
                var divAgregarArchivo = '<div class="col-lg-5 col-md-5 col-sm-5"><input name="archivo[]" type="file" class="form-control" required="required"></div>\n\
                                        <div class="col-lg-5 col-md-5 col-sm-5"><input name="nombreArchivo[]" type="text" class="form-control" placeholder="Nuevo nombre archivo - Opcional"></div>\n\
                                        <div class="col-lg-2 col-md-2 col-sm-2 text-center"><button type="button" class="btn btn-danger btn-group-justified" onclick="this.parentNode.parentNode.remove()"><i class="glyphicon glyphicon-remove"></i><span class="hidden-sm"> Remover</span></button></div>\n\
                                        <br>';
                setTimeout(function(){ $("#estadoSubidaArchivo").hide(0); $(".estadoSubidaArchivoBoton").hide(0); $(".aplicarSelect2Tag").select2({ tags: true }); },3000);
                setInterval(function(){
                    $('.carousel').carousel('pause');
                },1000);
            </script>
            
            <div id="modalAgregarArchivo" class="modal fade" role="dialog" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Subir archivos a <?php echo isset($tramite['codigoFormUnicNac']) ? $tramite['codigoFormUnicNac'] : $tramite['codigoDocumento'] ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="row" style="margin: 0px">
                                <div id="estadoSubidaArchivo" class="col-lg-12" style="margin: 0px">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped active estadoSubidaArchivoTexto" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%; min-width: 2em"></div>
                                    </div>
                                </div>
                                <form id="formSubirArchivos" class="formSubirArchivos col-lg-12" style="margin: 0px" action="javascript: sisgac1.subirArchivo()" enctype="multipart/form-data">
                                    <input id="idTramite" name="idTramite" type="hidden" value="<?php echo $tramite['_id'] ?>">
                                    <div id="tablaAgregarArchivos">
                                        <div class="form-group">
                                            <div class="col-lg-5 col-md-5 col-sm-5"><input name="archivo[]" type="file" class="form-control" required="required"></div>
                                            <div class="col-lg-5 col-md-5 col-sm-5"><input name="nombreArchivo[]" type="text" class="form-control" placeholder="Nuevo nombre archivo - Opcional"></div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 text-center"><button type="button" class="btn btn-danger btn-group-justified" onclick="this.parentNode.parentNode.remove()"><i class="glyphicon glyphicon-remove"></i><span class="hidden-sm"> Remover</span></button></div>
                                            <br>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row" style="margin: 0px">
                                <div class="formSubirArchivos col-lg-12">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-success btn-group-justified" onclick="$('#formSubirArchivos').trigger('submit')">Subir archivos</button>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-default btn-group-justified" onclick="var miniForm = document.createElement('div');
                                                miniForm.setAttribute('class','form-group');
                                                miniForm.innerHTML = divAgregarArchivo;
                                                document.getElementById('tablaAgregarArchivos').appendChild(miniForm);
                                                " >Agregar otro</button>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-danger btn-group-justified" onclick="$('#modalAgregarArchivo').modal('hide')">Cancelar</button>
                                    </div>
                                </div>
                                <div class="estadoSubidaArchivoBoton col-lg-12 col-md-12 col-xs-12">
                                    <button type="button" class="btn btn-success btn-group-justified" onclick="location.reload()">Recargar página</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 30px;">
                <h3 class="text-center">Archivos relacionados</h3>
                <div class="text-center">
                    <button class="btn btn-default btn-group-justified" onclick="$('#modalAgregarArchivo').modal('show')"><i class="glyphicon glyphicon-plus"></i> Agregar archivo</button>
                </div>
                <?php if(is_array($archivos)){ ?>
                <div id="archivosPaginados" class="carousel slide">
                    <!--Controles-->
                    <nav style="position: inherit; margin-bottom: -25px;">
                        <ul class="pager">
                            <li class="previous">
                                <a style="left: -84px; background: rgba(255,255,255,1);" href="#archivosPaginados" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Anterior</span>
                                </a>
                            </li>
                            <li class="next">
                                <a style="right: -84px; background: rgba(255,255,255,1);" href="#archivosPaginados" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Siguiente</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!--Fin Controles-->
                    
                    <div class="carousel-inner" role="listbox" style="padding-bottom: 0px;">
                        <?php $totalMb = 0; $totalArchivos = 0; $cont = 0; 
                            foreach($archivos as $key => $paginaArchivos) { ?>
                        <div class="item <?php echo $cont == 0 ? 'active' : '' ?>">
                            <table class="table table-striped table-hover">
                                    <thead>
                                    <th>Nombre archivo</th>
                                    <th>Tamaño</th>
                                    <th>Eliminar</th>
                                    </thead>
                                    <?php foreach($paginaArchivos as $clave => $archivo) { ?>
                                    <tr>
                                        <td><a href="<?php echo URL . 'archivos/consultar/' . $archivo["_id"] . '/' .$archivo["filename"] ?>" target="blank"><?php echo $archivo["filename"] ?></a></td>
                                        <td><?php echo number_format($archivo["length"] / 1024**2) > 1 ? number_format($archivo["length"] / 1024**2,2) . "Mb" : number_format($archivo["length"] / 1024) . "Kb" ?></td>
                                        <td class="text-center"><button type="button" class="btn btn-danger btn-group-justified" onclick="sisgac1.validarEliminarArchivo(this,'<?php echo $archivo["_id"] ?>' , '<?php echo $archivo["filename"] ?>')"><i class="glyphicon glyphicon-remove"></i></button></td>
                                    </tr>
                                    <?php $totalMb += $archivo["length"] / 1024**2; $totalArchivos++; } ?>
                            </table>
                        </div>
                        <?php $cont == 0 ? $cont++ : null; } ?>
                    </div>
                    
                    <div class="text-center" style="background: rgba(127,127,127,0.25); border-radius: 0px 0px 50% 50%;">
                        <div class="active">
                            <a data-target="#archivosPaginados" data-slide-to="0" class="btn btn-default">Página inicial</a>
                        </div>
                    </div>
                    
                  </div>
                <?php echo "Almacenamiento: <strong>" . ($totalMb < 1024 ? number_format($totalMb,2) . "Mb" : number_format($totalMb / 1024,2) . "Gb") . "</strong> - Total archivos: <strong>" . $totalArchivos . "</strong>"; } ?>
            </div>
        </div>
    </div>
</div>
