<section class="callaction">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="" class="borde-sombreado fondo-BlancoSisgac1" >
                        <h3>Crear trámite</h3>
                        <hr class="colorgraph">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <h4>Información del trámite</h4>
                            <hr class="hr-bordeAbajo">
                            <div class="col-lg-12">
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="idTipoTramites">Tipos de trámites o actuaciones</label>
                                    <select id="idTipoTramites" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                        <option value="null">Seleccione tipo de trámite</option>
                                        <?php foreach ($tipoTramites as $valor) { ?>
                                            <option value="<?php echo $valor['_id'] ?>" ><?php echo $valor['tipoTramite'] ?><?php echo $valor['tipoTramiteSigla'] != '' ? ' - ' . $valor['tipoTramiteSigla'] : '' ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group soloTramite col-lg-9 col-md-9 col-sm-6">
                                    <label for="observaciones">Asunto u observaciones</label>
                                    <textarea id="observaciones" class="form-control" style="resize:vertical" maxlength="725" placeholder="Digite aquí las observaciones"></textarea>
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="idTipoModalidades">Modalidades</label>
                                    <select id="idTipoModalidades" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                        <option value="null">Seleccione modalidades del trámite</option>
                                        <?php foreach ($tipoModalidades as $valor) { ?>
                                            <option value="<?php echo $valor['_id'] ?>" ><?php echo $valor['tipoModalidad'] ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="idTipoUsosVivienda">Tipos de uso</label>
                                    <select id="idTipoUsosVivienda" multiple="multiple" class="aplicarSelect2" style="width: 100%">
                                        <option value="null">Seleccione uso de vivienda del trámite</option>
                                        <?php foreach ($usosVivienda as $valor) { ?>
                                            <option value="<?php echo $valor['_id'] ?>" ><?php echo $valor['usoVivienda'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="idObjetoTramite">Objeto del trámite</label>
                                    <select id="idObjetoTramite" class="form-control">
                                        <option value="null">Seleccione objeto del trámite</option>
                                        <?php foreach ($objetoTramite as $valor) { ?>
                                            <option value="<?php echo $valor['_id'] ?>" ><?php echo $valor['tipoObjetoTramite'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label>Seleccione creación de:</label>
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="radio" name="esTramite" value="true" onchange="
                                                $('.soloTramite').hide(400,function(){ 
                                                    $('.soloDocumento').show(400); 
                                                });" autocomplete="off" > <i class="glyphicon glyphicon-inbox"></i> Trámite
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="esTramite" value="false" onchange="
                                                    $('.soloDocumento').hide(400,function(){
                                                        $('.soloTramite').show(400);
                                                    });" autocomplete="off" ><i class="glyphicon glyphicon-file"></i> Documento 
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="folioInicial">Folio inicial</label>
                                    <input id="folioInicial" type="number" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="folioFinal">Folio final</label>
                                    <input id="folioFinal" type="number" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="totalFolios">Total folios</label>
                                    <input id="totalFolios" type="number" class="form-control" placeholder="Ingrese">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div  class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                    <label for="consecutivoTramite">Número de consecutivo de trámite</label>
                                    <input id="consecutivoTramite" type="number" class="form-control" placeholder="Ej: 515, 1098, 798...">
                                </div>
                                <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                    <label for="anoRadicacionTramite">Año radicación</label>
                                    <input id="anoRadicacionTramite" type="number" class="form-control" placeholder="Ej: 2001, 2016, 1998">
                                </div>
                                <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-12">
                                    <label for="fechaEjecutoria">Fecha ejecutoria</label>
                                    <input id="fechaEjecutoria" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="consecutivoDocumento">Consecutivo <span class="hidden-md hidden-sm hidden-xs">de</span> documento</label>
                                    <input id="consecutivoDocumento" type="number" class="form-control" placeholder="Ej: 515, 1098, 798...">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="anoRadicacionDocumento">Año de documento</label>
                                    <input id="anoRadicacionDocumento" type="number" class="form-control" placeholder="Ej: 2001, 2016, 1998">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="fechaExpedicion">Fecha de expedición</label>
                                    <input id="fechaExpedicion" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="fechaLegalDebidaForma">Fecha <span class="hidden-md hidden-sm hidden-xs">de</span> legal/debida forma</label>
                                    <input id="fechaLegalDebidaForma" type="date" min="1990-01-01" max="<?php echo date("Y-m-d") ?>" class="form-control" placeholder="Ingrese">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <h4>Información del predio</h4>
                            <hr class="hr-bordeAbajo">
                            <div class="col-lg-12">
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="matriculaInmobiliaria">Matrícula inmobiliaria</label>
                                    <input id="matriculaInmobiliaria" type="text" class="form-control" placeholder="(con guiones)">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="codigoCatastral">Código catastral</label>
                                    <input id="codigoCatastral" placeholder="(sin guiones)" maxlength="20" onblur='if(this.value.trim().length < 20){ var calculo = 20 - parseInt(this.value.length);  var ceros = ""; for(var i = 0; i < calculo; i++){ ceros += "0"; }; this.value = this.value + ceros; $(this).trigger("keyup") }' onkeyup='if(this.value.length != 20){ this.setAttribute("style", "border-color: #d9232d"); }else { this.setAttribute("style", "border-color: green"); }' type="number" class="form-control">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="direccionNomenclatura">Dirección</label>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <select name="direccionNomenclatura" class="btn btn-default dropdown-toggle" >
                                                <option value="CL">CL</option>
                                                <option value="CR">CR</option>
                                                <option value="DG">DG</option>
                                                <option value="TV">TV</option>
                                                <option value="VI">VI</option>
                                            </select>
                                        </div>
                                        <input id="direccionNomenclatura" name="direccionNomenclatura" type="text" class="form-control">
                                        <div class="input-group-addon">
                                            Nº
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <div class="col-lg-6">
                                        <label for="direccionNomenclatura">&nbsp;</label>
                                        <input name="direccionNomenclatura" type="text" class="form-control" placeholder="">
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="direccionNomenclatura">&nbsp;</label>
                                        <input name="direccionNomenclatura" type="text" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-6">
                                    <label for="estrato">Estrato</label>
                                    <select id="estrato" class="form-control">
                                        <option value="null">Seleccione estrato</option>
                                        <option value="No aplica">No aplica</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-6">
                                    <?php  ?>
                                    <label for="barrio">Barrio</label>
                                    <select id="barrio" class="form-control aplicarSelect2" style="width: 100%">
                                        <option value="null" >Seleccione barrio</option>
                                        <option value="No aplica" >No aplica</option>
                                    <?php foreach ($barrios as $valor) { ?>
                                        <option value="<?php echo $valor['nombreBarrio'] ?>" ><?php echo $valor['codigoBarrio'] ?> - <?php echo $valor['nombreBarrio'] ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-4 col-md-4 col-sm-6">
                                    <label for="veredaUrb">Urbanización o vereda</label>
                                    <select id="veredaUrb" class="form-control aplicarSelect2Tag" style="width: 100%">
                                        <option value="null">Seleccione vereda o digite urbanización</option>
                                        <option value="No aplica" >No aplica</option>
                                        <?php foreach ($veredas as $valor) { ?>
                                            <option value="<?php echo $valor['nombreVereda'] ?>" ><?php echo $valor['codigoVereda'] ?> - <?php echo $valor['nombreVereda'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <h4>Información de personas</h4>
                            <hr class="hr-bordeAbajo">
                            <div class="col-lg-12">
                                <div class="form-group col-lg-3 col-md-3 col-sm-6 text-nowrap">
                                    <label for="tipoDocIdentificacionTitular">Tipo de identificación titular</label>
                                    <select id="tipoDocIdentificacionTitular" class="form-control">
                                        <option value="">Seleccione tipo de documento</option>
                                        <?php foreach ($tipoDocIdentificacion as $valor) { ?>
                                        <option value="<?php echo $valor['tipoDocumentoSigla'] ?>" ><?php echo $valor['tipoDocumentoSigla'] ?> - <?php echo $valor['tipoDocumentoId'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="numDocTitular">Documento titular</label>
                                    <input id="numDocTitular" type="number" class="form-control" placeholder="Ingrese">
                                </div>
                                
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="nombreTitular">Nombre titular</label>
                                    <input id="nombreTitular" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                    <label for="apellidoTitular">Apellido titular</label>
                                    <input id="apellidoTitular" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="tipoDocIdentificacionSolicitante">Tipo doc solicitante</label>
                                    <select id="tipoDocIdentificacionSolicitante" class="form-control">
                                        <option value="">Seleccione tipo de documento</option>
                                        <?php foreach ($tipoDocIdentificacion as $valor) { ?>
                                        <option value="<?php echo $valor['tipoDocumentoSigla'] ?>" ><?php echo $valor['tipoDocumentoSigla'] ?> - <?php echo $valor['tipoDocumentoId'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="numDocSolicitante">Num doc solicitante</label>
                                    <input id="numDocSolicitante" type="number" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="nombreSolicitante">Nombre Solicitante</label>
                                    <input id="nombreSolicitante" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-3 col-md-3 col-sm-6">
                                    <label for="apellidoSolicitante">Apellido Solicitante</label>
                                    <input id="apellidoSolicitante" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                    <label for="telefonoSolicitante">Teléfono solicitante (separado con guiones)</label>
                                    <input id="telefonoSolicitante" type="tel" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6 text-nowrap">
                                    <label for="direccionSolicitante">Dirección de correspondencia de solicitante</label>
                                    <input id="direccionSolicitante" class="form-control" placeholder="Ingrese">
                                </div>
                                <div class="form-group soloDocumento col-lg-4 col-md-4 col-sm-6">
                                    <label for="emailSolicitante">E-Mail de solicitante</label>
                                    <input id="emailSolicitante" class="form-control" placeholder="Ingrese">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" onclick="sisgac1.validarCrearTramite()" class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12">Crear trámite</button>
                            </div>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
    </section>

<script>
    window.setTimeout(function(){
        $('.soloDocumento').show(0);
        $('.soloTramite').hide(0);
    },500)
</script>