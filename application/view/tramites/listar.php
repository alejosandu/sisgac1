<script>
    setTimeout(function(){
        document.getElementById("divHidden").setAttribute("class","row");
        $('#buscarTramite').hide(0);
        $('#buscarDocumento').hide(0);
        $('#buscarTitular').hide(0);
        $('#buscarFecha').hide(0);
        $('#buscarPredio').hide(0);
    },1000);
    
    var idFormulario = null;
    var idFormularioAnterior = null;
    var tiempo = 250;
</script>
    <section class="callaction">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 borde-sombreado fondo-BlancoSisgac1">
                    <h3>Consultar trámites y documentos</h3>
                    <div class="row" style="margin-bottom: 0px;">
                        <div class="form-group col-lg-5 col-md-12 col-sm-12 col-xs-12">
                            <select class="form-control btn btn-default" onchange="
                                    if(idFormularioAnterior == null){
                                        idFormulario = this.value;
                                        idFormularioAnterior = idFormulario;
                                        $(idFormulario).show(tiempo);
                                    }else if(this.value == 'ocultar'){
                                        $(idFormulario).hide(tiempo);
                                    }else{
                                        idFormularioAnterior = idFormulario;
                                        idFormulario = this.value;
                                        $(idFormularioAnterior).hide(tiempo, function(){ $(idFormulario).show(tiempo) });
                                    }
                                    ">
                                <option value="ocultar">Elija un parámetro para buscar</option>
                                <option value="#buscarTitular">Titulares y solicitantes</option>
                                <option value="#buscarTramite">Trámites y licencias</option>
                                <option value="#buscarDocumento">Documentos y otras actucaciones</option>
                                <option value="#buscarFecha">Trámites y documentos por fechas</option>
                                <option value="#buscarPredio">Información de predio</option>
                            </select>
                        </div>
                        <form id="resetearParametros" class="col-lg-4 col-md-6 col-sm-6 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <button name="parametro" class="btn btn-default btn-group-justified" value="reset">Listar todos</button>
                        </form>
                        <div class="form-inline col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <label>Listar:</label>
                            <select id="listar" class="form-control">
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="75">75</option>
                            </select>
                            <button type="button" class="btn btn-success" onclick="location.href = '<?php echo URL ?>tramites/listar/' + $('#listar').val() + '/' + '<?php echo $numeroPagina ?>' "><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                    </div>
                    <div id="divHidden" class="row hidden" style="margin-bottom: 0px;">
                        <form id="buscarTitular" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <select id="busquedaConsecutivo" class="btn btn-default dropdown-toggle" name="parametro" required="required">
                                        <option value="">Seleccione una opción</option>
                                        <optgroup label="Titular">
                                            <option value="titular.numDocTitular">Número de documento</option>
                                            <option value="titular.nombreTitular">Nombre de titular</option>
                                            <option value="titular.apellidoTitular">Apellido de titular</option>
                                        </optgroup>
                                        <optgroup label="Solicitante">
                                            <option value="titular.numDocTitular">Número de documento</option>
                                            <option value="solicitante.nombreSolicitante">Nombre de solicitante</option>
                                            <option value="solicitante.apellidoSolicitante">Apellido de solicitante</option>
                                        </optgroup>
                                        
                                    </select>
                                </div>
                                <input name="valor" pattern="[A-Za-z0-9\-\. ÁáÉéÍíÓóÚúÜü]+" title="No se admiten caracteres especiales, solo se admite formato alfanumérico" placeholder="" type="text" class="form-control" aria-label="..." required="required">
                                <span class="input-group-btn">
                                    <button name="busquedaRegex" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                </span>
                            </div>
                        </form>
                        <form id="buscarTramite" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <select id="busquedaConsecutivo" class="btn btn-default dropdown-toggle" name="parametro" required="required">
                                        <option value="">Seleccione una opción</option>
                                        <optgroup label="Trámites">
                                            <option value="codigoFormUnicNac">Código de trámite</option>
                                            <option value="numConsecutivo.consecutivoTramite">Consecutivo de trámite</option>
                                            <option value="anoRadicacion.anoRadicacionTramite">Año radicación trámite</option>
                                        </optgroup>
                                        <optgroup label="Licencias">
                                            <option value="numConsecutivo.consecutivoDocumento">Consecutivo de licencia</option>
                                            <option value="anoRadicacion.anoRadicacionDocumento">Año expedición licencia</option>
                                        </optgroup>
                                        
                                    </select>
                                </div>
                                <input name="valor" pattern="[A-Za-z0-9\-\. ÁáÉéÍíÓóÚúÜü]+" title="No se admiten caracteres especiales, solo se admite formato alfanumérico" placeholder="" type="text" class="form-control" aria-label="..." required="required">
                                <span class="input-group-btn">
                                    <button name="busqueda" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                </span>
                            </div>
                        </form>
                        <form id="buscarDocumento" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <select id="busquedaConsecutivo" class="btn btn-default dropdown-toggle" name="parametro" required="required">
                                        <option value="">Seleccione una opción</option>
                                        <option value="codigoDocumento">Código de documento</option>
                                        <option value="numConsecutivo">Consecutivo de documento</option>
                                        <option value="anoRadicacion">Año radicación de documento</option>
                                    </select>
                                </div>
                                <input name="valor" pattern="[A-Za-z0-9\-\. ÁáÉéÍíÓóÚúÜü]+" title="No se admiten caracteres especiales, solo se admite formato alfanumérico" placeholder="Digite código" type="text" class="form-control" aria-label="..." required="required">
                                <span class="input-group-btn">
                                    <button name="busqueda" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                </span>
                            </div>
                        </form>
                        <form id="buscarFecha" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <select id="busquedaConsecutivo" class="btn btn-default dropdown-toggle" name="parametro" required="required">
                                        <option value="">Seleccione una opción</option>
                                        <option value="fechaEjecutoria">Fecha ejecutoria</option>
                                        <option value="fechaExpedicion">Fecha de expedición</option>
                                        <option value="fechaLegalDebidaForma">Fecha legal y debida forma</option>
                                    </select>
                                </div>
                                <input name="valor" pattern="[A-Za-z0-9\-\. ÁáÉéÍíÓóÚúÜü]+" title="No se admiten caracteres especiales, solo se admite formato alfanumérico" placeholder="Digite código" type="date" class="form-control" aria-label="..." required="required">
                                <span class="input-group-btn">
                                    <button name="busquedaFecha" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                </span>
                            </div>
                        </form>
                        <form id="buscarPredio" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" method="post" action="<?php echo URL ?>tramites/listar">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <select id="busquedaConsecutivo" class="btn btn-default dropdown-toggle" name="parametro" required="required">
                                        <option value="">Seleccione una opción</option>
                                        <option value="predio.matriculaInmobiliaria">Matrícula inmobiliaria</option>
                                        <option value="predio.codigoCatastral">Código catastral</option>
                                    </select>
                                </div>
                                <input name="valor" maxlength="20" onblur='if(this.value.trim().length >= 12){ var calculo = 20 - parseInt(this.value.length);  var ceros = ""; for(var i = 0; i < calculo; i++){ ceros += "0"; }; this.value = this.value + ceros; $(this).trigger("keyup") }' pattern="[A-Za-z0-9\-\. ÁáÉéÍíÓóÚúÜü]+" title="No se admiten caracteres especiales, solo se admite formato alfanumérico" placeholder="Digite código" type="text" class="form-control" aria-label="..." required="required">
                                <span class="input-group-btn">
                                    <button name="busqueda" class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>	
            </div>
        </div>
    </section>
    
    <section id="content" class="container-sisgac1">
        
        <div class="container">
            
            <div class="row" style="margin-bottom: 0px">
            <!--PAGINACIÓN-->
            <div class="text-center">
                <ul class="pagination">
                <?php if($numeroPagina > 1){ ?> 
                <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/1/' : '25/1/'; ?>"> <i class="glyphicon glyphicon-fast-backward"></i> </a></li>
                <?php } ?>

                <?php if($numeroPagina > 1){ ?> 
                <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo ($numeroPagina-1).'/'; ?>"> <i class="glyphicon glyphicon-backward"></i> </a></li>
                <?php } ?>

                <?php for($i = $numeroPagina - 9; $i <= $numeroPagina + 9; $i++){ 
                    if($i > 0 && $i <= $totalPaginas){ ?>
                    <li <?php echo $numeroPagina == $i ? " class='active'" : "" ?>><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo $i.'/'; ?>" > <?php echo $i ?></a></li>
                <?php }
                    } ?>

                <?php if($numeroPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo ($numeroPagina+1).'/'; ?>" > <i class="glyphicon glyphicon-forward"></i> </a></li>
                <?php } ?>

                <?php if($numeroPagina < $totalPaginas){ ?> 
                <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo $totalPaginas.'/'; ?>" > <i class="glyphicon glyphicon-fast-forward"></i> </a></li>
                <?php } ?>
            </ul>
            </div>
        </div>
            <!--INFORMACION DE CONSULTA-->
            <div class="row">
                <div class="text-center">
                    <strong>Total páginas: <?php echo number_format($totalPaginas) ?> - Total registros: <?php echo number_format($totalTramites) ?> </strong>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-hover table-bordered table-striped table-responsive borde-sombreado" >
                        <thead>
                            <th class="col-lg-2">Código</th>
                            <th class="col-lg-2">Tipo</th>
                            <th class="col-lg-2 hidden-xs">Documento</th>
                            <th class="col-lg-2 hidden-xs">Titular</th>
                            <th class="col-lg-2 hidden-xs">Dirección predio</th>
                            <th class="col-lg-1">Fecha de expedición</th>
                            <?php if($permisoSuperAdmin || $permisoAdmin){ ?>
                            <th class="text-center col-lg-1 hidden-xs"><i class="glyphicon glyphicon-remove"></i></th>
                            <?php } ?>
                        </thead>
                        <tbody>
                            <?php if(sizeof($listaTramites) > 0){
                                foreach ($listaTramites as $key => $value) {
                              ?>
                                <tr>
                                    <td><a href="<?php echo URL . 'tramites/consultar/' . $value["_id"] ?>"><?php echo isset($value['codigoFormUnicNac']) ? $value['codigoFormUnicNac'] : $value['codigoDocumento']; ?></a></td>
                                    <td><?php if(is_array($value['tipoTramites'])){
                                        //valido si es array, o sea, si es trámite
                                        for($i = 0; $i < sizeof($value['tipoTramites']); $i++){
                                            foreach ($tipos as $key => $tipo) {
                                                foreach ($tipo as $key => $nombreTipo) {
                                                    echo $key == $value['tipoTramites'][$i] ? " - " . $nombreTipo . "<br>" : null;
                                                }
                                            }
                                        }
                                    }else{
                                        foreach ($tipos as $key => $tipo) {
                                            foreach ($tipo as $key => $nombreTipo) {
                                                echo $key == $value['tipoTramites'] ? $nombreTipo . "<br>" : null;
                                            }
                                        }
                                    } ?>
                                    </td>
                                    <td class="hidden-xs"><?php echo $value['titular']['tipoDocIdentificacionTitular'] . " " . $value['titular']['numDocTitular']; ?></td>
                                    <td class="hidden-xs"><?php echo $value['titular']['nombreTitular'] . " " . $value['titular']['apellidoTitular']; ?></td>
                                    <td class="hidden-xs"><?php $direccion = explode("/", $value['predio']['direccionNomenclatura']); echo $direccion[0]." ".$direccion[1]." Nº ".$direccion[2]." - ".$direccion[3] ?></td>
                                    <td><?php echo date("d/m/Y", $value['fechaExpedicion']->sec); ?></td>
                                    <?php if($permisoSuperAdmin || $permisoAdmin){ ?>
                                    <td class="text-center hidden-xs"><button onclick="sisgac1.validarEliminarTramite(this,'<?php echo $value["_id"]; ?>','<?php echo isset($value['codigoFormUnicNac']) ? $value['codigoFormUnicNac'] : $value['codigoDocumento']; ?>')" class="btn btn-danger btn-group-justified"><i class="glyphicon glyphicon-remove"></i></button></td>
                                    <?php } ?>
                                </tr>
                            <?php }
                            
                            }else{ ?>
                                <tr class="warning text-center"><td colspan="7">No se encontraron registros de trámites</td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                
                <div class="row" style="margin-bottom: 0px">
                    <!--PAGINACIÓN-->
                    <div class="text-center">
                    <ul class="pagination">
                        <?php if($numeroPagina > 1){ ?> 
                        <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/1/' : '25/1/'; ?>"> <i class="glyphicon glyphicon-fast-backward"></i> </a></li>
                        <?php } ?>

                        <?php if($numeroPagina > 1){ ?> 
                        <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo ($numeroPagina-1).'/'; ?>"> <i class="glyphicon glyphicon-backward"></i> </a></li>
                        <?php } ?>

                        <?php for($i = $numeroPagina - 9; $i <= $numeroPagina + 9; $i++){ 
                            if($i > 0 && $i <= $totalPaginas){ ?>
                            <li <?php echo $numeroPagina == $i ? " class='active'" : "" ?>><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo $i.'/'; ?>" > <?php echo $i ?></a></li>
                        <?php }
                            } ?>

                        <?php if($numeroPagina < $totalPaginas){ ?> 
                        <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo ($numeroPagina+1).'/'; ?>" > <i class="glyphicon glyphicon-forward"></i> </a></li>
                        <?php } ?>

                        <?php if($numeroPagina < $totalPaginas){ ?> 
                        <li><a href="<?php echo URL . "tramites/listar/"; echo $registrosPorPagina ? $registrosPorPagina.'/' : '25/'; echo $totalPaginas.'/'; ?>" > <i class="glyphicon glyphicon-fast-forward"></i> </a></li>
                        <?php } ?>
                    </ul>
                    </div>
                </div>
            </div>
            
            
        </div>
        
    </section>


