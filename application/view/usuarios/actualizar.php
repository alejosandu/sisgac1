<section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="padding: 1px 15px 15px 15px; background-color: rgba(255,255,255,1); box-shadow: rgba(0,0,0,0.1) 0px 0px 5px 1px">
                <form id="formActualizarUsuario" role="form" method="post" action="" class="register-form" >
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Editar usuario</h3>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <input value="<?php echo $datosUsuario['nombreUsuario'] ?>" id="nombreUsuario" type="text" name="nombreUsuario" onkeyup="sisgac1.validarNombreUsuarioLibre(true)" class="form-control" placeholder="Nombre de usuario*">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <input value="<?php echo $datosUsuario['email'] ?>" id="email" type="text" name="email" onkeyup="sisgac1.validarEmailLibre(true)" class="form-control" placeholder="E-mail*">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <input value="<?php echo $datosUsuario['nombre'] ?>" id="nombre" type="text" name="nombre" class="form-control" placeholder="Nombre*">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <input value="<?php echo $datosUsuario['apellido'] ?>" id="apellido" type="text" name="apellido" class="form-control" placeholder="Apellido*">
                            </div>
                            <button type="button" name="btnActualizarUsuario" onclick="sisgac1.validarActualizarUsuario()" class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12">Actualizar datos</button>
                        </div>
                    </div>
                </form>
                <form id="formActualizarPass" role="form" method="post" action="" class="register-form">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Cambiar contraseña</h3>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <input id="oldPass" type="password" name="oldPass" class="form-control"  placeholder="Contraseña actual*">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <input id="newPass" type="password" name="newPass" class="form-control"  placeholder="Nueva contraseña*">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <input id="newPassReapeat" type="password" name="newPassRespeat" 
                                       onkeyup="if (document.getElementById('newPass').value.trim() == document.getElementById('newPassReapeat').value.trim()) {
                                                   document.getElementById('newPass').setAttribute('style', 'border-color: green');
                                                   document.getElementById('newPassReapeat').setAttribute('style', 'border-color: green');
                                               }else{ 
                                                   document.getElementById('newPass').setAttribute('style', 'border-color: #d9232d');
                                                   document.getElementById('newPassReapeat').setAttribute('style', 'border-color: #d9232d');
                                                }" class="form-control"  placeholder="Repetir nueva contraseña*">
                            </div>
                            <p>La contraseña debe contener <strong>mínimo 8 caracteres.</strong></p>
                            <p>Tenga en cuenta que si coloca espacios al principio o al final, estos <strong>no</strong> serán tomados en cuenta como caracteres.</p>
                            <button type="button" name="btnActualizarPass" onclick="sisgac1.validarCambiarPass()" class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12">Cambiar contraseña</button>
                        </div>
                    </div>
                </form>
            </div>	
        </div>
    </div>
</section>