
<section id="content" style="padding: 40px 0px; margin-bottom: -50px;">
    <div class="container-fluid">
        <div class="row">
            <table id="tablaDatosUsuarios" class="table table-striped table-hover borde-sombreado text-justify" style="border: solid #f2f2f2 1px; padding: 0px 10px;">
                <thead>
                <th>Usuario</th>
                <th class="hidden-xs">Nombre</th>
                <th class="hidden-xs">Apellido</th>
                <th class="hidden-xs">E-mail</th>
                <th><i class="glyphicon glyphicon-signal"></i> Estado</th>
                <th><i class="glyphicon glyphicon-wrench"></i> Cambiar rol</th>
                <?php if($permisoSuperAdmin){ ?>
                <th class="hidden-sm hidden-xs"><i class="glyphicon glyphicon-home"></i> Cambiar de curaduria</th>
                <?php } ?>
                </thead>
                <tbody id="bodyTablaDatosUsuarios">
                    <?php if(sizeof($usuarios) > 0){
                        foreach ($usuarios as $clave => $value) { ?>
                        <tr>
                            <td><?php echo $usuarios[$clave]['nombreUsuario'] ?></td>
                            <td class="hidden-xs"><?php echo $usuarios[$clave]['nombre'] ?></td>
                            <td class="hidden-xs"><?php echo $usuarios[$clave]['apellido'] ?></td>
                            <td class="hidden-xs"><a class="text-primary btn-link" href="mailto:<?php echo $usuarios[$clave]['email'] ?>"><?php echo $usuarios[$clave]['email'] ?></a></td>
                            <td>
                                <div class="btn-group" data-toggle="buttons">
                                    <!--si el estado es activo-->
                                    <label class="btn btn-success btn-sm <?php if($usuarios[$clave]['estado'] == true){ echo 'active'; } ?>">
                                        <input type="radio" name="estadoUsuario" onchange="sisgac1.cambiarEstadoUsuario( '<?php echo $usuarios[$clave]['_id'] ?>', true , '<?php echo $usuarios[$clave]['nombreUsuario'] ?>' )" id="estadoUsuarioActivo" autocomplete="off" <?php echo $usuarios[$clave]['estado'] ? ' checked="checked" ' : ''  ?> > <i class="glyphicon glyphicon-ok hidden-xs"></i> Activo
                                    </label>
                                    <label class="btn btn-default btn-sm <?php if($usuarios[$clave]['estado'] == false){ echo 'active'; } ?>">
                                        <input class="" type="radio" name="estadoUsuario" onchange="sisgac1.cambiarEstadoUsuario( '<?php echo $usuarios[$clave]['_id'] ?>', false, '<?php echo $usuarios[$clave]['nombreUsuario'] ?>' )" id="estadoUsuarioInactivo" autocomplete="off" <?php echo $usuarios[$clave]['estado'] ? '' : ' checked="checked" ' ?> >  Inactivo <i class="glyphicon glyphicon-remove hidden-xs"></i>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <select id="tipoUsuario" class="form-control btn btn-default btn-miniSisgac1" onchange="sisgac1.editarUsuario('<?php echo $usuarios[$clave]['_id'] ?>' , '<?php echo $usuarios[$clave]['nombreUsuario'] ?>', this.value )" name="tipoUsuario">
                            <?php foreach ($tipoUsuarios as $valor) { ?>
                                <option value="<?php echo $valor['_id']?>" <?php echo $valor['_id'] == $usuarios[$clave]['tipoUsuario']  ? " selected='selected'" : "" ?>><?php echo $valor['tipoUsuario'] ?></option>
                            <?php } ?>
                            </select>
                            </td>
                            <?php if($permisoSuperAdmin){ ?>
                            <td class="hidden-sm hidden-xs">
                                <select id="idNuevaCuraduria" class="aplicarSelect2" name="idNuevaCuraduria" onchange="sisgac1.cambiarCuraduriaUsuario('<?php echo $usuarios[$clave]['_id'] ?>' , '<?php echo $usuarios[$clave]['nombreUsuario'] ?>' , this.value)">
                            <?php foreach ($curadurias as $valor) { ?>
                                    <option value="<?php echo $valor['_id'] ?>" <?php echo $valor['_id'] == $usuarios[$clave]['curaduria'] ? " selected='selected'" : "" ?>><?php echo $valor['nombreCuraduria'] ?></option>
                            <?php } ?>
                                </select>
                            </td>
                            <?php } ?>
                        </tr>
                    <?php }
                    }else{ ?>
                <td colspan="6" class="text-center text-danger">No se encontraron usuarios para listar.</td>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>

</section>

