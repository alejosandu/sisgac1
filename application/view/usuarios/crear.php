
    <section class="callaction">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form id="formCrearNuevoUsuario" role="form" method="post" action="" class="register-form borde-sombreado" style="padding: 1px 15px 15px 15px; background-color: rgba(255,255,255,1);">
                        <h3>Crear usuario</h3>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <input id="nombreUsuario" type="text" name="nombreUsuario" onkeyup="sisgac1.validarNombreUsuarioLibre(false)" class="form-control"  placeholder="Nombre de usuario*">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <input id="email" type="text" name="email" onkeyup="sisgac1.validarEmailLibre(false)" class="form-control" placeholder="E-mail*">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <input id="nombre" type="text" name="nombre" class="form-control" placeholder="Nombre*">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <input id="apellido" type="text" name="apellido"class="form-control" placeholder="Apellido*">
                        </div>
                        
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <select id="tipoUsuario" class="form-control" name="tipoUsuario">
                                <option value="null">Seleccione rol*</option>
                            <?php foreach ($tipoUsuarios as $valor) { ?>
                                <option value="<?php echo $valor['_id']?>"><?php echo $valor['tipoUsuario'] ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <?php if(isset($curadurias)){ ?>
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            
                            <select id="curaduria" class="form-control aplicarSelect2" style="width: 100%" name="curaduria">
                                <option value="null">Seleccione curaduría*</option>

                            <?php foreach ($curadurias as $valor) { ?>
                                <option value="<?php echo $valor['_id'] ?>"><?php echo $valor['nombreCuraduria'] ?></option>
                            <?php } ?>
                            </select>
                        </div>
                        <?php }else{ ?>
                        <input id="curaduria" type="hidden" value="" >
                        <?php } ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" name="btnCrearNuevoUsuario" onclick="sisgac1.validarCrearNuevoUsuario()" class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12">Crear</button>
                            </div>
                        </div>
                    </form>
                </div>	
            </div>
        </div>
    </section>
    
</div>
