<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Departamentos</h3>
        </div>
        <div class="modal-body modal-fixSisgac1" >
            
            <input id="idAjuste" type="hidden">
            
        <div onkeydown="this.onkeyup = function (evt) {
                if (evt.keyCode == 13) {
                    sisgac1.validarCrearDepartamento(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'nombreDepartamento': $('#campo1').val().trim(),
                                'codigoDepartamento': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarDepartamentos'
                            }
                    )
                }
            }">
            
            <div class="form-group">
                <label for="campo1">Nombre del departamento</label>
                <input id="campo1" placeholder="Amazonas, Antioquia..." class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="campo2">Código DANE (Número con ceros incluidos)</label>
                <input id="campo2" placeholder="Código según el DANE Ej: 05,27..." class="form-control" type="text">
            </div>
        </div>

        <button onclick="sisgac1.validarCrearDepartamento(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'nombreDepartamento': $('#campo1').val().trim(),
                                'codigoDepartamento': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarDepartamentos'
                            }
                    )" 
                type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar departamento</button>
                <button onclick="sisgac1.validarActualizarDepartamento(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            'idAjuste',
                            {
                                'nombreDepartamento': $('#campo1').val().trim(),
                                'codigoDepartamento': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarDepartamentos'
                            }
                    )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar departamento</button>
    </div>
        
        <div class="modal-footer">
        <table class="table table-hover table-striped table-hover">
            <thead>
            <th class="text-center">Departamentos registrados</th>
            <th class="text-center">Código DANE</th>
            <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
            </thead>
            <tbody id="tablaDatosAjustes" class="text-center">
                <?php
                if (sizeof($departamentos) > 0) {
                    foreach ($departamentos as $clave) { ?>
                        <tr>
                            <td><?php echo $clave['nombreDepartamento'] ?></td>
                            <td><?php echo $clave['codigoDepartamento'] ?></td>
                            <td><button onclick="sisgac1.cargarInfoAjusteForm( '<?php echo $clave['_id'] ?>' , '<?php echo $clave['nombreDepartamento'] ?>' , '<?php echo $clave['codigoDepartamento'] ?>' )" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>
                        </tr>
                    <?php }
                } else {
                    ?>
                    <tr class="warning"><td colspan="3">No se encontraron departamentos</td></tr>
<?php } ?>

            </tbody>
        </table>
    </div>
    </div>
</div>