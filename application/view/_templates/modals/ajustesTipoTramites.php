<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Tipos de trámites</h3>
        </div>
                
        <div class="modal-body modal-fixSisgac1">
            
            <input id="idAjuste" type="hidden">
            
            <div onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ sisgac1.validarCrearAjuste(
                                ['campo1','campo2'],
                                ['texto','texto'],
                                [true,false],
                                'colTipoTramites',
                                {
                                    'tipoTramite': $('#campo1').val().trim(),
                                    'tipoTramiteSigla': $('#campo2').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'consultarTipoTramites'
                                }
                            ) } }">
                    <div class="form-group">
                        <label for="campo1">Nombre de nuevo tipo de trámite</label>
                        <input id="campo1" placeholder="Licencia de parcelación, construcción, subdivisión... "  class="form-control" type="text">
                    </div>
                    <div class="form-group">
                        <label for="campo2">Sigla (Opcional)</label>
                        <input id="campo2" placeholder="RL, O (oficio), RLU, RD, RN, N (norma)... "  class="form-control" type="text">
                    </div>
                </div>
                <button onclick="sisgac1.validarCrearAjuste(
                                ['campo1','campo2'],
                                ['texto','texto'],
                                [true,false],
                                'colTipoTramites',
                                {
                                    'tipoTramite': $('#campo1').val().trim(),
                                    'tipoTramiteSigla': $('#campo2').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'consultarTipoTramites'
                                }
                            )" type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar tipo de trámite</button>
                <button onclick="sisgac1.validarActualizarAjuste(
                                ['campo1','campo2'],
                                ['texto','texto'],
                                [true,false],
                                'colTipoTramites',
                                'idAjuste',
                                {
                                    'tipoTramite': $('#campo1').val().trim(),
                                    'tipoTramiteSigla': $('#campo2').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'consultarTipoTramites'
                                }
                            )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar tipo de trámite</button>
        </div>
        
        <div class="modal-footer">
                <table class="table table-hover table-striped table-hover">
                    <thead>
                        <th class="text-center">Tipos de trámites registrados</th>
                        <th class="text-center">Sigla</th>
                        <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
                    </thead>
                    <tbody id="tablaDatosAjustes" class="text-center">
                        <?php if(sizeof($tipoTramites) > 0){ 
                            foreach ($tipoTramites as $clave) { ?>
                            <tr>
                                <td><?php echo $clave['tipoTramite'] ?></td>
                                <td><?php echo $clave['tipoTramiteSigla'] ?></td>
                                <td><button onclick="sisgac1.cargarInfoAjusteForm( '<?php echo $clave['_id'] ?>' , '<?php echo $clave['tipoTramite'] ?>' , '<?php echo $clave['tipoTramiteSigla'] ?>' )" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>
                            </tr>
                        <?php } }else{ ?>
                        <tr class="warning"><td colspan="3">No se encontraron tipos de trámites</td></tr>
                        <?php } ?>

                    </tbody>
                </table>
        </div>
    </div>
</div>