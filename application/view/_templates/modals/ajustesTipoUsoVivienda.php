<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Tipos de uso</h3>
        </div>
            <div class="modal-body modal-fixSisgac1">
                
                <input id="idAjuste" type="hidden">
                
                
            <div onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ sisgac1.validarCrearAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colUsosVivienda',
                                {
                                    'usoVivienda': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoUsoVivienda'
                                }
                            ) } }">
                    <div class="form-group">
                        <label for="campo1">Nombre de nuevo uso</label>
                        <input id="campo1"  placeholder="VIP, comercio, vivienda, industrial, otros... "  class="form-control" type="text">
                    </div>
                </div>
                <button onclick="sisgac1.validarCrearAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colUsosVivienda',
                                {
                                    'usoVivienda': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoUsoVivienda'
                                }
                            )" type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar uso</button>
                    <button onclick="sisgac1.validarActualizarAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colUsosVivienda',
                                'idAjuste',
                                {
                                    'usoVivienda': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoUsoVivienda'
                                }
                            )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar uso</button>
        </div>
        <div class="modal-footer">
            <table class="table table-hover table-striped table-hover">
                    <thead>
                        <th class="text-center">Tipos de usos registrados</th>
                        <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
                    </thead>
                    <tbody id="tablaDatosAjustes" class="text-center">
                        <?php if(sizeof($usosVivienda) > 0){ 
                            foreach ($usosVivienda as $clave) { ?>
                            <tr>
                                <td><?php echo $clave['usoVivienda'] ?></td>
                                <td><button onclick="sisgac1.cargarInfoAjusteForm( '<?php echo $clave['_id'] ?>'  , '<?php echo $clave['usoVivienda'] ?>' )" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>
                            </tr>
                        <?php } }else{ ?>
                        <tr class="warning"><td colspan="3">No se encontraron tipos de usos</td></tr>
                        <?php } ?>

                    </tbody>
                </table>
        </div>
    </div>
</div>