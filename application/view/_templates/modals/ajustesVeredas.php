<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Veredas</h3>
        </div>
        <div class="modal-body modal-fixSisgac1" >
            
            <input id="idAjuste" type="hidden">
            
            <div class="form-group">
                <label for="idDepartamento">Seleccione departamento para asociar</label>
                <select id="idDepartamento" onchange="sisgac1.consultarAjustesMunicipios({'url': 'ajustes/cargarMunicipios','metodo': 'cargarMunicipios'} , 'select' )" class="form-control aplicarSelect2" style="width: 100%">
                    <option value="null">Seleccione departamento</option>
                    <?php foreach ($departamentos as $clave) { ?>
                    <option value="<?php echo $clave['_id'] ?>"><?php echo $clave['codigoDepartamento'] ?> - <?php echo $clave['nombreDepartamento'] ?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="form-group">
                <label for="idMunicipio">Seleccione Municipio para asociar</label>
                <select id="idMunicipio" onchange="sisgac1.consultarAjustesVeredas({'url': 'ajustes/cargarVeredas','metodo': 'cargarVeredas'}, 'tabla')" class="form-control aplicarSelect2" style="width: 100%">
                    <option value="null">Seleccione municipio</option>
                    <?php foreach ($municipios as $clave) { ?>
                    <option value="<?php echo $clave['_id'] ?>"><?php echo $clave['codigoMunicipio'] ?> - <?php echo $clave['nombreMunicipio'] ?></option>
                    <?php } ?>
                </select>
            </div>
            
        <div onkeydown="this.onkeyup = function (evt) {
                if (evt.keyCode == 13) {
                    sisgac1.validarCrearVereda(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            {
                                'municipios': {'veredas': { 'nombreVereda': $('#campo1').val().trim(), 'codigoVereda': $('#campo2').val().trim() }}
                            }
                    )
                }
            }">
            
            <div class="form-group">
                <label for="campo1">Nombre de la vereda</label>
                <input id="campo1" placeholder="Ej: Las Palmas, Manga arriba..." class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="campo2">Código DANE</label>
                <input id="campo2" placeholder="Ej: 05, 10, 30, 230..." class="form-control" type="text">
            </div>
        </div>

        <button onclick="sisgac1.validarCrearVereda(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            {
                                'municipios': {'veredas': { 'nombreVereda': $('#campo1').val().trim(), 'codigoVereda': $('#campo2').val().trim() }}
                            }
                    )" 
                type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar vereda</button>
                
                <button onclick="sisgac1.validarActualizarVereda(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            {
                                'municipios': {'veredas': { 'nombreVereda': $('#campo1').val().trim(), 'codigoVereda': $('#campo2').val().trim() }}
                            }
                    )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar vereda</button>
    </div>
        
        <div class="modal-footer">
        <table class="table table-hover table-striped table-hover">
            <thead>
            <th class="text-center">Veredas registradas</th>
            <th class="text-center">Código DANE</th>
            <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
            </thead>
            <tbody id="tablaDatosAjustes" class="text-center">
                <tr class="warning"><td colspan="3">No se encontraron veredas</td></tr>
            </tbody>
        </table>
    </div>
    </div>
</div>