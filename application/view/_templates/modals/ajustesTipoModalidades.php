<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Tipos de modalidades</h3>
        </div>
        
        <div class="modal-body modal-fixSisgac1">
            
            <input id="idAjuste" type="hidden">
            
            <div class="form-group">
                    <label for="campo1">Nombre de nuevo tipo de modalidad </label>
                    <input id="campo1" onkeydown="this.onkeyup = function(evt){ if(evt.keyCode == 13){ sisgac1.validarCrearAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colModalidades',
                                {
                                    'tipoModalidad': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoModalidades'
                                }
                            ) } }" placeholder="Obra nueva, amplicación, subdivisión..."  class="form-control" type="text">
                </div>
                <button onclick="sisgac1.validarCrearAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colModalidades',
                                {
                                    'tipoModalidad': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoModalidades'
                                }
                            )" type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar modalidad</button>
                            <button onclick="sisgac1.validarActualizarAjuste(
                                ['campo1'],
                                ['texto'],
                                [true],
                                'colModalidades',
                                'idAjuste',
                                {
                                    'tipoModalidad': $('#campo1').val().trim()
                                },
                                {
                                    'url':'ajustes/cargarAjusteEspecifico',
                                    'metodo':'cargarTipoModalidades'
                                }
                            )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar modalidad</button>
        </div>
        
        <div class="modal-footer">
            <table class="table table-hover table-striped table-hover">
                    <thead>
                        <th class="text-center">Tipos de modalidades registradas</th>
                        <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
                    </thead>
                    <tbody id="tablaDatosAjustes" class="text-center">
                        <?php if(sizeof($modalidades) > 0){ 
                            foreach ($modalidades as $clave) { ?>
                            <tr>
                                <td><?php echo $clave['tipoModalidad'] ?></td>
                                <td><button onclick="sisgac1.cargarInfoAjusteForm( '<?php echo $clave['_id'] ?>' , '<?php echo $clave['tipoModalidad'] ?>' )" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>
                            </tr>
                        <?php } }else{ ?>
                        <tr class="warning"><td colspan="3">No se encontraron modalidades</td></tr>
                        <?php } ?>

                    </tbody>
                </table>
        </div>
    </div>
</div>