<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Barrios</h3>
        </div>
        <div class="modal-body modal-fixSisgac1" >
            
            <input id="idAjuste" type="hidden">
            
            <div class="form-group">
                <label for="idDepartamento">Seleccione departamento para asociar</label>
                <select id="idDepartamento" onchange="sisgac1.consultarAjustesMunicipios({'url': 'ajustes/cargarMunicipios','metodo': 'cargarMunicipios'} , 'select' )" class="form-control aplicarSelect2" style="width: 100%">
                    <option value="null">Seleccione departamento</option>
                    <?php foreach ($departamentos as $clave) { ?>
                    <option value="<?php echo $clave['_id'] ?>"><?php echo $clave['codigoDepartamento'] ?> - <?php echo $clave['nombreDepartamento'] ?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="form-group">
                <label for="idMunicipio">Seleccione Municipio para asociar</label>
                <select id="idMunicipio" onchange="sisgac1.consultarAjustesBarrios({'url': 'ajustes/cargarBarrios','metodo': 'cargarBarrios'}, 'tabla')" class="form-control aplicarSelect2" style="width: 100%">
                    <option value="null">Seleccione municipio</option>
                    <?php foreach ($municipios as $clave) { ?>
                    <option value="<?php echo $clave['_id'] ?>"><?php echo $clave['codigoMunicipio'] ?> - <?php echo $clave['nombreMunicipio'] ?></option>
                    <?php } ?>
                </select>
            </div>
            
        <div onkeydown="this.onkeyup = function (evt) {
                if (evt.keyCode == 13) {
                    sisgac1.validarCrearBarrio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'municipios': {'barrios': { 'nombreBarrio': $('#campo1').val().trim(), 'codigoBarrio': parseInt($('#campo2').val().trim()) }}
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )
                }
            }">
            
            <div class="form-group">
                <label for="campo1">Nombre del barrio</label>
                <input id="campo1" placeholder="La playa, Zona centro, Montecarlos..." class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="campo2">Código</label>
                <input id="campo2" placeholder="Código Ej: 05,27..." class="form-control" type="text">
            </div>
        </div>

        <button onclick="sisgac1.validarCrearBarrio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'municipios': {'barrios': { 'nombreBarrio': $('#campo1').val().trim(), 'codigoBarrio': parseInt($('#campo2').val().trim()) }}
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )" 
                type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar barrio</button>
                
                <button onclick="sisgac1.validarActualizarBarrio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            'idAjuste',
                            {
                                'municipios': {'barrios': { 'nombreBarrio': $('#campo1').val().trim(), 'codigoBarrio': parseInt($('#campo2').val().trim()) }}
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar barrio</button>
    </div>
        
        <div class="modal-footer">
        <table class="table table-hover table-striped table-hover">
            <thead>
            <th class="text-center">Barrios registrados</th>
            <th class="text-center">Código</th>
            <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
            </thead>
            <tbody id="tablaDatosAjustes" class="text-center">
                <tr class="warning"><td colspan="3">No se encontraron barrios</td></tr>
            </tbody>
        </table>
    </div>
    </div>
</div>