<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Tipo de documentos de identificación personal</h3>
        </div>

        <div class="modal-body modal-fixSisgac1" >
            
            <input id="idAjuste" type="hidden">
            
        <div onkeydown="this.onkeyup = function (evt) {
                if (evt.keyCode == 13) {
                    sisgac1.validarCrearAjuste(
                            ['campo1', 'campo2'],
                            ['texto', 'texto'],
                            [true, true],
                            'colDocIdentificacion',
                            {
                                'tipoDocumentoId': $('#campo1').val().trim(),
                                'tipoDocumentoSigla': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'consultarIdentificaciones'
                            }
                    )
                }
            }">
            
            <div class="form-group">
                <label for="campo1">Nuevo tipo de documento de identificación</label>
                <input id="campo1" placeholder="Cédula, Pasaporte, NIT..." class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="campo2">Sigla</label>
                <input id="campo2" placeholder="C.C, P.P, T.I, NIT..." class="form-control" type="text">
            </div>
        </div>

        <button onclick="sisgac1.validarCrearAjuste(
                            ['campo1', 'campo2'],
                            ['texto', 'texto'],
                            [true, true],
                            'colDocIdentificacion',
                            {
                                'tipoDocumentoId': $('#campo1').val().trim(),
                                'tipoDocumentoSigla': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'consultarIdentificaciones'
                            }
                    )" 
                type="button" class="btn btn-primary col-lg-6 col-md-6 col-sm-6 col-xs-12">Registrar tipo de identificación personal</button>
                <button onclick="sisgac1.validarActualizarAjuste(
                            ['campo1', 'campo2'],
                            ['texto', 'texto'],
                            [true, true],
                            'colDocIdentificacion',
                            'idAjuste',
                            {
                                'tipoDocumentoId': $('#campo1').val().trim(),
                                'tipoDocumentoSigla': $('#campo2').val().trim()
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'consultarIdentificaciones'
                            }
                    )" type="button" class="btn btn-danger col-lg-6 col-md-6 col-sm-6 col-xs-12">Actualizar tipo de identificación</button>
    </div>
        
        <div class="modal-footer">
        <table class="table table-hover table-striped table-hover">
            <thead>
            <th class="text-center">Tipos de trámites registrados</th>
            <th class="text-center">Sigla</th>
            <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
            </thead>
            <tbody id="tablaDatosAjustes" class="text-center">
                <?php
                if (sizeof($identificaciones) > 0) {
                    foreach ($identificaciones as $clave) {
                        ?>
                        <tr>
                            <td><?php echo $clave['tipoDocumentoId'] ?></td>
                            <td><?php echo $clave['tipoDocumentoSigla'] ?></td>
                            <td><button onclick="sisgac1.cargarInfoAjusteForm( '<?php echo $clave['_id'] ?>' , '<?php echo $clave['tipoDocumentoId'] ?>' , '<?php echo $clave['tipoDocumentoSigla'] ?>' )" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>
                        </tr>
                    <?php }
                } else {
                    ?>
                    <tr class="warning"><td colspan="3">No se encontraron tipos de documentos</td></tr>
<?php } ?>

            </tbody>
        </table>
    </div>
    </div>
</div>