<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="modal-title">Municipios</h3>
        </div>
        <div class="modal-body modal-fixSisgac1" >
            
            <input id="idAjuste" type="hidden">
            
            <div class="form-group">
                <label for="idDepartamento">Seleccione departamento para asociar</label>
                <select id="idDepartamento" onchange="sisgac1.consultarAjustesMunicipios({'url': 'ajustes/cargarMunicipios','metodo': 'cargarMunicipios'} , 'tabla')" class="form-control aplicarSelect2" style="width: 100%">
                    <option value="null">Seleccione departamento</option>
                    <?php foreach ($departamentos as $clave) { ?>
                    <option value="<?php echo $clave['_id'] ?>"><?php echo $clave['codigoDepartamento'] ?> - <?php echo $clave['nombreDepartamento'] ?></option>
                    <?php } ?>
                </select>
            </div>
            
        <div onkeydown="this.onkeyup = function (evt) {
                if (evt.keyCode == 13) {
                    sisgac1.validarCrearMunicipio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'municipios': { 'nombreMunicipio': $('#campo1').val().trim(), 'codigoMunicipio': $('#campo2').val().trim() }
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )
                }
            }">
            
            <div class="form-group">
                <label for="campo1">Nombre del municipio</label>
                <input id="campo1" placeholder="Envigado, Abejorral, Girardota, Yolombó..." class="form-control" type="text">
            </div>
            <div class="form-group">
                <label for="campo2">Código DANE (Número con ceros incluidos)</label>
                <input id="campo2" placeholder="Código según el DANE Ej: 05,27..." class="form-control" type="text">
            </div>
        </div>

        <button onclick="sisgac1.validarCrearMunicipio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            {
                                'municipios': { 'nombreMunicipio': $('#campo1').val().trim(), 'codigoMunicipio': $('#campo2').val().trim() }
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )" 
                type="button" class="btn btn-primary col-lg-6 col-xs-6">Registrar municipio</button>
                <button onclick="sisgac1.validarActualizarMunicipio(
                            ['campo1', 'campo2'],
                            ['texto', 'num'],
                            [true, true],
                            'colDepartamentos',
                            'idAjuste',
                            {
                                'municipios': { 'nombreMunicipio': $('#campo1').val().trim(), 'codigoMunicipio': $('#campo2').val().trim() }
                            },
                            {
                                'url': 'ajustes/cargarAjusteEspecifico',
                                'metodo': 'cargarMunicipios'
                            }
                    )" type="button" class="btn btn-danger col-lg-6 col-xs-6">Actualizar municipio</button>
    </div>
        
        <div class="modal-footer">
        <table class="table table-hover table-striped table-hover">
            <thead>
            <th class="text-center">Municipios registrados</th>
            <th class="text-center">Código DANE</th>
            <th class="text-center"><i class="glyphicon glyphicon-wrench"></i></th>
            </thead>
            <tbody id="tablaDatosAjustes" class="text-center">
                <tr class="warning"><td colspan="3">No se encontraron municipios</td></tr>
            </tbody>
        </table>
    </div>
    </div>
</div>