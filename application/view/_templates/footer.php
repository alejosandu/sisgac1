

    <footer style="position: relative; z-index: 1;">
                <div class="container" >
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="widget">
                                <h4>Encuentranos</h4>
                                <address>
                                    <strong>Curaduría Urbana Primera de Envigado</strong><br>
                                    Edificio Casticentro Piso 3<br>
                                    Cra. 42 No. 35 Sur - 55<br>
                                </address>
                                <p>
                                    <i class="glyphicon glyphicon-phone-alt"></i> PBX: 322 40 80 <br>
                                    <i class="fa fa-phone"></i> Celular: 3206827979  <br>
                                    <i class="fa fa-globe"></i> Web: <a href="http://www.curaduria1env.com.co" target="blank">www.curaduria1env.com.co</a><br>
                                    <i class="glyphicon glyphicon-envelope"></i> Email: <a href="mailto:curador@curaduria1env.com.co">curador@curaduria1env.com.co</a> 
                                     
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="widget">
                                <h4>Enlaces relacionados</h4>
                                <ul class="list-group">
                                    <li><a href="http://curadoresurbanos.org/colegio" target="blank">Colegio Nacional de Curadores Urbanos</a></li>
                                    <li><a href="http://www.sociedadcolombianadearquitectos.org/site" target="blank">Sociedad Colombiana de Arquitectos</a></li>
                                    <li><a href="http://www.camacolantioquia.org.co/" target="blank">Cámara Colombiana de la Construcción Regional Antioquia</a></li>
                                    <li><a href="http://copnia.gov.co" target="blank">Consejo Profesional Nacional de Ingeniería</a></li>
                                    <li><a href="http://www.dane.gov.co" target="blank">DANE</a></li>
                                    <li><a href="http://www.envigado.gov.co" target="blank">Alcaldía de Envigado</a></li>
                                    <li><a href="http://www.minvivienda.gov.co" target="blank">Ministerio de Vivienda</a></li>
                                    <li><a href="http://www.corantioquia.gov.co" target="blank">Corantioquia</a></li>
                                </ul>
                            </div>
                            
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="widget">
                                <h4>Otras aplicaciones</h4>
                                <ul class="list-group">
                                    <li><a href="http://srvc1envi/PortalC1/" target="blank">Cur@pp</a></li>
                                    <li><a href="https://play.google.com/store/apps/details?id=co.extein.curaduriasUrbanas" target="blank">Appplicación en android</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="copyright">
                                    <p>
                                        <span>&copy; Sailor 2015 All right reserved. | Designed by </span><a href="http://bootstraptaste.com" target="_blank">BootstrapTaste</a> | Versión PHP: <?php echo PHP_VERSION ?> | MongoDB: 3.2.3
                                        <!-- 
                                           All links in the footer should remain intact. 
                                           Licenseing information is available at: http://bootstraptaste.com/license/
                                           You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Sailor
                                        -->

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

</div>


<div class="modal fade" id="modalAjustes" role="dialog">
        
</div>




    <!-- define the project's URL (to make AJAX calls possible, even when using this in sub-folders etc) -->
    <script>
        var urlPHP = "<?php echo URL; ?>";
    </script>
    
    <a href="#" class="scrollup" style="position: fixed; z-index: 2;"><i class="fa fa-angle-up active"></i></a>

<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo URL ?>js/jquery.min.js"></script>
<script src="<?php echo URL ?>js/alertify.min.js"></script>
<script src="<?php echo URL ?>js/modernizr.custom.js"></script>
<script src="<?php echo URL ?>js/jquery.easing.1.3.js"></script>
<script src="<?php echo URL ?>js/bootstrap.min.js"></script>
<script src="<?php echo URL ?>plugins/flexslider/jquery.flexslider-min.js"></script> 
<script src="<?php echo URL ?>plugins/flexslider/flexslider.config.js"></script>
<script src="<?php echo URL ?>js/jquery.appear.js"></script>
<script src="<?php echo URL ?>js/stellar.js"></script>
<script src="<?php echo URL ?>js/classie.js"></script>
<script src="<?php echo URL ?>js/uisearch.js"></script>
<script src="<?php echo URL ?>js/jquery.cubeportfolio.min.js"></script>
<script src="<?php echo URL ?>js/google-code-prettify/prettify.js"></script>
<script src="<?php echo URL ?>js/animate.js"></script>
<script src="<?php echo URL ?>js/custom.js"></script>
<script src="<?php echo URL ?>js/select2.full.js"></script>
<!--traducción para select2 al español-->
<script src="<?php echo URL ?>js/i18n/es.js"></script>
<script src="<?php echo URL ?>js/sisgac1.js"></script>

    
    
</body>
</html>
