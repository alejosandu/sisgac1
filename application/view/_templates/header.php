<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo strtoupper($_SESSION["nombreUsuario"]) ?>@SISGAC - <?php echo $seccion ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sistema de Gestión de Archivos de la Curaduría Urbana Primera de Envigado" />
        <meta name="author" content="Alejandro Sánchez Durán - Practicante del SENA - Análisis y Desarrollo de Sistemas de Información - Ficha 751130" />
        <!-- css -->
        <link href="<?php echo URL ?>css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/alertify.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/themes/default.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/themes/semantic.css" rel="stylesheet" />

        <link href="<?php echo URL ?>plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
        <link href="<?php echo URL ?>css/cubeportfolio.min.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/style.css" rel="stylesheet" />
        
        <link href="<?php echo URL ?>css/select2.css" rel="stylesheet" />

        <!--SISGAC1 STYLESHEET CUSTOM-->
        <link href="<?php echo URL ?>css/sisgac1.css" rel="stylesheet" />

        <!-- Theme skin -->
        <link id="t-colors" href="<?php echo URL; ?>skins/green.css" rel="stylesheet" />
        <!-- boxed bg -->
        <link id="bodybg" href="<?php echo URL; ?>bodybg/bg1.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="<?php echo URL ?>img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo URL ?>img/favicon.ico" type="image/x-icon">

    </head>

    <div id="wrapper">

        <header>
            <div class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="col-sm-12">
                            <div class="col-lg-12 col-md-12 col-lg-offset-0 col-md-offset-0 col-sm-offset-12">
                                <a class="navbar-brand" href="<?php echo URL ?>tramites/listar" ><img src="<?php echo URL ?>img/logo.png" alt="" width="199" height="65" style="position: relative; top: -12px" /></a>
                            </div>
                        </div>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul id="menu" class="nav navbar-nav">
                            <li ><a href="<?php echo URL ?>tramites/listar">Inicio</a></li>
                                <?php foreach ($this->menuInController['modulos'] as $key => $value) {
                                    foreach ($value as $nombreModulo => $submodulo) { ?>
                                    <?php if(is_array($submodulo)){ ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" ><?php echo $nombreModulo; ?> <i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($submodulo as $nombreSubmodulo => $enlace) {
                                                    foreach ($enlace as $titulo => $url) { ?>
                                                <li><a data-script="<?php echo $url ?>" href="<?php echo URL . $url ?>" ><?php echo $titulo; ?></a></li>
                                            <?php   }
                                                  } ?>
                                        </ul>
                                        </li>
                                <?php }else{ ?>
                                        <li><a href="<?php echo URL . $submodulo ?>"><?php echo $nombreModulo; ?></a></li>
                                <?php }
                                    }
                                } ?>
                                    
                                    <li ><a href="<?php echo URL ?>login/cerrar"><i class="glyphicon glyphicon-off"></i> Cerrar sesión</a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </header>