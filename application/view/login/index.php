<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo $seccion ?>@SISGAC - Curaduría Urbana Primera de Envigado</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Sistema de Gestión de Archivos de la Curaduría Urbana Primera de Envigado" />
        <meta name="author" content="Alejandro Sánchez Durán - Practicante del SENA - Análisis y Desarrollo de Sistemas de Información - Ficha 751130" />
        <!-- css -->
        <link href="<?php echo URL ?>css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/alertify.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/themes/default.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/themes/semantic.css" rel="stylesheet" />
        
        <link href="<?php echo URL ?>plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />
        <link href="<?php echo URL ?>css/cubeportfolio.min.css" rel="stylesheet" />
        <link href="<?php echo URL ?>css/style.css" rel="stylesheet" />
        
        <!--SISGAC1 STYLESHEET CUSTOM-->
        <link href="<?php echo URL ?>css/sisgac1.css" rel="stylesheet" />
        
        <!-- Theme skin -->
        <link id="t-colors" href="<?php echo URL; ?>skins/green.css" rel="stylesheet" />
        <!-- boxed bg -->
        <link id="bodybg" href="<?php echo URL; ?>bodybg/bg1.css" rel="stylesheet" type="text/css" />
        
        <link rel="shortcut icon" href="<?php echo URL ?>img/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo URL ?>img/favicon.ico" type="image/x-icon">

    </head>
    <body>

        <div id="wrapper">
            <!-- start header -->
            <header>			
                <div class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="<?php echo URL ?>"><img src="img/logo.png" alt="" width="199" height="65" style="position: relative; top: -12px" /></a>
                        </div>

                    </div>
                </div>
            </header>
            <!-- end header -->

            <section id="content">
                <div class="container">
                    <div class="hidden-sm hidden-xs" style="-webkit-filter: blur(3px); position: absolute; z-index: 0; top: 0px; left: 0px; width: 100%; height: 100%; background-image: url('<?php echo URL ?>img/archivoCuraduria.jpg'); background-size: 100% auto; background-position: 0px o0px; background-repeat: no-repeat;"></div>
                    <div class="row" style="">
                        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                            <form role="form" method="post" action="" onkeydown="this.onkeypress = function(evt){ if(evt.keyCode == 13){ sisgac1.iniciarSesion() } }" class="register-form" style="padding: 1px 15px 15px 15px; background-color: rgba(255,255,255,1); box-shadow: rgba(0,0,0,0.5) 0px 0px 15px 3px">
                                <h2>Login <small>Ingresar al sistema</small></h2>
                                <hr class="colorgraph">

                                <div class="form-group">
                                    <input autofocus="autofocus" type="text" name="nombreUsuario" id="nombreUsuario" class="form-control input-lg" placeholder="Nombre de usuario">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pass" id="pass" class="form-control input-lg"  placeholder="Contraseña">
                                </div>

                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6"><input type="button" name="btnEntrar" value="Ingresar" onclick="sisgac1.iniciarSesion()" class="btn btn-primary btn-block btn-lg"></div>
                                    <div class="col-xs-12 col-md-6" style="padding-top: 10px">¿Olvidó su contraseña? <a href="javascript: $('#modRecuperarPWD').modal({show: true})" class="text-primary">Recupérala!</a></div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </section>

            <footer style="position: relative; z-index: 1;">
                <div class="container" >
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="widget">
                                <h4>Encuentranos</h4>
                                <address>
                                    <strong>Curaduría Urbana Primera de Envigado</strong><br>
                                    Edificio Casticentro Piso 3<br>
                                    Cra. 42 No. 35 Sur - 55<br>
                                </address>
                                <p>
                                    <i class="glyphicon glyphicon-phone-alt"></i> PBX: 322 40 80 <br>
                                    <i class="fa fa-phone"></i> Celular: 3206827979  <br>
                                    <i class="fa fa-globe"></i> Web: <a href="http://www.curaduria1env.com.co" target="blank">www.curaduria1env.com.co</a> <br>
                                    <i class="glyphicon glyphicon-envelope"></i> Email: <a href="mailto:curador@curaduria1env.com.co">curador@curaduria1env.com.co</a> 
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="widget">
                                <h4>Enlaces relacionados</h4>
                                <ul class="list-group">
                                    <li><a href="http://curadoresurbanos.org/colegio" target="blank">Colegio Nacional de Curadores Urbanos</a></li>
                                    <li><a href="http://www.sociedadcolombianadearquitectos.org/site" target="blank">Sociedad Colombiana de Arquitectos</a></li>
                                    <li><a href="http://www.camacolantioquia.org.co/" target="blank">Cámara Colombiana de la Construcción Regional Antioquia</a></li>
                                    <li><a href="http://copnia.gov.co" target="blank">Consejo Profesional Nacional de Ingeniería</a></li>
                                    <li><a href="http://www.dane.gov.co" target="blank">DANE</a></li>
                                    <li><a href="http://www.envigado.gov.co" target="blank">Alcaldía de Envigado</a></li>
                                    <li><a href="http://www.minvivienda.gov.co" target="blank">Ministerio de Vivienda</a></li>
                                    <li><a href="http://www.corantioquia.gov.co" target="blank">Corantioquia</a></li>
                                </ul>
                            </div>
                            <div class="widget">
                                <h4>Otras aplicaciones</h4>
                                <ul class="list-group">
                                    <li><a href="http://srvc1envi/PortalC1/" target="blank">Cur@pp</a></li>
                                    <li><a href="https://play.google.com/store/apps/details?id=co.extein.curaduriasUrbanas" target="blank">Appplicación en android</a></li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <iframe class="col-lg-6 col-md-6 col-sm-12" height="380" frameborder="0" style="border:0;" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJhXB_-02CRo4RPZgYiwAJrKs&key=AIzaSyBzAlMeTeooHxbwQ1Yug6WgvWt7c7ljWLU" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div id="sub-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="copyright">
                                    <p>
                                        <span>&copy; Sailor 2015 All right reserved. | Designed by </span><a href="http://bootstraptaste.com" target="_blank">BootstrapTaste</a>
                                        <!-- 
                                           All links in the footer should remain intact. 
                                           Licenseing information is available at: http://bootstraptaste.com/license/
                                           You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Sailor
                                        -->

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>

        <!--        MODAL RECUPERAR PASS-->
        <div class="modal fade" id="modRecuperarPWD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Recuperar contraseña</h4>
                    </div>
                    <div class="modal-body">
                        <form action="action" method="post">
                            <div class="form-group">
                                <input id="nombreUsuarioRecuperar" type="text" name="nombreUsuarioRecuperar"  class="form-control input-lg" placeholder="Nombre de usuario">
                            </div>
                            <div class="form-group">
                                <input id="emailRecuperar" type="email" name="emailRecuperar" class="form-control input-lg" placeholder="Correo electrónico">
                            </div>
                            <hr class="colorgraph">
                        </form>
                        <p>
                            Se enviará una nueva contraseña a su correo electrónico, 
                            deberá ingresar de nuevo con este código como contraseña.
                            <strong>Luego de ingresar con su nueva contraseña se recomienda que la cambie.</strong>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Calcelar</button>
                        <button type="button" class="btn btn-primary" onclick="sisgac1.recuperarCuenta()">Enviar código</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var urlPHP = "<?php echo URL; ?>";
        </script>
        <!-- javascript
            ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo URL; ?>js/jquery.min.js"></script>
        <script src="<?php echo URL; ?>js/alertify.min.js"></script>
        <script src="<?php echo URL; ?>js/bootstrap.min.js"></script>
        <script src="<?php echo URL ?>js/select2.full.js"></script>
        <script src="<?php echo URL; ?>js/sisgac1.js"></script>


    </body>
</html>