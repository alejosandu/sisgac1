
<div id="wrapper">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo URL ?>home"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
                        <li class="active">404 - Página no encontrada</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <div class="jumbotron">
                            <h1>404</h1>
                            <p>La página a la que intenta acceder no está disponible o no existe.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

