//función para validar datos de cualquier campo que se le asigne
function validarCampo(input, tipo, obligatorio) {
    var correo = /^[a-z0-9\.\_\-]+\@[a-z0-9]+\.(([a-z0-9]+)|([a-z0-9]+\.[a-z]+)|([a-z0-9]+\.[a-z]+\.[a-z]+))$/i;
    var letras = /^[a-z\,\.\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00D1\u00F1\s\']*$/i;
    var numeros = /^[0-9\+\-\(\)]*$/;
    var documentos = /^[0-9\-\.]*$/;
    var free = /^[a-z0-9 \u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00D1\u00F1\s \.\_\-\#\/\\\']*$/i;
    var url = /^[a-z0-9\.\-\_\/\=\?\:]*$/i;
    var alfanum = /^[a-z0-9\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00D1\u00F1\s]*$/i;

    var resultado = false;

    var validez = [];

    for (var i = 0; i < input.length; i++) {

        if (obligatorio[i] == true || obligatorio[i] === true || obligatorio[i] == "true") {
            if (document.getElementById(input[i]).value == "") {
                document.getElementById(input[i]).setAttribute("style", "border-color: #d9232d");
                validez.push(false);
                continue;
            }
        }

        if (tipo[i] == "email") {
            if(obligatorio[i] == true || obligatorio[i] === true || obligatorio[i] == "true"){
                resultado = correo.test(document.getElementById(input[i]).value);
            }else if(document.getElementById(input[i]).value.trim() != ""){
                resultado = correo.test(document.getElementById(input[i]).value);
            }
        } else if (tipo[i] == "texto") {
            resultado = letras.test(document.getElementById(input[i]).value);
        } else if (tipo[i] == "num") {
            resultado = numeros.test(document.getElementById(input[i]).value);
        } else if (tipo[i] == "alfanum") {
            resultado = alfanum.test(document.getElementById(input[i]).value);
        } else if (tipo[i] == "doc") {
            resultado = documentos.test(document.getElementById(input[i]).value);
        } else if (tipo[i] == "libre") {
            resultado = free.test(document.getElementById(input[i]).value);
        } else if (tipo[i] == "url") {
            resultado = url.test(document.getElementById(input[i]).value);
        }

        if (RegExp("^[\u0020\]+$", "g").test(document.getElementById(input[i]).value)) {
            validez.push(false);
        }

        if (resultado) {
            document.getElementById(input[i]).setAttribute("style", "");
        } else {
            document.getElementById(input[i]).setAttribute("style", "border-color: #d9232d");
        }

        validez.push(resultado);

    }
    for (var j = 0; j < validez.length; j++) {
        if (validez[j] == false) {
            return false;
        }
    }
    
    return true;

}
//función para limpiar los campos después de hacer algún registo o consultar sin formularios
function limpiarCampos(input){
    for(var i = 0; i < input.length; i++){
        document.getElementById(input[i]).value = "";
    }
}
//función para validar los select cuando los option no se seleccionan
function validarSelect(input, comparar) {
    if (document.getElementById(input).value.trim() == comparar) {
        document.getElementById(input).setAttribute("style", "border-color: #d9232d");
        alertify.warning("Por favor " + document.getElementById(input)[0].text);
        return false;
    }
    document.getElementById(input).setAttribute("style", "");
    return true;
}

function validarSelectMultiple(input, comparar){
    var cont = 0;
    var options = document.getElementById(input).options;
    for (var i=0; i < options.length; i++) {
        if (options[i].selected) {
            if (options[i].value.trim() == comparar) {
                document.getElementById(input).setAttribute("style", "border-color: #d9232d");
                alertify.warning("Por favor " + document.getElementById(input)[0].text);
                return false;
            };
            cont++;
        };
        
    }
    if(cont == 0 || cont == -1){
        document.getElementById(input).setAttribute("style", "border-color: #d9232d");
        alertify.warning("Por favor " + document.getElementById(input)[0].text);
        return false;
    }
    
    document.getElementById(input).setAttribute("style", "");
    return true;
}
//función para alertar sobre un error inesperado en el sistema
function errorInesperado(code, msg) {
    alertify.error("Ha ocurrido un error inesperado al actualizar los datos.",0);
    console.error("Ha ocurrido un error inesperado al actualizar los datos.\nCode:" + code + "\nMsg:" + msg);
}

//funcines para cerrar la sesión por inactividad
var timeOutCerrarSesionAuto = null;
var timeOutIniciarContadorCerrarSesionAuto = null;

function iniciarContadorCerrarSesionAuto(){
    timeOutIniciarContadorCerrarSesionAuto = setTimeout(function () {
        if (location.href != (urlPHP + "login"))
        {
            var titulo = document.title;
            document.title = "( ! ) " + document.title;
            alertify.warning(
                    "En 5 minutos se cerrará la sesión, por favor <strong>recargue la página o haga clic sobre esta notificación</strong> para generar actividad en el sistema o de lo contrario se cerrará automáticamente la sesión.", 
                    0, 
                    function(){ 
                        document.title = titulo;
                        clearTimeout(timeOutCerrarSesionAuto); 
                        clearTimeout(timeOutIniciarContadorCerrarSesionAuto);
                        iniciarContadorCerrarSesionAuto();
                        cerrarSesionAuto();
                    });
        }
    }, (15 * 60000))
}

function cerrarSesionAuto(){
    timeOutCerrarSesionAuto = setTimeout(
        function () {
            if (location.href != (urlPHP + "login"))
            {
                location.href = urlPHP + "login/cerrar";
            }
        }, (20 * 60000));
}

//función para reemplazar las url de los ajustes, para llamar funciones los modales mediante javascript
$(document).ready(function(){
    iniciarContadorCerrarSesionAuto();
    cerrarSesionAuto();
    
    $("a[data-script]").each(function(){
        var a = $(this).attr("data-script");
        if(/javascript:/i.test(a)){
            this.setAttribute("href",$(this).attr("data-script"));
        }
    });
});

$(".aplicarSelect2").select2({placeholder:" Seleccione"});
$("#veredaUrb").select2({ tags: true ,placeholder:" (Opcional)" });
$("#barrio").trigger("change");

var sisgac1 = {
    iniciarSesion: function () {
        $.ajax({
            url: urlPHP + "login/entrar",
            type: "post",
            data: {
                nombreUsuario: document.getElementById("nombreUsuario").value.trim(),
                pass: document.getElementById("pass").value.trim()
            },
            success: function (res) {
                if (res.res === true) {
                    location.href = urlPHP + "tramites/listar";
                } else if (res.res === false) {
                    alertify.error("Error al iniciar sesión.\nVerifique los datos.");
                } else if (res.res == "sinAcceso") {
                    alertify.error("Su cuenta ha sido inhabilitada.\nPor favor contáctese con soporte técnico.");
                }
            }
        });
    },
    validarCrearNuevoUsuario: function () {
        var campos = [
            "nombreUsuario",
            "email",
            "nombre",
            "apellido",
            "tipoUsuario"
        ];

        var tipoDatos = [
            "alfanum",
            "email",
            "texto",
            "texto",
            "libre"
        ];

        var obligatorios = [
            true,
            true,
            true,
            true,
            true
        ];

        if (!validarCampo(campos, tipoDatos, obligatorios)) {
            alertify.warning("Complete todos los campos obligatorios o verifique los tipos de datos ingresados.", 10);
            return;
        }

        if (!validarSelect("tipoUsuario", "null")) {
            return;
        }
        
        try{
            if (!validarSelect("curaduria", "null")) {
            return;
        }
        }catch(e){}
        
        if(document.getElementById("nombreUsuario").value.trim().length == 24){
            alertify.warning("El nombre de usuario no puede tener 24 caracteres exactos");
            return;
        }

        $.ajax({
            url: urlPHP + "usuarios/crearNuevo",
            method: "post",
            data: {
                btnCrearNuevoUsuario: true,
                nombreUsuario: document.getElementById("nombreUsuario").value.trim(),
                email: document.getElementById("email").value.trim(),
                nombre: document.getElementById("nombre").value.trim(),
                apellido: document.getElementById("apellido").value.trim(),
                tipoUsuarioTexto: $('#tipoUsuario option:selected').text().trim(),
                tipoUsuario: document.getElementById("tipoUsuario").value.trim(),
                curaduria: document.getElementById("curaduria").value.trim()
            },
            beforeSend: function(){
                alertify.warning("Registrando información del usuario",0);
            },
            success: function (res) {
                alertify.dismissAll();
                if (res.usuarioExiste === true) {
                    alertify.warning("El nombre de usuario " + document.getElementById("nombreUsuario").value.trim() + " ya está siendo utilizado.", 0)
                } else if (res.emailExiste == true) {
                    alertify.warning("El e-mail " + document.getElementById("email").value.trim() + " ya está siendo utilizado.", 0);
                } else if (res.n === 1 && (res.err == null && res.errmsg == null)) {
                    alertify.success("El usuario \"" + document.getElementById("nombreUsuario").value.trim() + "\" fue registrado con éxito.", 0);
                    document.getElementById("formCrearNuevoUsuario").reset();
                } else if ((res.err !== null || res.errmsg !== null) || res == "") {
                    errorInesperado(res.err, res.errmsg);
                }
            }
        });
    },
    validarNombreUsuarioLibre: function (parametro) {
        $.ajax({
            url: urlPHP + "usuarios/validarNombreUsuarioLibre",
            method: "post",
            data: {
                param: parametro,
                nombreUsuario: document.getElementById("nombreUsuario").value.trim()
            },
            success: function (res) {
                if (res > 0) {
                    document.getElementById("nombreUsuario").setAttribute("style", "border-color: #d9232d");
                    alertify.warning("El nombre de usuario " + document.getElementById("nombreUsuario").value.trim() + " ya está siendo utilizado.", 10);
                    return false;
                } else {
                    document.getElementById("nombreUsuario").setAttribute("style", "border-color: green");
                    return true;
                }
            }
        });
    },
    validarEmailLibre: function (parametro) {
        $.ajax({
            url: urlPHP + "usuarios/validarEmailLibre",
            method: "post",
            data: {
                param: parametro,
                email: document.getElementById("email").value.trim()
            },
            success: function (res) {
                if (res > 0) {
                    document.getElementById("email").setAttribute("style", "border-color: #d9232d");
                    alertify.warning("El e-mail " + document.getElementById("email").value.trim() + " ya está siendo utilizado.", 10);
                } else {
                    document.getElementById("email").setAttribute("style", "border-color: green");
                }
            }
        });
    },
    validarActualizarUsuario: function () {
        var campos = [
            "nombreUsuario",
            "email",
            "nombre",
            "apellido"
        ];

        var tipoDatos = [
            "alfanum",
            "email",
            "texto",
            "texto"
        ];

        var obligatorios = [
            true,
            true,
            true,
            true
        ];

        if (!validarCampo(campos, tipoDatos, obligatorios)) {
            alertify.warning("Complete todos los campos obligatorios o verifique los tipos de datos ingresados.", 10);
            return;
        }
        
        if(document.getElementById("nombreUsuario").value.trim().length == 24){
            alertify.warning("El nombre de usuario no puede tener 24 caracteres exactos");
            return;
        }

        $.ajax({
            url: urlPHP + "usuarios/actualizarUsuario",
            method: "post",
            data: {
                btnActualizarUsuario: true,
                nombreUsuario: document.getElementById("nombreUsuario").value.trim(),
                email: document.getElementById("email").value.trim(),
                nombre: document.getElementById("nombre").value.trim(),
                apellido: document.getElementById("apellido").value.trim()
            },
            success: function (res) {
                if (res.usuarioExiste == true) {
                    alertify.warning("El nombre de usuario ya está siendo usado.")
                } else if (res.emailExiste == true) {
                    alertify.warning("El e-mail " + document.getElementById("email").value.trim() + " ya está siendo utilizado.", 10);
                } else if (res.n == 1 && (res.err == null && res.errmsg == null)) {
                    alertify.success("Datos actualizados con éxito.");
                } else if ((res.err !== null || res.errmsg) || res == "") {
                    errorInesperado(res.err, res.errmsg);
                }
            }
        });

    },
    validarCambiarPass: function () {
        var campos = [
            "oldPass",
            "newPass",
            "newPassReapeat"
        ];
        var tipoDatos = [
            "alfanum",
            "alfanum",
            "alfanum"
        ];
        var obligatorios = [
            true,
            true,
            true
        ];

        if (!validarCampo(campos, tipoDatos, obligatorios)) {
            alertify.warning("Por favor llene todos los campos obligatorios.");
            return;
        }

        if (document.getElementById("newPass").value.trim() != document.getElementById("newPassReapeat").value.trim()) {
            alertify.warning("La nueva contraseña y la repetición de la nueva contraseña deben coincidir.");
            return;
        }

        if (document.getElementById("newPass").value.trim().length < 7) {
            alertify.warning("La nueva contraseña debe contener al menos 8 caracteres.");
            return;
        }

        $.ajax({
            url: urlPHP + "usuarios/cambiarPass",
            method: "post",
            data: {
                btnActualizarPass: true,
                oldPass: document.getElementById("oldPass").value.trim(),
                newPass: document.getElementById("newPass").value.trim(),
                newPassReapeat: document.getElementById("newPassReapeat").value.trim()
            },
            success: function (res) {
                if (res.n == 1 && (res.err == null && res.errmsg == null)) {
                    alertify.success("Contraseña cambiada con éxito.");
                    document.getElementById("formActualizarPass").reset();
                } else if (res.res == "malPassActual") {
                    alertify.warning("La contraseña actual no coincide con el sistema.");
                } else if (res.res == "noCoinciden") {
                    alertify.warning("La nueva contraseña y la repetición de la nueva contraseña deben coincidir.");
                } else if ((res.err !== null || res.errmsg) || res == "") {
                    errorInesperado(res.err, res.errmsg);
                }
            }
        });
    },
    cambiarEstadoUsuario: function (idUsuario, nuevoEstado, nombreUsuario) {
        $.ajax({
            url: urlPHP + "usuarios/cambiarEstadoUsuario",
            method: "post",
            data: {
                idUsuario: idUsuario,
                nombreUsuario: nombreUsuario,
                estado: nuevoEstado
            },
            success: function (res) {
                if (res.n == 1) {
                    alertify.success("El usuario ha cambiado de estado exitosamente.");
                } else if ((res.err !== null || res.errmsg) || res == "") {
                    errorInesperado(res.err, res.errmsg);
                }
            }
        });
    },
    consultarDatosUsuarioListar: function (idUsuario) {
        alertify.message("Consultando información del usuario...", 0);
        $.ajax({
            url: urlPHP + "usuarios/consultarDatosUsuario",
            method: "post",
            data: {
                idUsuario: idUsuario
            },
            success: function (res) {
                if (res != null) {
                    document.getElementById("modalTituloEditarUsuario").innerHTML = "Datos de <strong class='text-primary'>" + res.nombreUsuario.toUpperCase() + "</strong>";
                    document.getElementById("idUsuario").value = res._id['$id'];
                    document.getElementById("nombreUsuario").value = res.nombreUsuario;
                    document.getElementById("email").value = res.email;
                    document.getElementById("nombre").value = res.nombre;
                    document.getElementById("apellido").value = res.apellido;
                    document.getElementById("tipoUsuario").value = res.tipoUsuario;
                    setTimeout(function () {
                        alertify.dismissAll();
                        $('#modalEditarUsuario').modal("show");
                    }, 1500);
                } else {
                    alertify.message("No se encontraron resultados en la consulta.", 10);
                }
            }
        });
    },
    editarUsuario: function (idUsuario, nombreUsuario, value) {
        $.ajax({
            url: urlPHP + "usuarios/actualizarDatosUsuario",
            method: "post",
            data: {
                idUsuario: idUsuario,
                nombreUsuario: nombreUsuario,
                tipoUsuarioTexto: $('#tipoUsuario option:selected').text().trim(),
                tipoUsuario: value.trim()
            },
            success: function (res) {
                if (res.n == 1) {
                    alertify.success("El usuario " + nombreUsuario + " ha cambiado de rol.",10);
                } else if ((res.err !== null || res.errmsg) || res == "") {
                    errorInesperado(res.err, res.errmsg);
                }
            }
        });
    },
    cambiarCuraduriaUsuario: function(idUsuario,nombreUsuario, idCuraduria){
        alertify.confirm(
            "Atención",
            "¿Está seguro que desea cambiar de curaduría al usuario " + nombreUsuario + "?",
            function(){ 
                $.ajax({
                    url: urlPHP + "usuarios/cambiarCuraduriaUsuario",
                    method: "post",
                    data:{
                        nombreUsuario: nombreUsuario,
                        idUsuario: idUsuario,
                        curaduria: idCuraduria.trim()
                    },
                    success: function(res){
                        if(res.n == 1){
                            alertify.success("El usuario " + nombreUsuario + " ha cambiado de curaduría exitosamente.");
                        }else if ((res.err !== null || res.errmsg) || res == "") {
                            errorInesperado(res.err, res.errmsg);
                        }
                    }
                });
            },
            function(){ return; }
        );
        
    },
    cargarModalAjustes: function(urlAjuste){
        $("#modalAjustes").load(urlPHP + urlAjuste,function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success"){
            $("#modalAjustes").modal({ show: true });
            $(".aplicarSelect2").select2({placeholder:" Seleccione"});
        }
        else if(statusTxt == "error"){
            errorInesperado(xhr,xhr.error.name);
        }
    });
    },
    validarCrearAjuste: function(id,tipo,obligatorio,coleccion,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearRegistro",
                        method: "post",
                        data: {
                            coleccion: coleccion,
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ sisgac1.consultarAjustesAjax(callback); limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); 
                                    });
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarCrearDepartamento: function(id,tipo,obligatorio,coleccion,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearDepartamento",
                        method: "post",
                        data: {
                            coleccion: coleccion,
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ sisgac1.consultarAjustesAjax(callback); limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); 
                                    });
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarCrearMunicipio: function(id,tipo,obligatorio,coleccion,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        if(!validarSelect("idDepartamento","null") || !validarSelect("idDepartamento","")){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearMunicipio",
                        method: "post",
                        data: {
                            coleccion: coleccion,
                            idDepartamento: document.getElementById("idDepartamento").value.trim(),
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ 
                                                $("#idDepartamento").trigger("change");
                                                limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); 
                                    });
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarCrearBarrio: function(id,tipo,obligatorio,coleccion,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código de barrio",10);
            return;
        }
        if(!validarSelect("idMunicipio","null") || !validarSelect("idMunicipio","")){
            alertify.warning("Asegurese de seleccionar municipio y que solo contenga letras y el código DANE",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearBarrio",
                        method: "post",
                        data: {
                            coleccion: coleccion,
                            idMunicipio: document.getElementById("idMunicipio").value.trim(),
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ 
                                                $("#idDepartamento").val("null");
                                                $("#idDepartamento").trigger("change");
                                                $("#idMunicipio").trigger("change");
                                                limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); 
                                    });
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarCrearCuraduria: function(id,tipo,obligatorio,datos){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código de curaduría",10);
            return;
        }
        if(!validarSelect("idMunicipio","null") || !validarSelect("idMunicipio","")){
            alertify.warning("Asegurese de seleccionar un municipio",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearCuraduria",
                        method: "post",
                        data: {
                            idMunicipio: document.getElementById("idMunicipio").value.trim(),
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ 
                                                $("#idDepartamento").val("null");
                                                $("#idDepartamento").trigger("change");
                                                $("#idMunicipio").trigger("change");
                                                limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); }
                                    );
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarCrearVereda: function(id,tipo,obligatorio,datos){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código de curaduría",10);
            return;
        }
        if(!validarSelect("idMunicipio","null") || !validarSelect("idMunicipio","")){
            alertify.warning("Asegurese de seleccionar un municipio",10);
            return;
        }
        alertify.confirm(
                "¿Crear registro de ajuste?", 
                "¿Está seguro de crear un nuevo registro de "+ document.getElementById(id[0]).value +"?",
                function(){
                    $.ajax({
                        url: urlPHP + "ajustes/crearVereda",
                        method: "post",
                        data: {
                            idMunicipio: document.getElementById("idMunicipio").value.trim(),
                            valor: datos
                        },
                        success: function(res){
                            if(res.ok == 1){
                                setTimeout(function(){
                                    alertify.confirm(
                                            "¿Desea registrar otro?",
                                            "Registro exitoso de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar registrando nueva información dentro del sistema de ajustes?",
                                            function(){ 
                                                $("#idDepartamento").val("null");
                                                $("#idDepartamento").trigger("change");
                                                $("#idMunicipio").trigger("change");
                                                limpiarCampos(id); },
                                            function(){ $("#modalAjustes").modal("hide"); }
                                    );
                                },350);
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje);
                            }
                        }
                    });
                },
                function(){}
        );
        
        
    },
    validarActualizarAjuste: function(id,tipo,obligatorio,coleccion,idAjuste,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras",10);
            return;
        }
        $.ajax({
            url: urlPHP + "ajustes/actualizarRegistro",
            method: "post",
            data: {
                coleccion: coleccion,
                idAjuste: $('#'+idAjuste).val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ sisgac1.consultarAjustesAjax(callback); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }
            }
        });
    },
    validarActualizarDepartamento: function(id,tipo,obligatorio,coleccion,idAjuste,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        $.ajax({
            url: urlPHP + "ajustes/actualizarDepartamento",
            method: "post",
            data: {
                coleccion: coleccion,
                idAjuste: $('#'+idAjuste).val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ sisgac1.consultarAjustesAjax(callback); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }else if(res.mensaje != ""){
                    alertify.warning(res.mensaje);
                }
            }
        });
    },
    validarActualizarMunicipio: function(id,tipo,obligatorio,coleccion,idAjuste,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        if(!validarSelect("idDepartamento","null") || !validarSelect("idDepartamento","")){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        
        $.ajax({
            url: urlPHP + "ajustes/actualizarMunicipio",
            method: "post",
            data: {
                coleccion: coleccion,
                idDepartamento: document.getElementById("idDepartamento").value.trim(),
                idAjuste: $('#'+idAjuste).val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ $("#idDepartamento").trigger("change"); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }else if(res.mensaje != ""){
                    alertify.warning(res.mensaje);
                }
            }
        });
    },
    validarActualizarBarrio: function(id,tipo,obligatorio,coleccion,idAjuste,datos,callback){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código",10);
            return;
        }
        if(!validarSelect("idDepartamento","null") || !validarSelect("idDepartamento","")){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código",10);
            return;
        }
        
        $.ajax({
            url: urlPHP + "ajustes/actualizarBarrio",
            method: "post",
            data: {
                coleccion: coleccion,
                idMunicipio: document.getElementById("idMunicipio").value.trim(),
                idAjuste: $('#'+idAjuste).val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ $("#idDepartamento").val("null"); $("#idDepartamento").trigger("change"); $("#idMunicipio").trigger("change"); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }else if(res.mensaje != ""){
                    alertify.warning(res.mensaje);
                }
            }
        });
    },
    validarActualizarCuraduria: function(id,tipo,obligatorio,datos){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código",10);
            return;
        }
        if(!validarSelect("idMunicipio","null") || !validarSelect("idMunicipio","")){
            alertify.warning("Asegurese de seleccionar un municipio",10);
            return;
        }
        
        $.ajax({
            url: urlPHP + "ajustes/actualizarCuraduria",
            method: "post",
            data: {
                idMunicipio: document.getElementById("idMunicipio").value.trim(),
                idAjuste: $('#idAjuste').val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ $("#idDepartamento").val("null"); $("#idDepartamento").trigger("change"); $("#idMunicipio").trigger("change"); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }else if(res.mensaje != ""){
                    alertify.warning(res.mensaje);
                }
            }
        });
    },
    validarActualizarVereda: function(id,tipo,obligatorio,datos){
        if(!validarCampo(id,tipo,obligatorio)){
            alertify.warning("Asegurese de llenar el campo y que solo contenga letras y el código DANE",10);
            return;
        }
        if(!validarSelect("idMunicipio","null") || !validarSelect("idMunicipio","")){
            alertify.warning("Asegurese de seleccionar un municipio",10);
            return;
        }
        
        $.ajax({
            url: urlPHP + "ajustes/actualizarVereda",
            method: "post",
            data: {
                idMunicipio: document.getElementById("idMunicipio").value.trim(),
                idAjuste: $('#idAjuste').val().trim(),
                valor: datos
            },
            success: function(res){
                if(res.n == 1){
                    setTimeout(function(){
                        alertify.confirm(
                                "¡Éxito! ¿Desea actualizar otro?",
                                "Actualización exitosa de " + document.getElementById(id[0]).value.trim()+" ¿Desea continuar actualizando información dentro del sistema de ajustes?",
                                function(){ $("#idDepartamento").val("null"); $("#idDepartamento").trigger("change"); $("#idMunicipio").trigger("change"); limpiarCampos(id); },
                                function(){ $("#modalAjustes").modal("hide"); 
                        });
                    },350);
                }else if(res.mensaje != ""){
                    alertify.warning(res.mensaje);
                }
            }
        });
    },
    //función llamada en validarCrearAjuste en el callback si acepta crear más registros
    consultarAjustesAjax: function(callback){
                                    $.ajax({
                                        url: urlPHP + callback.url,
                                        method: "post",
                                        data:{
                                            metodo: callback.metodo
                                        },
                                        success: function(res){
                                            var row = "";
                                            var obj = {};
                                            var posicion = null;
                                            for(var objeto in res){
                                                obj = res[objeto];
                                                posicion = Object.keys(obj);
                                                row += "<tr>";
                                                row += "<td>";
                                                row += obj[posicion[1]];
                                                row += "</td>";
                                                
                                                if(posicion.length > 2){
                                                    row += "<td>";
                                                    row += obj[posicion[2]];
                                                    row += "</td>";
                                                }
                                                row += '<td><button onclick="sisgac1.cargarInfoAjusteForm(';
                                                row += ' \''+ obj[posicion[0]]['$id'] +'\' , \''+ obj[posicion[1]] +'\' ';
                                                if(posicion.length > 2){
                                                    row += ' , \''+ obj[posicion[2]] +'\' ';
                                                }
                                                row += ')" ';
                                                row += 'type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>';
                                                row += "</tr>";
                                            }
                                            document.getElementById("tablaDatosAjustes").innerHTML = row;
                                        }
                                    });
                                },
    consultarAjustesMunicipios: function(callback,objetivo){
        
        if(objetivo == "tabla"){
            document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron municipios</td></tr>';
            $.ajax({
                url: urlPHP + callback.url,
                method: "post",
                data:{
                    metodo: callback.metodo,
                    idDepartamento: document.getElementById("idDepartamento").value.trim()
                },
                success: function(res){
                    if(res.length > 0 || res != ""){
                        var row = "";
                        var obj = {};
                        var posicion = null;
                        for(var objeto in res){
                            obj = res[objeto];
                            posicion = Object.keys(obj);
                            row += "<tr>";
                            row += "<td>";
                            row += obj[posicion[1]];
                            row += "</td>";

                            if(posicion.length > 2){
                                row += "<td>";
                                row += obj[posicion[2]];
                                row += "</td>";
                            }
                            row += '<td><button onclick="sisgac1.cargarInfoAjusteForm(';
                            row += ' \''+ obj[posicion[0]]['$id'] +'\' , \''+ obj[posicion[1]] +'\' ';
                            if(posicion.length > 2){
                                row += ' , \''+ obj[posicion[2]] +'\' ';
                            }
                            row += ')" ';
                            row += 'type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>';
                            row += "</tr>";
                        }
                        document.getElementById("tablaDatosAjustes").innerHTML = row;
                        return;
                    }else{
                        document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron municipios</td></tr>';
                        return;
                    }
                }
            }); 
        }else if(objetivo == "select"){
            document.getElementById("idMunicipio").value = null;
            document.getElementById("idMunicipio").innerHTML = '<option value="null">Seleccione municipio</option>';
            $("#idMunicipio").trigger("change");
            $.ajax({
                url: urlPHP + callback.url,
                method: "post",
                data:{
                    metodo: callback.metodo,
                    idDepartamento: document.getElementById("idDepartamento").value.trim()
                },
                success: function(res){
                    if(res.length > 0 || res != ""){
                        var row = '<option value="null">Seleccione municipio</option>';
                        var obj = {};
                        var posicion = null;
                        for(var objeto in res){
                            obj = res[objeto];
                            posicion = Object.keys(obj);
                            
                            row += "<option value='"+ obj[posicion[0]]['$id'] +"'>";
                            row += obj[posicion[2]] + " - " + obj[posicion[1]];
                            row += "</option>";

                        }
                        document.getElementById("idMunicipio").innerHTML = row;
                        return;
                    }else{
                        document.getElementById("idMunicipio").innerHTML = '<option value="null">Seleccione municipio</option>';
                        return;
                    }
                }
            });
        }
        
        
    },
    consultarAjustesBarrios: function(callback, objetivo){
        if(objetivo == "tabla"){
            document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron barrios</td></tr>';
            $.ajax({
                url: urlPHP + callback.url,
                method: "post",
                data:{
                    metodo: callback.metodo,
                    idMunicipio: document.getElementById("idMunicipio").value.trim()
                },
                success: function(res){
                    if(res.length > 0 || res != ""){
                        var row = "";
                        var obj = {};
                        var posicion = null;
                        for(var objeto in res){
                            obj = res[objeto];
                            posicion = Object.keys(obj);
                            row += "<tr>";
                            row += "<td>";
                            row += obj[posicion[1]];
                            row += "</td>";

                            if(posicion.length > 2){
                                row += "<td>";
                                row += obj[posicion[2]];
                                row += "</td>";
                            }
                            row += '<td><button onclick="sisgac1.cargarInfoAjusteForm(';
                            row += ' \''+ obj[posicion[0]]['$id'] +'\' , \''+ obj[posicion[1]] +'\' ';
                            if(posicion.length > 2){
                                row += ' , \''+ obj[posicion[2]] +'\' ';
                            }
                            row += ')" ';
                            row += 'type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>';
                            row += "</tr>";
                        }
                        document.getElementById("tablaDatosAjustes").innerHTML = row;
                        return;
                    }else{
                        document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron barrios</td></tr>';
                        return;
                    }
                }
            });
        }
         
    },
    consultarAjustesCuradurias: function(callback, objetivo){
        if(objetivo == "tabla"){
            document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron curadurías</td></tr>';
            $.ajax({
                url: urlPHP + callback.url,
                method: "post",
                data:{
                    metodo: callback.metodo,
                    idMunicipio: document.getElementById("idMunicipio").value.trim()
                },
                success: function(res){
                    if(res.length > 0 || res != ""){
                        var row = "";
                        var obj = {};
                        var posicion = null;
                        for(var objeto in res){
                            obj = res[objeto];
                            posicion = Object.keys(obj);
                            row += "<tr>";
                            row += "<td>";
                            row += obj[posicion[1]];
                            row += "</td>";

                            if(posicion.length > 2){
                                row += "<td>";
                                row += obj[posicion[2]];
                                row += "</td>";
                            }
                            row += '<td><button onclick="sisgac1.cargarInfoAjusteForm(';
                            row += ' \''+ obj[posicion[0]]['$id'] +'\' , \''+ obj[posicion[1]] +'\' ';
                            if(posicion.length > 2){
                                row += ' , \''+ obj[posicion[2]] +'\' ';
                            }
                            row += ')" ';
                            row += 'type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>';
                            row += "</tr>";
                        }
                        document.getElementById("tablaDatosAjustes").innerHTML = row;
                        return;
                    }else{
                        document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron curadurías</td></tr>';
                        return;
                    }
                }
            }); 
        }
    },
    consultarAjustesVeredas: function(callback, objetivo){
        if(objetivo == "tabla"){
            document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron veredas</td></tr>';
            $.ajax({
                url: urlPHP + callback.url,
                method: "post",
                data:{
                    metodo: callback.metodo,
                    idMunicipio: document.getElementById("idMunicipio").value.trim()
                },
                success: function(res){
                    if(res.length > 0 || res != ""){
                        var row = "";
                        var obj = {};
                        var posicion = null;
                        for(var objeto in res){
                            obj = res[objeto];
                            posicion = Object.keys(obj);
                            row += "<tr>";
                            row += "<td>";
                            row += obj[posicion[1]];
                            row += "</td>";

                            if(posicion.length > 2){
                                row += "<td>";
                                row += obj[posicion[2]];
                                row += "</td>";
                            }
                            row += '<td><button onclick="sisgac1.cargarInfoAjusteForm(';
                            row += ' \''+ obj[posicion[0]]['$id'] +'\' , \''+ obj[posicion[1]] +'\' ';
                            if(posicion.length > 2){
                                row += ' , \''+ obj[posicion[2]] +'\' ';
                            }
                            row += ')" ';
                            row += 'type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-wrench"></i></button></td>';
                            row += "</tr>";
                        }
                        document.getElementById("tablaDatosAjustes").innerHTML = row;
                        return;
                    }else{
                        document.getElementById("tablaDatosAjustes").innerHTML = '<tr class="warning"><td colspan="3">No se encontraron veredas</td></tr>';
                        return;
                    }
                }
            }); 
        }
    },
    //función para llenar campo1 y campo2 correspondientemente con los valores cargados desde base de datos
    cargarInfoAjusteForm:function(id,campo1,campo2){
        document.getElementById("idAjuste").value = id;
        document.getElementById("campo1").value = campo1;
        if(campo2){
            document.getElementById("campo2").value = campo2;
        }
    },
    validarCrearTramite: function(){
        var ids = [
            "idTipoTramites",
            "observaciones",
            "idTipoUsosVivienda",
            "idTipoModalidades",
            "idObjetoTramite",
            "folioInicial",
            "folioFinal",
            "totalFolios",
            "consecutivoTramite",
            "anoRadicacionTramite",
            "fechaEjecutoria",
            "fechaExpedicion",
            "fechaLegalDebidaForma",
            "consecutivoDocumento",
            "anoRadicacionDocumento",
            "matriculaInmobiliaria",
            "codigoCatastral",
            "direccionNomenclatura",
            "barrio",
            "veredaUrb",
            "tipoDocIdentificacionTitular",
            "numDocTitular",
            "nombreTitular",
            "apellidoTitular",
            "tipoDocIdentificacionSolicitante",
            "numDocSolicitante",
            "nombreSolicitante",
            "apellidoSolicitante",
            "telefonoSolicitante",
            "direccionSolicitante",
            "emailSolicitante"
        ];
        var tipos = [
            "alfanum",
            "alfanum",
            "alfanum",
            "alfanum",
            "alfanum",
            "num",
            "num",
            "num",
            "num",
            "num",
            "libre",
            "libre",
            "libre",
            "num",
            "num",
            "num",
            "num",
            "libre",
            "libre",
            "libre",
            "texto",
            "num",
            "texto",
            "texto",
            "texto",
            "num",
            "texto",
            "texto",
            "num",
            "libre",
            "email"
        ];
        var obligatorios = [
            true,
            false,
            false,
            false,
            true,
            true,
            true,
            true,
            false,
            false,
            false,
            true,
            false,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        ];
        
        if(!document.getElementById("anoRadicacionTramite").value.trim().length == 4 || !document.getElementById("anoRadicacionTramite").value.trim().length == 2 ){
            alertify.error("El año del trámite debe contener 2 o 4 caracteres exactos, según sea el año. Ej: 1999 o 99.");
            document.getElementById("anoRadicacionTramite").setAttribute("style", "border-color: #d9232d");
            return;
        }
        
        if(!document.getElementById("anoRadicacionDocumento").value.trim().length == 4 || !document.getElementById("anoRadicacionDocumento").value.trim().length == 2 ){
            alertify.error("El año del documento debe contener 2 o 4 caracteres exactos, según sea el año. Ej: 1999 o 99.");
            document.getElementById("anoRadicacionDocumento").setAttribute("style", "border-color: #d9232d");
            return;
        }
        
        if(document.getElementById("codigoCatastral").value.trim().length != 20 ){
            alertify.error("El código catastral debe contener 20 caracteres.");
            return;
        }
        if(!validarCampo(ids,tipos,obligatorios)){
            alertify.error("Por favor asegurese de llenar todos los campos obligatorios y que tengan un tipo de dato adecuado.");
            return;
        }
        
        var direccion  = document.getElementsByName("direccionNomenclatura");
        if(direccion[0].value.trim() == "" || direccion[1].value.trim() == "" || direccion[2].value.trim() == "" || direccion[3].value.trim() == ""){
            alertify.error("Por favor llene la dirección de manera adecuada",0);
            return;
        }
        
        var esTramite = document.getElementsByName("esTramite");
        for(var i = 0; i < esTramite.length; i++){
            if(esTramite[i].checked){
                esTramite = esTramite[i].value == "true" ? true : false;
                break;
            }
        }
        if(typeof esTramite == "object"){ 
            alertify.error("Debe seleccionar si desea crear un trámite o un documento.", 0); 
            return; 
        }
        
        if(esTramite){
            
            if(!validarCampo(["consecutivoTramite","anoRadicacionTramite"],["num","num"],[true,true])){
                alertify.error("Por favor asegurese de llenar todos los campos obligatorios y que tengan un tipo de dato adecuado.");
                return;
            }
            if(!validarSelectMultiple("idTipoModalidades","") || !validarSelectMultiple("idTipoModalidades","null")){
                return;
            }
            if(!validarSelectMultiple("idTipoUsosVivienda","") || !validarSelectMultiple("idTipoUsosVivienda","null")){
                return;
            }
            if(!validarSelectMultiple("idObjetoTramite","") || !validarSelectMultiple("idObjetoTramite","null")){
                return;
            }
            
            if(document.getElementById("consecutivoTramite").value.trim().length != 4 || document.getElementById("consecutivoDocumento").value.trim().length != 4){
                alertify.error("Los consecutivos deben ser de  4 caracteres exactos, incluya ceros en caso de hacer falta.");
                document.getElementById("consecutivoTramite").setAttribute("style", "border-color: #d9232d");
                document.getElementById("consecutivoDocumento").setAttribute("style", "border-color: #d9232d");
                return;
            }
            if(!validarCampo(["fechaLegalDebidaForma"],["libre"],[true])){
                return;
            }
        }else{
            var cont = $("#idTipoTramites").val();
            if(cont.length > 1){
                alertify.error("Solo puede seleccionar un tipo de trámite o actuación si desea guardar un documento.", 0);
                return;
            }
            if(document.getElementById("consecutivoDocumento").value.trim().length != 4){
                alertify.error("Los consecutivos deben ser de  4 caracteres exactos, incluya ceros en caso de hacer falta.");
                document.getElementById("consecutivoDocumento").setAttribute("style", "border-color: #d9232d");
                return;
            }
        }
        
        if(!validarSelectMultiple("idTipoTramites","") || !validarSelectMultiple("idTipoTramites","null")){
                return;
        }
        if(!validarSelectMultiple("barrio","") || !validarSelectMultiple("barrio","null")){
                return;
        }
        if(!validarSelect("estrato","") || !validarSelect("estrato","null")){
                return;
        }
        if(!validarSelect("veredaUrb","") || !validarSelect("veredaUrb","null")){
                return;
        }
        
        alertify.confirm(
                "¿Está seguro de registrar un nuevo trámite?",
                "¿Está seguro de que desea guardar información relacionada con un trámite?",
                function(){
                    $.ajax({
                        url: urlPHP + "tramites/crearNuevo",
                        method: "post",
                        data: {
                            "esTramite" : esTramite,
                            "idTipoTramites" : $("#idTipoTramites").val(),
                            "observaciones" : $("#observaciones").val(),
                            "idTipoModalidades" : $("#idTipoModalidades").val(),
                            "idTipoUsosVivienda" : $("#idTipoUsosVivienda").val(),
                            "idObjetoTramite" : $("#idObjetoTramite").val() == null || $("#idObjetoTramite").val() == "null" ? "" : $("#idObjetoTramite").val(),
                            "folioInicial" : document.getElementById("folioInicial").value.trim(),
                            "folioFinal" : document.getElementById("folioFinal").value.trim(),
                            "totalFolios" : document.getElementById("totalFolios").value.trim(),
                            "numConsecutivo" : {
                                "consecutivoTramite" : document.getElementById("consecutivoTramite").value.trim(),
                                "consecutivoDocumento" : document.getElementById("consecutivoDocumento").value.trim()
                            },
                            "anoRadicacion" : {
                                "anoRadicacionTramite" : document.getElementById("anoRadicacionTramite").value.trim(),
                                "anoRadicacionDocumento" : document.getElementById("anoRadicacionDocumento").value.trim()
                            },
                            "fechaEjecutoria" : document.getElementById("fechaEjecutoria").value.trim(),
                            "fechaExpedicion" : document.getElementById("fechaExpedicion").value.trim(),
                            "fechaLegalDebidaForma" : document.getElementById("fechaLegalDebidaForma").value.trim(),
                            predio: {
                                "matriculaInmobiliaria" : document.getElementById("matriculaInmobiliaria").value.trim(),
                                "codigoCatastral" : document.getElementById("codigoCatastral").value.trim(),
                                "direccionNomenclatura" : direccion[0].value.trim()+"/"+direccion[1].value.trim()+"/"+direccion[2].value.trim()+"/"+direccion[3].value.trim(),
                                "estrato" : document.getElementById("estrato").value.trim(),
                                "barrio" : document.getElementById("barrio").value.trim(),
                                "veredaUrb" : document.getElementById("veredaUrb").value.trim(),
                            },
                            titular: {
                                "tipoDocIdentificacionTitular" : document.getElementById("tipoDocIdentificacionTitular").value.trim(),
                                "numDocTitular" : document.getElementById("numDocTitular").value.trim(),
                                "nombreTitular" : document.getElementById("nombreTitular").value.trim(),
                                "apellidoTitular" : document.getElementById("apellidoTitular").value.trim()
                            },
                            solicitante: {
                                "tipoDocIdentificacionSolicitante" : document.getElementById("tipoDocIdentificacionSolicitante").value.trim(),
                                "numDocSolicitante" : document.getElementById("numDocSolicitante").value.trim(),
                                "nombreSolicitante" : document.getElementById("nombreSolicitante").value.trim(),
                                "apellidoSolicitante" : document.getElementById("apellidoSolicitante").value.trim(),
                                "telefonoSolicitante" : document.getElementById("telefonoSolicitante").value.trim(),
                                "direccionSolicitante" : document.getElementById("direccionSolicitante").value.trim(),
                                "emailSolicitante" : document.getElementById("emailSolicitante").value.trim()
                            }    
                        },
                        success: function(res){
                            if("ok" in res[0]){
                                alertify.dismissAll();
                                alertify.success(res["mensajeExito"],0);
                                limpiarCampos(ids);
                                $("#idTipoTramites").trigger("change");
                                $("#idTipoModalidades").trigger("change");
                                $("#idTipoUsosVivienda").trigger("change");
                                $("#idObjetoTramite").trigger("change");
                            }else if(res.mensaje != ""){
                                alertify.warning(res.mensaje,0);
                            }else if ((res.err !== null || res.errmsg !== null) || res == "") {
                                errorInesperado(res.err, res.errmsg);
                            }
                        }
                    });
                },
                function(){}
            );
        
    },
    validarEditarTramite: function(){
        var esTramite = document.getElementsByName("esTramite")[0].value.trim();
                    
        var datosTramite = {};
        var ids = null;
        var tipos = null;
        var obligatorios = null;
        
        if(esTramite == "true"){
            if(!validarSelectMultiple("idTipoTramites","") || !validarSelectMultiple("idTipoTramites","null")){
                return;
            }
            if(!validarSelectMultiple("idTipoModalidades","") || !validarSelectMultiple("idTipoModalidades","null")){
                return;
            }
            if(!validarSelectMultiple("idTipoUsosVivienda","") || !validarSelectMultiple("idTipoUsosVivienda","null")){
                return;
            }
            if(!validarSelectMultiple("idObjetoTramite","") || !validarSelectMultiple("idObjetoTramite","null")){
                return;
            }
            
            ids = [
                "idTipoTramites",
                "idTipoUsosVivienda",
                "idTipoModalidades",
                "idObjetoTramite",
                "folioInicial",
                "folioFinal",
                "totalFolios",
                "fechaEjecutoria",
                "fechaExpedicion",
                "fechaLegalDebidaForma",
                "matriculaInmobiliaria",
                "codigoCatastral",
                "direccionNomenclatura",
                "tipoDocIdentificacionTitular",
                "numDocTitular",
                "nombreTitular",
                "apellidoTitular",
                "tipoDocIdentificacionSolicitante",
                "numDocSolicitante",
                "nombreSolicitante",
                "apellidoSolicitante",
                "telefonoSolicitante",
                "direccionSolicitante",
                "emailSolicitante"
            ];
            tipos = [
                "alfanum",
                "alfanum",
                "alfanum",
                "alfanum",
                "num",
                "num",
                "num",
                "libre",
                "libre",
                "libre",
                "num",
                "num",
                "libre",
                "texto",
                "num",
                "texto",
                "texto",
                "texto",
                "num",
                "texto",
                "texto",
                "num",
                "libre",
                "email"
            ];
            obligatorios = [
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "false",
                "false",
                "false",
                "false",
                "false",
                "false",
                "false"
            ];
        }else{
            ids = [
                "observaciones",
                "folioInicial",
                "folioFinal",
                "totalFolios",
                "fechaExpedicion",
                "matriculaInmobiliaria",
                "codigoCatastral",
                "direccionNomenclatura",
                "tipoDocIdentificacionTitular",
                "numDocTitular",
                "nombreTitular",
                "apellidoTitular"
            ];
            tipos = [
                "alfanum",
                "num",
                "num",
                "num",
                "libre",
                "num",
                "num",
                "libre",
                "libre",
                "num",
                "texto",
                "texto"
            ];
            obligatorios = [
                "false",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
                "true",
            ];
        }
        
        if(document.getElementById("codigoCatastral").value.trim().length != 20 ){
            alertify.error("El código catastral debe contener 20 caracteres.");
            return;
        }
        var direccion  = document.getElementsByName("direccionNomenclatura");
        if(direccion[0].value.trim() == "" || direccion[1].value.trim() == "" || direccion[2].value.trim() == "" || direccion[3].value.trim() == ""){
            alertify.error("Por favor llene la dirección de manera adecuada",0);
            return;
        }
        if(!validarSelectMultiple("barrio","") || !validarSelectMultiple("barrio","null")){
            return;
        }
        if(!validarSelect("estrato","") || !validarSelect("estrato","null")){
            return;
        }
        if(!validarSelect("veredaUrb","") || !validarSelect("veredaUrb","null")){
            return;
        }
        if(!validarCampo(ids,tipos,obligatorios)){
            alertify.error("Por favor asegurese de llenar todos los campos obligatorios y que tengan un tipo de dato adecuado.");
            return;
        }
        
        
        
        alertify.confirm(
                "Edición de información",
                "¿Está seguro que desea modificar esta información?",
                function(){
                    if(esTramite == "true"){
                        datosTramite = {
                            "esTramite" : esTramite,
                            "idTramite" : $("#idTramite").val(),
                            "idTipoTramites" : $("#idTipoTramites").val(),
                            "idTipoModalidades" : $("#idTipoModalidades").val(),
                            "idTipoUsosVivienda" : $("#idTipoUsosVivienda").val(),
                            "idObjetoTramite" : $("#idObjetoTramite").val() == null || $("#idObjetoTramite").val() == "null" ? "" : $("#idObjetoTramite").val(),
                            "folioInicial" : document.getElementById("folioInicial").value.trim(),
                            "folioFinal" : document.getElementById("folioFinal").value.trim(),
                            "totalFolios" : document.getElementById("totalFolios").value.trim(),
                            "fechaEjecutoria" : document.getElementById("fechaEjecutoria").value.trim(),
                            "fechaExpedicion" : document.getElementById("fechaExpedicion").value.trim(),
                            "fechaLegalDebidaForma" : document.getElementById("fechaLegalDebidaForma").value.trim(),
                            predio: {
                                "matriculaInmobiliaria" : document.getElementById("matriculaInmobiliaria").value.trim(),
                                "codigoCatastral" : document.getElementById("codigoCatastral").value.trim(),
                                "direccionNomenclatura" : direccion[0].value.trim()+"/"+direccion[1].value.trim()+"/"+direccion[2].value.trim()+"/"+direccion[3].value.trim(),
                                "estrato" : document.getElementById("estrato").value.trim(),
                                "barrio" : document.getElementById("barrio").value.trim(),
                                "veredaUrb" : document.getElementById("veredaUrb").value.trim(),
                            },
                            titular: {
                                "tipoDocIdentificacionTitular" : document.getElementById("tipoDocIdentificacionTitular").value.trim(),
                                "numDocTitular" : document.getElementById("numDocTitular").value.trim(),
                                "nombreTitular" : document.getElementById("nombreTitular").value.trim(),
                                "apellidoTitular" : document.getElementById("apellidoTitular").value.trim()
                            },
                            solicitante: {
                                "tipoDocIdentificacionSolicitante" : document.getElementById("tipoDocIdentificacionSolicitante").value.trim(),
                                "numDocSolicitante" : document.getElementById("numDocSolicitante").value.trim(),
                                "nombreSolicitante" : document.getElementById("nombreSolicitante").value.trim(),
                                "apellidoSolicitante" : document.getElementById("apellidoSolicitante").value.trim(),
                                "telefonoSolicitante" : document.getElementById("telefonoSolicitante").value.trim(),
                                "direccionSolicitante" : document.getElementById("direccionSolicitante").value.trim(),
                                "emailSolicitante" : document.getElementById("emailSolicitante").value.trim()
                            }
                        };
                    }else{
                        datosTramite = {
                            "esTramite" : esTramite,
                            "idTramite" : $("#idTramite").val(),
                            "observaciones" : $("#observaciones").val(),
                            "folioInicial" : document.getElementById("folioInicial").value.trim(),
                            "folioFinal" : document.getElementById("folioFinal").value.trim(),
                            "totalFolios" : document.getElementById("totalFolios").value.trim(),
                            "fechaExpedicion" : document.getElementById("fechaExpedicion").value.trim(),
                            predio: {
                                "matriculaInmobiliaria" : document.getElementById("matriculaInmobiliaria").value.trim(),
                                "codigoCatastral" : document.getElementById("codigoCatastral").value.trim(),
                                "direccionNomenclatura" : direccion[0].value.trim()+"/"+direccion[1].value.trim()+"/"+direccion[2].value.trim()+"/"+direccion[3].value.trim(),
                                "estrato" : document.getElementById("estrato").value.trim(),
                                "barrio" : document.getElementById("barrio").value.trim(),
                                "veredaUrb" : document.getElementById("veredaUrb").value.trim(),
                            },
                            titular: {
                                "tipoDocIdentificacionTitular" : document.getElementById("tipoDocIdentificacionTitular").value.trim(),
                                "numDocTitular" : document.getElementById("numDocTitular").value.trim(),
                                "nombreTitular" : document.getElementById("nombreTitular").value.trim(),
                                "apellidoTitular" : document.getElementById("apellidoTitular").value.trim()
                            }
                        };
                    }
                    
                    $.ajax({
                        url: urlPHP + "tramites/editar",
                        method: "post",
                        data:datosTramite,
                        success: function(res){
                            if("ok" in res){
                                alertify.success("Información editada con éxito",0);
                                $('#modalEditarTramite').modal('hide')
                            }else if ((res.err !== null || res.errmsg !== null) || res == "") {
                                errorInesperado(res.err, res.errmsg);
                            }
                        }
                    });
                },
                function(){}
            );
    },
    validarEliminarTramite: function(objeto,idTramite,cod){
        alertify.confirm(
                "Advertencia, borrado de " + cod,
                "Está a punto de eliminar <strong>toda la información de un trámite</strong>, esto incluye archivos que tengan con este.<br>Se aconseja que <strong>genere un respaldo de todos los archivos</strong> del trámite antes de continuar, si ya es el caso asegúrese que es el trámite exacto.",
                function(){
                    $.ajax({
                        url: urlPHP + "tramites/eliminar",
                        method: "post",
                        data:{
                            idTramite: idTramite,
                            cod: cod
                        },
                        beforeSend: function(){
                            $(objeto.firstChild).toggleClass("glyphicon glyphicon-repeat animarLoading");
                        },
                        success: function(res){
                            var validez = [];
                            $(objeto.firstChild).toggleClass("glyphicon glyphicon-repeat animarLoading");
                            if(res.archivoEliminado.err != null){
                                alertify.error("Ocurrió un error al eliminar los archivos del trámite");
                                validez.push(false);
                            }
                            if(res.tramiteEliminado.err != null){
                                alertify.error("Ocurrió un error al eliminar el trámite");
                                validez.push(false);
                            }
                            if(validez.length > 0){
                                return;
                            }else{
                                location.href = urlPHP + "tramites/listar/25/1";
                            }
                        }
                    });
                },
                function(){}
            );
    },
    subirArchivo: function(){
        
        if(document.getElementsByName("archivo[]").length == 0){
            alertify.warning("No hay archivos para subir",0);
            return;
        }else if(document.getElementsByName("archivo[]").length >= 25){
            alertify.warning("No puede subir más de 25 archivos simultaneamente<br>límite de subida: 200Mb",0);
            return;
        }
        var archivo = document.getElementsByName("archivo[]");
        var nombreArchivo = document.getElementsByName("nombreArchivo[]");
        var datosArchivos = new FormData(document.getElementById("formSubirArchivos"));
        
        var validez = [];
        var letras = /^[a-z0-9\u00C1\u00E1\u00C9\u00E9\u00CD\u00ED\u00D3\u00F3\u00DA\u00FA\u00D1\u00F1\s\_\-\(\)]*$/i;
        
        for(var i = 0; i < archivo.length; i++){
            if(archivo[i].value == null || archivo[i].value == ""){
                alertify.error("Debe llenar todos los campos de archivos",0);
                return;
            }
        }
        
        for(var i = 0; i < nombreArchivo.length; i++){
            if(!letras.test(nombreArchivo[i].value.trim())){
                validez.push(false);
                nombreArchivo[i].setAttribute("style","border-color: red");
            }else{
                nombreArchivo[i].setAttribute("style","");
            }
        }
        if(validez.length > 0){
            validez = [];
            return;
        }
        
//        función llamada dentro del callback XHR
        function progressHandlingFunction(e){
            if(e.lengthComputable){
                var cargado = e.loaded;
                var total = e.total;
                var actual = Math.ceil(Number(cargado / total * 100));
                if(actual <= 20){
                    $('.estadoSubidaArchivoTexto').attr({"aria-valuenow":actual, "style":"width: " + actual + "%; min-width: 2em","aria-valuemax":100});
                    $('.estadoSubidaArchivoTexto').html(Math.ceil(actual)+"%");
                }else if(actual > 20){
                    $('.estadoSubidaArchivoTexto').attr({"aria-valuenow":actual, "style":"width: " + actual + "%","aria-valuemax":100});
                    $('.estadoSubidaArchivoTexto').html("Enviando - " + Math.ceil(actual)+"%");
                }
                if(actual == 100){
                    $('.estadoSubidaArchivoTexto').html("Guardando archivos en el sistema - " + Math.ceil(actual)+"%");
                }
            }
        }
        
        $(".formSubirArchivos").hide(500,function(){ $("#estadoSubidaArchivo").show(500); });
        
        setTimeout(function(){
            $.ajax({
                url: urlPHP + "archivos/subir",
                method: "post",
                dataType: "html",
                data: datosArchivos,
                cache: false,
                contentType: false,
                processData: false,
                success: function(res){
                    if(res == true){
                        $('.estadoSubidaArchivoTexto').html("Archivos guardados exitosamente");
                        alertify.dismissAll();
                    }else{
                        alertify.error(res + "<br>LÍMITE DE SUBIDA: 200Mb",0);
                        $('.estadoSubidaArchivoTexto').html("Ocurrió un error al guardar los archivos");
                    }
                    $(".estadoSubidaArchivoBoton").show(500);
                },
                xhr: function() {  //Código buscado en internet
                    // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // Check if upload property exists
                        myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                    }
                    return myXhr;
                }
            })
        },1500);
    },
    validarEliminarArchivo: function(objeto,idArchivo,archivo){
        alertify.confirm(
                "Advertencia, borrado de " + archivo,
                "Está a punto de eliminar <strong>un archivo digital.</strong><br><strong>¿Realmente desea eliminar el archivo \"" + archivo + "\"?</strong>",
                function(){
                    $.ajax({
                        url: urlPHP + "archivos/eliminar",
                        method: "post",
                        data:{
                            idArchivo: idArchivo,
                            archivo: archivo
                        },
                        beforeSend: function(){
                            $(objeto.firstChild).toggleClass("glyphicon-repeat animarLoading");
                        },
                        success: function(res){
                            $(objeto.firstChild).toggleClass("glyphicon-repeat animarLoading");
                            if(res == true){
                                objeto.parentNode.parentNode.remove();
                                alertify.success("Archivo \""+archivo+"\" eliminado con éxito",0);
                            }else{
                                alertify.error(res,0);
                            }
                        }
                    });
                },
                function(){}
            );
    },
    recuperarCuenta: function(){
        $.ajax({
            url: urlPHP + "recuperarCuenta/recuperar",
            method: "post",
            data: {
                nombreUsuario: document.getElementById("nombreUsuarioRecuperar").value.trim(),
                email: document.getElementById("emailRecuperar").value.trim()
            },
            beforeSend: function(){
                alertify.success("<i class='glyphicon glyphicon-repeat animarLoading'></i> verificando información",0);
            },
            success: function(res){
                alertify.dismissAll();
                if(res == true){
                    alertify.success("Se ha enviado un mensaje a su correo electrónico con la nueva contraseña para ingresar al sistema",0);
                    $("#modRecuperarPWD").modal("hide");
                }else{
                    alertify.error("La información proporcionada no fue encontrada",0);
                }
            }
        });
    }
};